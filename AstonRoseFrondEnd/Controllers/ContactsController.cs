﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Controllers;

namespace AstonRoseFrondEnd.Controllers
{
    public class ContactsController : BaseController
    {
        //
        // GET: /Contacts/
        //private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public ActionResult Index()
        {
            ContactModel model = new ContactModel();
            ViewBag.MetaTitle = "Contact Us : Aston Rose";
            ViewBag.MetaDescription = "Contact Us : Aston Rose";
            ViewBag.PageTitle = "Contact Us  - ";
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.Contacts = (from m in db.Team
                                  where m.ShowInContacts
                                  orderby m.DisplayOrder
                                  select new Contact
                                  {
                                      Department = m.Department,
                                      Email = m.Email,
                                      Id = m.Id,
                                      Name = m.FirstName + " " + (m.LastName == null ? "" : m.LastName),
                                      Designation = m.Designation,
                                      Telephone = m.TelephoneNumber
                                  }).ToList();

                model.Offices = (db.Offices).ToList();
            }
            return View(model);

        }

    }
}
