﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Controllers;
using System.Text.RegularExpressions;

namespace AstonRoseFrondEnd.Controllers
{
    public class ServicesController : BaseController
    {
        //
        // GET: /Services/
        //private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public ActionResult Index(string title = null, int id = 0)
        {
            
            ServiceListModel model = GetModel(title, id);
            ViewBag.MetaTitle = "Services : Aston Rose";
            ViewBag.MetaDescription = "Services : Aston Rose";
            ViewBag.PageTitle = "Services  - " ;
            return View(model);
        }

        public ActionResult Select(string title = null, int id = 0)
        {
            ServiceListModel model = GetModel(title, id);
            return View(model);
        }

        private ServiceListModel GetModel(string title, int id)
        {
            ServiceListModel model = new ServiceListModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.ViewList = (from s in db.Services
                                  select new ServiceView { Id = s.Id, Title = s.Title }).ToList(); ;
                model.SelectedService = (from s in db.Services
                                         join cs in db.CaseStudies on s.Id equals cs.ServiceID into caseStudies
                                         from cs1 in caseStudies.DefaultIfEmpty()
                                         where id == 0 || s.Id == id
                                         select new ServiceViewModel
                                         {
                                             Id = s.Id,
                                             Title = s.Title,
                                             Description = s.Description,
                                             ContactIds = s.ContactIds,
                                             Image = s.Image,
                                             CaseStudyTitle = cs1.Title,
                                             CaseStudyDescription = cs1.Description,
                                             MetaTitle = s.MetaTitle,
                                             MetaDescription = s.MetaDescription,
                                             ServicesOfferedTitle1 = s.ServicesOfferedTitle1,
                                             ServicesOffered1 = s.ServicesOffered1,
                                             ServicesOfferedTitle2 = s.ServicesOfferedTitle2,
                                             ServicesOffered2 = s.ServicesOffered2,
                                             PDFBrochure = s.PDFBrochure,
                                         }).FirstOrDefault();


                if (model.SelectedService != null)
                {
                    model.SelectedService.Contacts = db.Database.SqlQuery<ServiceContact>("select FirstName + ' ' + LastName as Name, TelephoneNumber as Phone, FirstName as EmailName,Email as EmailID from Team where ID in (" + model.SelectedService.ContactIds + ") ").ToList();
                    ViewBag.MetaTitle = model.SelectedService.MetaTitle;
                    ViewBag.MetaDescription = model.SelectedService.MetaDescription;
                    if (string.IsNullOrEmpty(model.SelectedService.MetaTitle))
                    {
                        ViewBag.MetaTitle = model.SelectedService.Title;
                    }

                    if (string.IsNullOrEmpty(model.SelectedService.MetaDescription))
                    {
                        ViewBag.MetaDescription = Regex.Replace(model.SelectedService.Description, @"<(.|\n)*?>", "");
                    }
                    ViewBag.PageTitle = model.SelectedService.Title + " - ";
                    model.SelectedService.Images = (from i in db.UploadedImages
                                                    where i.ReferenceId == model.SelectedService.Id && i.Type == "Service"
                                                    select i).ToList();
                    
                    model.SelectedService.CaseStudies = (from cs in db.CaseStudies
                                                         join scs in db.ServiceCaseStudies on cs.Id equals scs.CaseStudyId
                                                         where scs.ServiceId == model.SelectedService.Id && scs.Live != null && scs.Live == true
                                                         select cs).ToList();

                }
            }
            return model;
        }



    }
}
