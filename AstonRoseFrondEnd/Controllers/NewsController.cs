﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Controllers;
using System.Text.RegularExpressions;

namespace AstonRoseFrondEnd.Controllers
{
    public class NewsController : BaseController
    {
        //
        // GET: /News/
        //private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public ActionResult Index(int year = 0)
        {
            return View(GetModel(year));
        }

        public ActionResult Search(int year = 0)
        {
            return View(GetModel(year));
        }
        int pageSize = 3;
        public ActionResult Page(int pageIndex = 0, int pageCount = 0, int year = 0)
        {
            return View(GetModel(year, pageIndex, pageCount));
        }

        private NewsListModel GetModel(int year = 0, int pageIndex = 0, int pageCount = 0)
        {
            ViewBag.Year = year;

            NewsListModel model = new NewsListModel();
            model.FilteredList = GetArchivedNews();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                int cnt = (from n in db.News
                           where n.GoLive && (year == 0 || n.NewsDate.Year == year) && n.NewsDate.Year > 2008
                           orderby n.NewsDate descending
                           select n.Id).Count();
                ViewBag.PageCount = (cnt % pageSize == 0 ? cnt / pageSize : (cnt / pageSize) + 1);
                ViewBag.PageIndex = pageIndex;

                model.NewsList = (from n in db.News
                                  where n.GoLive && (year == 0 || n.NewsDate.Year == year)
                                  orderby n.NewsDate descending
                                  select n).Skip(pageIndex * pageSize).Take(pageSize).ToList();
                ViewBag.MetaTitle = "News : Aston Rose";
                ViewBag.MetaDescription = "News : Aston Rose";
                ViewBag.PageTitle = "News  - " ;
            }
            return model;
        }

        List<NewsFilter> GetArchivedNews()
        {
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                return db.Database.SqlQuery<NewsFilter>("select Year, Count from (SELECT DATEPART(Year, NewsDate) Year, Count(ID) [Count] " +
                    " FROM News where GoLive=1 " +
                    " GROUP BY DATEPART(Year, NewsDate) " +

                    " ) as NewsTable  where Year > 2008 ORDER BY Year desc").ToList();
            }
        }

        public ActionResult NewsDetail(string title = "", int Id = 0)
        {


            NewsListModel model = new NewsListModel();
            model.FilteredList = GetArchivedNews();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.SelectedNews = db.News.Find(Id);

                if (model.SelectedNews != null)
                {
                    ViewBag.MetaTitle = model.SelectedNews.MetaTitle;
                    ViewBag.MetaDescription = model.SelectedNews.MetaDescription;
                    if (string.IsNullOrEmpty(model.SelectedNews.MetaTitle))
                    {
                        ViewBag.MetaTitle = model.SelectedNews.Title;
                    }

                    if (string.IsNullOrEmpty(model.SelectedNews.MetaDescription))
                    {
                        ViewBag.MetaDescription = Regex.Replace(model.SelectedNews.FullDescription, @"<(.|\n)*?>", "");
                    }
                    ViewBag.PageTitle = model.SelectedNews.Title + " - ";
                }
            }
            return View(model);
        }

    }
}
