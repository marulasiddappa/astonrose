﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using System.Web.Security;
using System.Net.Mail;
using System.Configuration;
using System.Web.Script.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.ComponentModel;
using System.Net;
using System.Xml;
using System.Collections;
using System.Web.Routing;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using AstonRoseFrondEnd.Models;

namespace AstonRoseFrondEnd.Helpers
{
    public class SCHelpers : IDisposable
    {

        public static List<SelectListItem> TravelDistanceDropdown()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            
            _ReturnList.Add(new SelectListItem() { Text = "10 miles", Value = "10" });
            _ReturnList.Add(new SelectListItem() { Text = "20 miles", Value = "20" });
            _ReturnList.Add(new SelectListItem() { Text = "30 miles", Value = "30" });
            _ReturnList.Add(new SelectListItem() { Text = "40 miles", Value = "40" });
            _ReturnList.Add(new SelectListItem() { Text = "50 miles", Value = "50" });
            _ReturnList.Add(new SelectListItem() { Text = "60 miles", Value = "60" });
            _ReturnList.Add(new SelectListItem() { Text = "70 miles", Value = "70" });
            _ReturnList.Add(new SelectListItem() { Text = "80 miles", Value = "80" });
            _ReturnList.Add(new SelectListItem() { Text = "90 miles", Value = "90" });
            _ReturnList.Add(new SelectListItem() { Text = "100 miles plus", Value = "100" });
            
            return _ReturnList;
        }
        
        public static Property GetPropById(int id)
        {
            using (AstonRoseCMSContext context = new AstonRoseCMSContext())
            {
                Property _property = new Property();
                _property = (from j in context.Property
                        where j.Id == id
                        select j).FirstOrDefault();
                if (_property != null)
                {
                    Basket basket = Basket.GetCart(new HttpContextWrapper(HttpContext.Current));
                    _property.Added = basket.CheckIfAdded(_property.Id);
                }
                return _property;
            }
        }

       
        public static string RenderPartialToString(string controlName, object viewData)
        {
            ViewPage viewPage = new ViewPage() { ViewContext = new ViewContext() };
            viewPage.Url = GetBogusUrlHelper();

            viewPage.ViewData = new ViewDataDictionary(viewData);
            viewPage.Controls.Add(viewPage.LoadControl(controlName));

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (System.Web.UI.HtmlTextWriter tw = new System.Web.UI.HtmlTextWriter(sw))
                {
                    viewPage.RenderControl(tw);
                }
            }

            return sb.ToString();
        }

        public static UrlHelper GetBogusUrlHelper()
        {
            var httpContext = HttpContext.Current;

            if (httpContext == null)
            {
                var url = System.Configuration.ConfigurationManager.AppSettings["SiteUrl"];
                var request = new HttpRequest("/", url, "");
                var response = new HttpResponse(new StringWriter());
                httpContext = new HttpContext(request, response);
            }

            var httpContextBase = new HttpContextWrapper(httpContext);
            var routeData = new RouteData();
            var requestContext = new RequestContext(httpContextBase, routeData);

            return new UrlHelper(requestContext);
        }
        public static List<List<KeyValuePair<string, string>>> ChunkDict(Dictionary<string, string> theList, int chunkSize)
        {
            var result = theList.Select((x, i) =>
                new { data = x, indexgroup = i / chunkSize })
                .GroupBy(x => x.indexgroup, x => x.data)
                .Select(y => y.Select(x => x).ToList()).ToList();
            return result;
        }


        [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
        public class MustBeTrueAttribute : ValidationAttribute
        {
            public override bool IsValid(object value)
            {
                return value != null && value is bool && (bool)value;
            }
        }
        public static string EmptyIfStr(object value)
        {
            if (value == System.DBNull.Value | (value == null))
            {
                return "";
            }
            else
            {
                if (String.IsNullOrEmpty(value.ToString()) == true)
                {
                    return "";
                }
                else
                {
                    return value.ToString().Trim().Replace("\r", "");

                }
            }
        }
        public static int EmptyIfInt(object value)
        {
            try
            {
                if (Convert.IsDBNull(value) | (value == null))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(value.ToString());
                }
            }
            catch
            {
                return 0;
            }

        }

        #region IDisposable Members

        public void Dispose()
        {
            //db.Dispose();
        }

        #endregion

        public static int CurrentUserId()
        {
            string cookieName = null;
            cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = default(HttpCookie);

            authCookie = HttpContext.Current.Request.Cookies[cookieName];
            if ((authCookie != null))
            {
                FormsAuthenticationTicket authTicket = default(FormsAuthenticationTicket);
                authTicket = null;
                try
                {
                    int UserId = 0;
                    authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    string userDataString = authTicket.UserData;
                    string[] userDataPieces = userDataString.Split("|".ToCharArray());
                    // //0 - AccountType, 1-AccountTypeName, 2-AccountId, 3-AccountCode,4-AccoutEmail,5-FullName
                    UserId = Convert.ToInt32(userDataPieces[2]);
                    return UserId;
                }
                catch
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }

        public static string escapeQueryChars(string s)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                char c = s[i];
                // These characters are part of the query syntax and must be escaped
                if (c == '\\' || c == '+' || c == '-' || c == '!' || c == '(' || c == ')' || c == ':'
                  || c == '^' || c == '[' || c == ']' || c == '\"' || c == '{' || c == '}' || c == '~'
                  || c == '*' || c == '?' || c == '|' || c == '&' || c == ';'
                  || char.IsWhiteSpace(c))
                {
                    sb.Append('\\');
                }
                sb.Append(c);
            }
            return sb.ToString();
        }




        public static string GetCustomerCookiesValues(int cookieindex)
        {
            string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[cookieName];
            if ((authCookie != null))
            {
                FormsAuthenticationTicket authTicket = default(FormsAuthenticationTicket);
                authTicket = null;
                try
                {
                    authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    string userDataString = authTicket.UserData;
                    string[] userDataPieces = userDataString.Split("|".ToCharArray());
                    //0 - AccountType, 1-AccountTypeName, 2-AccoutId, 3-AccountCode,4-AccoutEmail,5-FullName
                    return userDataPieces[cookieindex];
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }


       

        public string JavaScriptStringEncode(string message)
        {
            if (String.IsNullOrEmpty(message))
            {
                return message;
            }

            StringBuilder builder = new StringBuilder();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.Serialize(message, builder);
            return builder.ToString(1, builder.Length - 2); // remove first + last quote
        }

        [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
        public sealed class PropertiesMustMatchAttribute : ValidationAttribute
        {
            private const string _defaultErrorMessage = "'{0}' and '{1}' do not match.";
            private readonly object _typeId = new object();

            public PropertiesMustMatchAttribute(string originalProperty, string confirmProperty)
                : base(_defaultErrorMessage)
            {
                OriginalProperty = originalProperty;
                ConfirmProperty = confirmProperty;
            }

            public string ConfirmProperty { get; private set; }
            public string OriginalProperty { get; private set; }

            public override object TypeId
            {
                get
                {
                    return _typeId;
                }
            }

            public override string FormatErrorMessage(string name)
            {
                return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString,
                    OriginalProperty, ConfirmProperty);
            }

            public override bool IsValid(object value)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
                object originalValue = properties.Find(OriginalProperty, true /* ignoreCase */).GetValue(value);
                object confirmValue = properties.Find(ConfirmProperty, true /* ignoreCase */).GetValue(value);
                return Object.Equals(originalValue, confirmValue);
            }
        }




        public string escapeSolrValue(string inputvalue)
        {
            if (String.IsNullOrEmpty(inputvalue))
            {
                return inputvalue;
            }
            else
            {

                try
                {
                    return Regex.Escape(inputvalue.ToString()) + @"(?<!\\)(?P<char>[&|+\-!(){}[\]^~*?:])";
                }
                catch
                {
                    return inputvalue;
                }

            }

        }

        public string getpricerangedisptext(string inputvalue)
        {

            if (String.IsNullOrEmpty(inputvalue))
            {
                return inputvalue;
            }
            else
            {
                string rtnPriceRange = "";

                switch (inputvalue.ToString())
                {
                    case "1":
                        rtnPriceRange = "1p to 25p";
                        break;
                    case "2":
                        rtnPriceRange = "25p to 50p";
                        break;
                    case "3":
                        rtnPriceRange = "50p to £1";
                        break;
                    case "4":
                        rtnPriceRange = "£1 to £2";
                        break;
                    case "5":
                        rtnPriceRange = "£2 to £4";
                        break;
                    case "6":
                        rtnPriceRange = "£4 to £6";
                        break;
                    case "7":
                        rtnPriceRange = "£6 to £10";
                        break;
                    case "8":
                        rtnPriceRange = "£10 to £15";
                        break;
                    case "9":
                        rtnPriceRange = "£15 to £20";
                        break;
                    case "10":
                        rtnPriceRange = "£20 to £50";
                        break;
                    case "11":
                        rtnPriceRange = "£50 to £100";
                        break;
                    case "12":
                        rtnPriceRange = "Under 25p";
                        break;
                    case "13":
                        rtnPriceRange = "Under 50p";
                        break;
                    case "14":
                        rtnPriceRange = "Under £1";
                        break;
                    case "15":
                        rtnPriceRange = "Under £2";
                        break;
                    case "16":
                        rtnPriceRange = "Under £4";
                        break;
                    case "17":
                        rtnPriceRange = "Under £6";
                        break;
                    case "18":
                        rtnPriceRange = "Over £6";
                        break;
                    case "19":
                        rtnPriceRange = "Over £10";
                        break;
                    case "20":
                        rtnPriceRange = "Over £15";
                        break;
                    case "21":
                        rtnPriceRange = "Over £20";
                        break;
                    case "22":
                        rtnPriceRange = "Over £50";
                        break;
                    case "23":
                        rtnPriceRange = "Over £100";
                        break;
                    default:
                        rtnPriceRange = "";
                        break;
                }
                return rtnPriceRange;

            }


        }

        public static List<SelectListItem> GetTitleDropdowns()
        {

            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Mr", Value = "Mr" });
            _ReturnList.Add(new SelectListItem() { Text = "Mrs", Value = "Mrs" });
            _ReturnList.Add(new SelectListItem() { Text = "Miss", Value = "Miss" });
            _ReturnList.Add(new SelectListItem() { Text = "Ms", Value = "Ms" });
            _ReturnList.Add(new SelectListItem() { Text = "Dr", Value = "Dr" });
            //if (Name == "Title")
            //{

            //    //_ReturnList.Add(new SelectListItem() { Text ="Select title", Value = "" });

            //}



            return _ReturnList;

        }


        public static string UrlFriendly(string title)
        {
            if (title == null) return "";

            const int maxlen = 80;
            int len = title.Length;
            bool prevdash = false;
            var sb = new StringBuilder(len);
            char c;

            for (int i = 0; i < len; i++)
            {
                c = title[i];
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
                {
                    sb.Append(c);
                    prevdash = false;
                }
                else if (c >= 'A' && c <= 'Z')
                {
                    // tricky way to convert to lowercase
                    sb.Append((char)(c | 32));
                    prevdash = false;
                }
                else if (c == ' ' || c == ',' || c == '.' || c == '/' ||
                    c == '\\' || c == '-' || c == '_' || c == '=')
                {
                    if (!prevdash && sb.Length > 0)
                    {
                        sb.Append('-');
                        prevdash = true;
                    }
                }
                else if ((int)c >= 128)
                {
                    int prevlen = sb.Length;
                    sb.Append(RemapInternationalCharToAscii(c));
                    if (prevlen != sb.Length) prevdash = false;
                }
                if (i == maxlen) break;
            }

            if (prevdash)
                return sb.ToString().Substring(0, sb.Length - 1);
            else
                return sb.ToString();
        }


        public static string RemapInternationalCharToAscii(char c)
        {
            string s = c.ToString().ToLowerInvariant();
            if ("àåáâäãåą".Contains(s))
            {
                return "a";
            }
            else if ("èéêëę".Contains(s))
            {
                return "e";
            }
            else if ("ìíîïı".Contains(s))
            {
                return "i";
            }
            else if ("òóôõöøőð".Contains(s))
            {
                return "o";
            }
            else if ("ùúûüŭů".Contains(s))
            {
                return "u";
            }
            else if ("çćčĉ".Contains(s))
            {
                return "c";
            }
            else if ("żźž".Contains(s))
            {
                return "z";
            }
            else if ("śşšŝ".Contains(s))
            {
                return "s";
            }
            else if ("ñń".Contains(s))
            {
                return "n";
            }
            else if ("ýÿ".Contains(s))
            {
                return "y";
            }
            else if ("ğĝ".Contains(s))
            {
                return "g";
            }
            else if (c == 'ř')
            {
                return "r";
            }
            else if (c == 'ł')
            {
                return "l";
            }
            else if (c == 'đ')
            {
                return "d";
            }
            else if (c == 'ß')
            {
                return "ss";
            }
            else if (c == 'Þ')
            {
                return "th";
            }
            else if (c == 'ĥ')
            {
                return "h";
            }
            else if (c == 'ĵ')
            {
                return "j";
            }
            else
            {
                return "";
            }
        }

        public static string GenerateRandomDigitCode(int length)
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < length; i++)
                str = String.Concat(str, random.Next(10).ToString());
            return str;
        }

    }
}