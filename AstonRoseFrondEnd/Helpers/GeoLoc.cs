﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for GeoLoc
/// </summary>
namespace AstonRoseFrondEnd.Helpers
{
    public class GeoLoc
    {
        public GeoLoc()
        {
        }
        public GeoLoc(double lat, double lon, string mapURL)
        {
            Lat = lat;
            Lon = lon;
            MapURL = mapURL;
        }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string MapURL { get; set; }
        public override string ToString()
        {
            return "Latitude: " + Lat.ToString() + " Longitude: " + Lon.ToString();
        }
        public string ToQueryString()
        {
            return "+to:" + Lat + "%2B" + Lon;
        }
    }
}