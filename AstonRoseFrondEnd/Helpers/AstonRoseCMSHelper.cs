﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;
using System.Globalization;
using AstonRoseFrondEnd.Models;

namespace AstonRoseFrondEnd.Helpers
{
    public class AstonRoseCMSHelper
    {
        public static List<SelectListItem> GetTitleDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Mr", Value = "Mr" });
            _ReturnList.Add(new SelectListItem() { Text = "Mrs", Value = "Mrs" });
            _ReturnList.Add(new SelectListItem() { Text = "Miss", Value = "Miss" });
            _ReturnList.Add(new SelectListItem() { Text = "Ms", Value = "Ms" });
            _ReturnList.Add(new SelectListItem() { Text = "Dr", Value = "Dr" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyRegionDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "London (Central)", Value = "London (Central)" });
            _ReturnList.Add(new SelectListItem() { Text = "London (Greater)", Value = "London (Greater)" });
            _ReturnList.Add(new SelectListItem() { Text = "South", Value = "South" });
            _ReturnList.Add(new SelectListItem() { Text = "South East", Value = "South East" });
            _ReturnList.Add(new SelectListItem() { Text = "South West", Value = "South West" });
            _ReturnList.Add(new SelectListItem() { Text = "East Anglia", Value = "East Anglia" });
            _ReturnList.Add(new SelectListItem() { Text = "East Midlands", Value = "East Midlands" });
            _ReturnList.Add(new SelectListItem() { Text = "West Midlands", Value = "West Midlands" });
            _ReturnList.Add(new SelectListItem() { Text = "Wales", Value = "Wales" });
            _ReturnList.Add(new SelectListItem() { Text = "North West", Value = "North West" });
            _ReturnList.Add(new SelectListItem() { Text = "North East", Value = "North East" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetExistingRegionDropdowns()
        {
            using (AstonRoseCMSContext context = new AstonRoseCMSContext())
            {
                var existingRegions = (from r in context.Property
                                       select new SelectListItem { Text = r.Region, Value = r.Region }).Distinct().ToList();
                return existingRegions;
            }
        }

        public static List<SelectListItem> GetPropertyTypeDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "All", Value = "" });
            _ReturnList.Add(new SelectListItem() { Text = "Offices", Value = "Offices" });
            _ReturnList.Add(new SelectListItem() { Text = "Retail", Value = "Retail" });
            _ReturnList.Add(new SelectListItem() { Text = "Industrial", Value = "Industrial" });
            _ReturnList.Add(new SelectListItem() { Text = "Leisure", Value = "Leisure" });
            _ReturnList.Add(new SelectListItem() { Text = "Mixed Use", Value = "Mixed Use" });
            _ReturnList.Add(new SelectListItem() { Text = "Land", Value = "Land" });
            _ReturnList.Add(new SelectListItem() { Text = "Residential", Value = "Residential" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyCategoryDropdowns(bool includeEmpty = false)
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            if (includeEmpty)
            {
                _ReturnList.Add(new SelectListItem() { Text = "All", Value = "" });
            }
            _ReturnList.Add(new SelectListItem() { Text = "Sales/Lettings", Value = "Sales Let" });
            _ReturnList.Add(new SelectListItem() { Text = "Investment", Value = "Investment" });

            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyTenureDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "All Tenures", Value = "" });
            _ReturnList.Add(new SelectListItem() { Text = "Sale", Value = "Sale" });
            _ReturnList.Add(new SelectListItem() { Text = "To let", Value = "To Let" });
            _ReturnList.Add(new SelectListItem() { Text = "Sale/To let", Value = "Sale To Let" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyStatusDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Available", Value = "Available" });
            _ReturnList.Add(new SelectListItem() { Text = "Under Offer", Value = "Under Offer" });
            _ReturnList.Add(new SelectListItem() { Text = "Reserved", Value = "Reserved" });
            _ReturnList.Add(new SelectListItem() { Text = "Sold", Value = "Sold" });
            _ReturnList.Add(new SelectListItem() { Text = "Let", Value = "Let" });
            _ReturnList.Add(new SelectListItem() { Text = "Not Released", Value = "Not Released" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertySizeDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "All Sizes", Value = "" });
            _ReturnList.Add(new SelectListItem() { Text = "250-500 sq ft (23.23-46.45 sq m)", Value = "250-500 sq ft (23.23-46.45 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "500-2,000 sq ft (46.45-185.80 sq m)", Value = "500-2,000 sq ft (46.45-185.80 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "2,000-5,000 sq ft (185.80-464.50 sq m)", Value = "2,000-5,000 sq ft (185.80-464.50 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "5,000-10,000 sq ft (464.5-929.03 sq m)", Value = "5,000-10,000 sq ft (464.5-929.03 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "10,000-15,000 sq ft (929.03-1393.50 sq m)", Value = "10,000-15,000 sq ft (929.03-1393.50 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "15,000 sq ft + (1393.50 sq m +)", Value = "15,000 sq ft + (1393.50 sq m +)" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetTeamTypeDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Director", Value = "Director" });
            _ReturnList.Add(new SelectListItem() { Text = "Team Member", Value = "Team Member" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetDistanceDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "All Distances", Value = "9999" });
            _ReturnList.Add(new SelectListItem() { Text = "1 miles", Value = "1" });
            _ReturnList.Add(new SelectListItem() { Text = "2 miles", Value = "2" });
            _ReturnList.Add(new SelectListItem() { Text = "5 miles", Value = "5" });
            _ReturnList.Add(new SelectListItem() { Text = "10 miles", Value = "10" });
            _ReturnList.Add(new SelectListItem() { Text = "20 miles", Value = "20" });
            _ReturnList.Add(new SelectListItem() { Text = "30 miles", Value = "30" });
            _ReturnList.Add(new SelectListItem() { Text = "50 miles", Value = "50" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Director", Value = "Director" });
            _ReturnList.Add(new SelectListItem() { Text = "Team Member", Value = "Team Member" });
            return _ReturnList;
        }

        public static List<string> GetCountryList()
        {
            List<string> list = new List<string>();
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo info in cultures)
            {
                //if (info.IsNeutralCulture) continue;
                RegionInfo info2 = new RegionInfo(info.LCID);
                if (!list.Contains(info2.EnglishName))
                {
                    list.Add(info2.EnglishName);
                }
            }
            list = (from l in list orderby l ascending select l).ToList();
            return list;
        }

        public static List<System.Web.Mvc.SelectListItem> GetDisplayOrder()
        {
            List<System.Web.Mvc.SelectListItem> _ReturnList = new List<System.Web.Mvc.SelectListItem>();
            for (int i = 1; i <= 20; i++)
            {
                _ReturnList.Add(new System.Web.Mvc.SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }

            return _ReturnList;
        }

        public static Team GetContact(string contacts)
        {
            Team _return = null;
            if (contacts != null)
            {
                var values = contacts.Split(',');

                using (AstonRoseCMSContext db = new AstonRoseCMSContext())
                {
                    if (values.Length > 0 && !string.IsNullOrEmpty(values[0]))
                    {
                        int teamId = int.Parse(values[0].Trim());
                        _return = db.Team.Find(teamId);
                    }
                }
            }
            if (_return == null)
                _return = new Team();
            return _return;
        }
        public static IEnumerable<Property> GetProperties()
        {
            using (AstonRoseCMSContext context = new AstonRoseCMSContext())
            {
                return context.Property.Take(10).ToList();
            }
        }

    }

    public class FloorAreas
    {
        public FloorAreas()
        {
            FloorAreaFValues = new List<FloorArea>();
            FloorAreaFValues.Add(new FloorArea { Area = "250-500 sq ft (23.23-46.45 sq m)", Min = 250, Max = 500 });
            FloorAreaFValues.Add(new FloorArea { Area = "500-2,000 sq ft (46.45-185.80 sq m)", Min = 500, Max = 2000 });
            FloorAreaFValues.Add(new FloorArea { Area = "2,000-5,000 sq ft (185.80-464.50 sq m)", Min = 2000, Max = 5000 });
            FloorAreaFValues.Add(new FloorArea { Area = "5,000-10,000 sq ft (464.5-929.03 sq m)", Min = 5000, Max = 10000 });
            FloorAreaFValues.Add(new FloorArea { Area = "10,000-15,000 sq ft (929.03-1393.50 sq m)", Min = 10000, Max = 15000 });
            FloorAreaFValues.Add(new FloorArea { Area = "15,000 sq ft + (1393.50 sq m +)", Min = 15000, Max = int.MaxValue });



            FloorAreaMValues = new List<FloorArea>();
            FloorAreaMValues.Add(new FloorArea { Area = "250-500 sq ft (23.23-46.45 sq m)", Min = 23, Max = 47 });
            FloorAreaMValues.Add(new FloorArea { Area = "500-2,000 sq ft (46.45-185.80 sq m)", Min = 46, Max = 186 });
            FloorAreaMValues.Add(new FloorArea { Area = "2,000-5,000 sq ft (185.80-464.50 sq m)", Min = 185, Max = 465 });
            FloorAreaMValues.Add(new FloorArea { Area = "5,000-10,000 sq ft (464.5-929.03 sq m)", Min = 464, Max = 930 });
            FloorAreaMValues.Add(new FloorArea { Area = "10,000-15,000 sq ft (929.03-1393.50 sq m)", Min = 929, Max = 1394 });
            FloorAreaMValues.Add(new FloorArea { Area = "15,000 sq ft + (1393.50 sq m +)", Min = 1393, Max = int.MaxValue });

        }
        public List<FloorArea> FloorAreaFValues { get; set; }
        public List<FloorArea> FloorAreaMValues { get; set; }


    }
    public class FloorArea
    {
        public string Area { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
    }

}