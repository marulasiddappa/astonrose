﻿

using System.Drawing;
using System.Drawing.Text;
using System;
using System.Web.Mvc;
using System.IO;

namespace AstonRoseFrondEnd.Helpers
{
    public static class StringHelper {
        public static int TryParse(string u, int defaultValue) {
            try {
                return int.Parse(u);
            } catch {
                return defaultValue;
            }
        }

        public static int TryParse(string u) {
            return TryParse(u, 0);
        }

		public static double EmptyIfDbl(object value)
		{
			try
			{
				if (Convert.IsDBNull(value) | (value == null))
				{
					return 0;
				}
				else
				{
					return Convert.ToDouble(value.ToString());
				}
			}
			catch
			{
				return 0;
			}
		}

        /// <summary>
        /// Like null coalescing operator (??) but including empty strings
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string IfNullOrEmpty(string a, string b) {
            return string.IsNullOrEmpty(a) ? b : a;
        }

        /// <summary>
        /// If <paramref name="a"/> is empty, returns null
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string EmptyToNull(string a) {
            return string.IsNullOrEmpty(a) ? null : a;
        }
		public static string GetHEXAColor(string colorname)
		{
			try
			{
				//string[] colorArray = Enum.GetNames(typeof(KnownColor));
				//style="background: none repeat scroll 0 0 #001CEF;"
				Color colorValue = ColorTranslator.FromHtml(colorname);
				string htmlHexColorValueThree = String.Format("#{0:X2}{1:X2}{2:X2}", colorValue.R, colorValue.G, colorValue.B);
				htmlHexColorValueThree = "background:" + htmlHexColorValueThree+";";
				return htmlHexColorValueThree;
			}
			catch
			{
				return "";
			}
		}


		public static string RenderPartialView(this Controller controller, string viewName, object model)
		{
			if (string.IsNullOrEmpty(viewName))
				viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

			controller.ViewData.Model = model;
			using (var sw = new StringWriter())
			{
				ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
				var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
				viewResult.View.Render(viewContext, sw);

				return sw.GetStringBuilder().ToString();
			}
		}


    }
}