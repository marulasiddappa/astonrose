﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace AstonRoseFrondEnd.Helpers
{
	public static class SessionExtensions
	{
		public static T GetDataFromSession<T>(this HttpSessionStateBase session, string key)
		{
			return (T)session[key];
		}

		public static T SetDataInSession<T>(this HttpSessionStateBase session, string key, object value)
		{
			try
			{
				session[key] = value;
				
			}
			catch
			{
				session[key] = null;
				
			}
			return (T)session[key];
		}

	}
}