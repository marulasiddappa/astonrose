﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;

/// <summary>
/// Summary description for GeoCoder
/// </summary>
namespace AstonRoseFrondEnd.Helpers
{
    public class GeoCoder
    {
        public static string googleMapsKey = ConfigurationManager.AppSettings["GoogleMapsKey"];
        public GeoCoder()
        {
            //
            // TODO: Add constructor logic here
            //
        }



        //Old Method
        //public static GeoLoc LocateGoogle(string query)
        //{
        //    string url = "http://maps.google.co.uk/maps/geo?q={0}&output=xml&key=" + googleMapsKey;
        //    url = String.Format(url, query);
        //    XmlNode coords = null;

        //    String xmlString = GetUrl(url);
        //    XmlDocument xd = new XmlDocument();
        //    xd.LoadXml(xmlString);
        //    XmlNamespaceManager xnm = new XmlNamespaceManager(xd.NameTable);

        //    coords = xd.GetElementsByTagName("coordinates")[0];

        //    GeoLoc loc = null;

        //    if (coords != null)
        //    {
        //        String[] coordinateArray = coords.InnerText.Split(',');
        //        if (coordinateArray.Length >= 2)
        //        {
        //            loc = new GeoLoc();
        //            Double Lat = Convert.ToDouble(coordinateArray[1]);
        //            Double Lon = Convert.ToDouble(coordinateArray[0]);
        //            loc.Lon = Lon;
        //            loc.Lat = Lat;
        //            loc.MapURL = url;
        //        }
        //    }

        //    return loc;
        //}

        public static GeoLoc LocateGoogle(string query)
        {
            string url = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=true&address={0}";
            url = String.Format(url, query);
            XmlNode coords = null;

            String xmlString = GetUrl(url);
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlString);
            XmlNamespaceManager xnm = new XmlNamespaceManager(xd.NameTable);
            coords = xd.GetElementsByTagName("geometry")[0];
            GeoLoc loc = null;
            if (coords != null)
            {
                coords = coords.FirstChild;
                if (coords != null)
                {
                    loc = new GeoLoc();
                    Double Lat = 0;
                    Double Lon = 0;
                    if (coords.FirstChild != null)
                        Lat = Convert.ToDouble(coords.FirstChild.InnerText);
                    if (coords.LastChild != null)
                        Lon = Convert.ToDouble(coords.LastChild.InnerText);
                    loc.Lon = Lon;
                    loc.Lat = Lat;
                    loc.MapURL = url;
                }
            }

            return loc;
        }
        private static string GetUrl(string url)
        {
            string result = "";
            System.Net.WebClient client = new System.Net.WebClient();
            Stream strm = client.OpenRead(url);
            StreamReader sr = new StreamReader(strm);
            result = sr.ReadToEnd();
            return result;
        }
        public static GeoLoc LocateGoogle(string address, string suburb, string state)
        {
            return LocateGoogle(address + "," + suburb + "," + state + ",United Kingdom");
        }
        public static GeoLoc LocateGooglePostcode(string postcode)
        {
            return LocateGoogle(postcode + ",United Kingdom");
        }


    }

    public class CDistanceBetweenLocations
    {
        //Public Shared Function Calc(ByVal Lat1 As Double, ByVal Long1 As Double, ByVal Lat2 As Double, ByVal Long2 As Double) As Double
        public static double Calc(double Lat1, double Long1, double Lat2, double Long2)
        {

            double dDistance = double.MinValue;
            double dLat1InRad = Lat1 * (Math.PI / 180.0);
            double dLong1InRad = Long1 * (Math.PI / 180.0);
            double dLat2InRad = Lat2 * (Math.PI / 180.0);
            double dLong2InRad = Long2 * (Math.PI / 180.0);
            double dLongitude = dLong2InRad - dLong1InRad;
            double dLatitude = dLat2InRad - dLat1InRad;


            double a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) + Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) * Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);


            double c = 2.0 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1.0 - a));


            dDistance = 6376.5 + c;
            return dDistance;

        }
        public static double Calc(string NS1, double Lat1, double Lat1Min, string EW1, double Long1, double Long1Min,
            string NS2, double Lat2, double Lat2Min, string EW2, double Long2, double Long2Min)
        {
            double NS1Sign = 1.0;
            double EW1Sign = 1.0;
            double NS2Sign = 1.0;
            double EW2Sign = 1.0;
            return (Calc((Lat1 + (Lat1Min / 60)) * NS1Sign, (Long1 + (Long1Min / 60)) * EW1Sign, (Lat2 + (Lat2Min / 60)) * NS2Sign, (Long2 + (Long2Min / 60)) * EW2Sign));
        }

    }
}