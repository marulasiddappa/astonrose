;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  $(document).ready(function() {
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationButtons          ? $doc.foundationButtons() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
    $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
    $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
    $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
    $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
    $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
    $.fn.foundationClearing         ? $doc.foundationClearing() : null;

    $.fn.placeholder                ? $('input, textarea').placeholder() : null;
  });

  // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
  $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
  $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
  $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
  $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }

})(jQuery, this);


var mapOpen=false;

$(function () {

    $(".toggle-menu").click(function () {
        $('.g-nav ul').slideToggle('2000', "swing");
        $(this).toggleClass("active");
    });

    $('.news-filter li:has(ul) > a').click(function () {
        $(this).siblings('ul').toggle();
        $(this).toggleClass("active");
        return false;
    });

    $(".refine-but").click(function () {
        if ($(".map-panel").is(':visible')) {
            $(".map-panel").hide();
        }
        $(".refine-panel").slideToggle();
        $(this).toggleClass("active");
        return false;
    });

    $(".map-but").click(function () {
        if ($(".refine-panel").is(':visible')) {
            $(".refine-panel").hide();
        }
        $(".map-panel").slideToggle();
        $(this).toggleClass("active");
        google.maps.event.trigger(map, "resize");
        

        return false;
    });

    $('.lrg-image div').hide();
    $('.lrg-image div:first').show();
    $('.thumbs li:first, .tbn-image li:first').addClass('active');
    $('.tbn-image li a').click(function () {
        $('.tbn-image li').removeClass('active');
        $(this).parent().addClass('active');
        var currentTab = $(this).attr('href');
        $('.lrg-image div').hide();
        $(currentTab).fadeIn();
        return false;
    });

    $(".map-but").click(function () {
        if ($(".pic-panel").is(':visible')) {
            $(".pic-panel").hide();
        }
        $(".map-panel-dets").show();
        $(this).toggleClass("active");
        google.maps.event.trigger(map, "resize");
        return false;
    });

    $(".pic-but").click(function () {
        if ($(".map-panel-dets").is(':visible')) {
            $(".map-panel-dets").hide();
        }
        $(".pic-panel").show();
        $(this).toggleClass("active");
        return false;
    });





});

$(document).ready(function(){
$(".account").click(function(){
	var X=$(this).attr('id');
	if(X==1){
	$(".submenu").hide();
	$(this).attr('id', '0');	
	}
	else{$(".submenu").show();
	$(this).attr('id', '1');
	}
});

//Mouseup textarea false
$(".submenu").mouseup(function(){
return false
});
$(".account").mouseup(function(){
return false
});

//Textarea without editing.
$(document).mouseup(function(){
$(".submenu").hide();
$(".account").attr('id', '');
});
	
});


