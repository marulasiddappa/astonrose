﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net.Config;
using Microsoft.Practices.Unity;
using Microsoft.Practices.ServiceLocation;

using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;
using SolrNet.Impl;


using System.Web.Security;
using System.Security.Principal;


using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;
using SolrNet.Impl;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Helpers;
using AstonRoseFrondEnd.Models.Binders;
using AstonRoseFrondEnd.IoC;



namespace AstonRoseFrondEnd
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            //routes.MapRoute(
            //    "Property-Search",
            //            "properties/{action}/{q}",
            //            new { controller = "Prop", action = "PropertyResults", q = "", proptype = UrlParameter.Optional, postcode = UrlParameter.Optional }
            //            );


            routes.MapRoute(
                "Property2", // Route name
                "property/searchresult/{location}/{propertyType}/{distance}/{tenure}/{region}/{floorArea}/{price}", // URL with parameters
                new
                {
                    controller = "property",
                    action = "searchresult",
                    location = UrlParameter.Optional,
                    propertyType = UrlParameter.Optional,
                    distance = UrlParameter.Optional,
                    tenure = UrlParameter.Optional,
                    region = UrlParameter.Optional,
                    floorArea = UrlParameter.Optional,
                    price = UrlParameter.Optional,
                } // Parameter defaults
            );

            routes.MapRoute(
                "Property1", // Route name
                "property/quicksearch/{location}/{propertyType}/{distance}", // URL with parameters
                new { controller = "property", action = "quicksearch", location = UrlParameter.Optional, propertyType = UrlParameter.Optional, distance = UrlParameter.Optional } // Parameter defaults
            );
            routes.MapRoute(
                "propertydetail", // Route name
                "property/detail/{name}/{id}", // URL with parameters
                new { controller = "property", action = "detail", name = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "property", // Route name
                "property/index", // URL with parameters
                new { controller = "property", action = "index" } // Parameter defaults
            );

            routes.MapRoute(
            "home", // Route name
            "home/{action}/{name}/{id}", // URL with parameters
            new { controller = "home", action = "index", name = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
        );

            routes.MapRoute(
      "news1", // Route name
      "news/search/{year}/{month}", // URL with parameters
      new { controller = "news", action = "search", year = UrlParameter.Optional, month = UrlParameter.Optional } // Parameter defaults
  );

            routes.MapRoute(
         "news2", // Route name
         "news/newsdetail/{title}/{id}", // URL with parameters
         new { controller = "news", action = "newsdetail", title = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
     );

            routes.MapRoute(
                   "news3", // Route name
                   "news/page/{pageindex}/{pagecount}/{year}/{month}", // URL with parameters
                   new { controller = "news", action = "page", pageIndex = UrlParameter.Optional, pageCount = UrlParameter.Optional, year = UrlParameter.Optional, month = UrlParameter.Optional } // Parameter defaults
               );

            routes.MapRoute(
              "news", // Route name
              "news/index", // URL with parameters
              new { controller = "news", action = "index" } // Parameter defaults
          );




            routes.MapRoute(
    "CSR1", // Route name
    "csr/search/{year}/{month}", // URL with parameters
    new { controller = "csr", action = "search", year = UrlParameter.Optional, month = UrlParameter.Optional } // Parameter defaults
);

            routes.MapRoute(
         "CSR2", // Route name
         "csr/csrdetail/{title}/{id}", // URL with parameters
         new { controller = "csr", action = "csrdetail", title = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
     );

            routes.MapRoute(
                   "csr3", // Route name
                   "csr/page/{pageindex}/{pagecount}/{year}/{month}", // URL with parameters
                   new { controller = "csr", action = "page", pageIndex = UrlParameter.Optional, pageCount = UrlParameter.Optional, year = UrlParameter.Optional, month = UrlParameter.Optional } // Parameter defaults
               );

            routes.MapRoute(
              "csr", // Route name
              "csr/index", // URL with parameters
              new { controller = "csr", action = "index" } // Parameter defaults
          );




            routes.MapRoute(
                "services1", // Route name
                "services/select/{title}/{id}", // URL with parameters
                new { controller = "services", action = "select", title = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
            );
            routes.MapRoute(
                "services", // Route name
                "services/index", // URL with parameters
                new { controller = "services", action = "index" } // Parameter defaults
            );


            routes.MapRoute(
                            "clienttypes1", // Route name
                            "clienttypes/select/{title}/{id}", // URL with parameters
                            new { controller = "clienttypes", action = "select", title = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
                        );

            routes.MapRoute(
                "clienttypes", // Route name
                "clienttypes/index", // URL with parameters
                new { controller = "clienttypes", action = "index" } // Parameter defaults
            );



            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "home", action = "index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        private static readonly string solrUrl = ConfigurationManager.AppSettings["solrUrl"];
        protected void Application_Start()
        {
           
            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Path.Combine(Server.MapPath("/"), "log4net.config")));

            RegisterRoutes(RouteTable.Routes);

            
            var connectionproperty = new SolrConnection(solrUrl);
            var loggingConnection1 = new LoggingConnection(connectionproperty);
            Startup.Init<SolrProperty>(loggingConnection1);



            RegisterAllControllers();
            IUnityContainer container = GetUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));



            ControllerBuilder.Current.SetControllerFactory(new ServiceProviderControllerFactory(Startup.Container));
            ModelBinders.Binders[typeof(SearchParameters)] = new SearchParametersBinder();



        }
        public IController GetContainerRegistration(IServiceProvider container, Type t)
        {
            var constructor = t.GetConstructors()[0];
            var dependencies = constructor.GetParameters().Select(p => container.GetService(p.ParameterType)).ToArray();
            return (IController)constructor.Invoke(dependencies);
        }

        public string GetControllerName(Type t)
        {
            return Regex.Replace(t.Name, "controller$", "", RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Registers controllers in the DI container
        /// </summary>
        public void RegisterAllControllers()
        {
            var controllers = typeof(MvcApplication).Assembly.GetTypes().Where(t => typeof(IController).IsAssignableFrom(t));
            foreach (var controller in controllers)
            {
                Type controllerType = controller;
                Startup.Container.Register(GetControllerName(controller), controller, c => GetContainerRegistration(c, controllerType));
            }

        }
        private IUnityContainer GetUnityContainer()
        {

            IUnityContainer container = new UnityContainer();


            return container;
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (Context.Response.StatusCode == 401)
            { // this is important, because the 401 is not an error by default!!!
                throw new HttpException(401, "You are not authorised");
            }
        }
    }
}