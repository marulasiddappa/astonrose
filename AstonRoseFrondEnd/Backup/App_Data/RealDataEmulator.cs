﻿using System;
using System.Collections.Generic;

namespace Mvc.Googlemap.Examples.App_Data
{
    class RealDataEmulator
    {
        private double _min;
        private double _max;
        private double _value;
        private double _maxIncrement;
        private double _maxDecrement;
        private double _direction;
        private readonly Random _rnd = new Random();
        private readonly Random _rndDirection = new Random();
        private readonly RangeList _rangeList = new RangeList();

        #region Constructors
        private void Initialzation(double minValue, double maxValue,
                                    double currentValue,
                                    double maxIncrement, 
                                    double maxDecrement)
        {
            _min = minValue;
            _max = maxValue;

            _value = currentValue;
            _maxIncrement = maxIncrement;
            _maxDecrement = maxDecrement;

            _direction = 1d;
        }

        public RealDataEmulator(double minValue, double maxValue,
            double currentValue,
            double maxIncrement, double maxDecrement)
        {
            Initialzation(minValue, maxValue,
                currentValue,
                maxIncrement, maxDecrement);
        }

        public RealDataEmulator(double minValue, double maxValue, double currentValue)
        {
            double increment = (maxValue - minValue) / 10d;
            Initialzation(minValue, maxValue, currentValue, increment, increment);
        }

        public RealDataEmulator(double minValue, double maxValue)
        {
            Initialzation(minValue, maxValue, minValue,
                (maxValue - minValue) / 10d,
                (maxValue - minValue) / 10d);
        }

        public RealDataEmulator()
        {
            Initialzation(0d, 100d, 0d, 10d, 10d);
        }
        #endregion

        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private void SetDirection()
        {
            double range = _rangeList.GetDirectionPriorityValue(_value);
            double directionValue = _rndDirection.NextDouble();
            _direction = 1d;

            if (directionValue > 0.5 - range / 2
                && directionValue < 0.5 + range / 2)
                _direction = -_direction;
        }

        private double GetNormalizedValue()
        {
            double nextValue = _rnd.NextDouble();
            if (_direction > 0)
            {
                nextValue *= _rangeList.MaxIncrement < 0 ? _maxIncrement : _rangeList.MaxIncrement;
            }
            else
            {
                nextValue *= _rangeList.MaxDecrement < 0 ? _maxDecrement : _rangeList.MaxDecrement;
            }

            return _direction * nextValue;
        }

        public double GetNextValue()
        {
            SetDirection();
            double stepValue = GetNormalizedValue();

            double nextValue = _value + stepValue;

            if (nextValue > _max)
                nextValue = _max - Math.Abs(stepValue);

            if (nextValue < _min)
                nextValue = _min + Math.Abs(stepValue);

            _value = nextValue;

            if (_value > _max)
                _value = _max;

            if (_value < _min)
                _value = _min;

            return _value;
        }

        public void AddRange(double min, double max, double directionPriority,
            double maxIncrement, double maxDecrement)
        {
            _rangeList.AddRange(min, max, directionPriority, maxIncrement, maxDecrement);
        }

        public void AddRange(double min, double max, double directionPriority)
        {
            AddRange(min, max, directionPriority, -1, -1);
        }

        class RangeList
        {
            private readonly List<Range> _rangeList = new List<Range>();
            public double MaxIncrement;
            public double MaxDecrement;

            public double GetDirectionPriorityValue(double value)
            {
                const double directionPriority = 0.5;
                MaxIncrement = -1;
                MaxDecrement = -1;

                foreach (Range range in _rangeList)
                {
                    double newPriority = range.GetDirectionPriorityValue(value);
                    if (newPriority != 0)
                    {
                        MaxIncrement = range.MaxIncrement;
                        MaxDecrement = range.MaxDecrement;
                        return newPriority;
                    }
                }

                return directionPriority;
            }

            public void AddRange(double min, double max, double directionPriority,
                double maxIncrement, double maxDecrement)
            {
                _rangeList.Add(new Range(min, max, directionPriority, maxIncrement, maxDecrement));
            }

            class Range
            {
                private readonly double _min;
                private readonly double _max;
                private readonly double _directionPriority;
                public readonly double MaxIncrement;
                public readonly double MaxDecrement;

                public Range(double min, double max, double directionPriority,
                    double maxIncrement, double maxDecrement)
                {
                    _min = min;
                    _max = max;
                    _directionPriority = directionPriority;

                    MaxIncrement = maxIncrement;
                    MaxDecrement = maxDecrement;
                }

                public double GetDirectionPriorityValue(double value)
                {
                    return !IsInRange(value) ? 0d : _directionPriority;
                }

                private bool IsInRange(double value)
                {
                    return value >= _min && value <= _max;
                }
            }
        }
    }
}