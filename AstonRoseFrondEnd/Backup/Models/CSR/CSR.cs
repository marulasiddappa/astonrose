﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class CSR
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is Required")]
        [Display(Name = "Title")]
        [AllowHtml]
        public string Title { get; set; }

        [Required(ErrorMessage = "Date is Required")]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        //[Required(ErrorMessage = "Short Description is Required")]
        //[Display(Name = "Short Description")]
        //[AllowHtml]
        //public string ShortDescription { get; set; }

        [Required(ErrorMessage = "Full Description is Required")]
        [Display(Name = "Full Description")]
        [AllowHtml]
        public string FullDescription { get; set; }

        [Display(Name = "Image")]
        public string Image { get; set; }

        //[Required(ErrorMessage = "Video is Required")]
        [Display(Name = "Video URL")]
        public string Video { get; set; }

        [Display(Name = "Display on Home Page")]
        public bool HomePageDisplay { get; set; }

        [Display(Name = "Go Live")]
        public bool GoLive { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }
    }
}