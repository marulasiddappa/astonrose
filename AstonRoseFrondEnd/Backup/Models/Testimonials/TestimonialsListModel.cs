﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class TestimonialsListModel
    {
        public GridModel<Testimonials> TestimonialsList { get; set; }
    }
}