﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class ContactModel
    {
        
        public List<Contact> Contacts { get; set; }
        public List<Offices> Offices { get; set; }
    }
    public class Contact
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
       
        public string MetaTitle { get; set; }

       
        public string MetaDescription { get; set; }

    }

    

    
}