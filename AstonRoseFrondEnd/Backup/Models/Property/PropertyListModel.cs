﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;
using AstonRoseFrondEnd.Models;

namespace AstonRoseFrondEnd.Models
{
    public class PropertyListModel
    {
        public GridModel<Property> PropertyList { get; set; }
        public Property SelectedProperty { get; set; }
        public List<UploadedImage> UploadedImages { get; set; }
        public Map Map { get; set; }

        public List<Team> Contacts { get; set; }
        public PropertyFilter Filter { get; set; }
        public EnquiryUser Enquiry { get; set; }

    }

    public class PropertyFilter : Pagination
    {
        public string Proplocation { get; set; }
        public string Proptype { get; set; }
        public string Distance { get; set; }
        public string Tenure { get; set; }
        public string[] Region { get; set; }
        public string PropsizeF { get; set; }
        public decimal FloorMinF { get; set; }
        public decimal FloorMaxF { get; set; }
        public string PropsizeM { get; set; }
        public decimal FloorMinM { get; set; }
        public decimal FloorMaxM { get; set; }
        public string Category { get; set; }

    }

    public class Pagination
    {
        public int CurrentPageIndex { get; set; }
        public int TotalPages { get; set; }
    }
}