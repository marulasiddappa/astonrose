﻿using System.Collections.Generic;
using AstonRoseFrondEnd.Helpers;


namespace AstonRoseFrondEnd.Models
{
    public class SearchParameters
    {

        public static int DefaultPageSize = SCHelpers.EmptyIfInt(System.Configuration.ConfigurationManager.AppSettings["DefaultPageSize"]);

        public SearchParameters()
        {
            Facets = new Dictionary<string, string>();
            PageSize = GetDefaultPageSize;
            PageIndex = 1;
            PostCode = GetDefaultPostCode;
            Distance = GetDefaultDistance;
        }

        public string PageDisp { get; set; }
        public string FreeSearch { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public IDictionary<string, string> Facets { get; set; }

        public int Distance { get; set; }
        public string PostCode { get; set; }

        public IEnumerable<string> MultiValueList { get; set; }
        public string MultiSearch { get; set; }

        public string Sort { get; set; }

        public int FirstItemIndex
        {
            get
            {
                return (PageIndex - 1) * PageSize;
            }
        }

        public int LastItemIndex
        {
            get
            {
                return FirstItemIndex + PageSize;
            }
        }
        public int GetDefaultPageSize
        {
            get
            {
                return DefaultPageSize;
            }
        }
        public string GetDefaultPostCode
        {
            get
            {
                return "";

            }

        }
        public int GetDefaultDistance
        {
            get
            {
                int _DefaultDistance = SCHelpers.EmptyIfInt(System.Configuration.ConfigurationManager.AppSettings["DefaultDistance"]);
                return _DefaultDistance;
            }
        }
        public decimal prmin { get; set; }
        public decimal prmax { get; set; }

        public decimal smin { get; set; }
        public decimal smax { get; set; }
    }
}