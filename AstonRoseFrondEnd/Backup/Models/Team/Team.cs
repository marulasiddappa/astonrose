﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class Team
    {
        public Team()
        {
            TeamTypes = new List<SelectListItem>();
        }
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Firs tName is Required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is Required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Team Type")]

        public string TeamType { get; set; }

        [Required(ErrorMessage = "Department is Required")]
        [Display(Name = "Department")]
        public string Department { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telephone Number is Required")]
        [Display(Name = "Telephone Number")]
        public string TelephoneNumber { get; set; }

        [Required(ErrorMessage = "Mobile Number is Required")]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Required(ErrorMessage = "Summary is Required")]
        [Display(Name = "Summary")]
        [AllowHtml]
        public string Summary { get; set; }

        [Display(Name = "Image")]
        public string Image { get; set; }

        [Display(Name = "Property Contact Flag")]
        public bool PropertyContactFlag { get; set; }

        [Display(Name = "Office Contact Flag")]
        public bool OfficeContactFlag { get; set; }

        [Display(Name = "LinkedIn URL")]
        public string LinkedInURL { get; set; }

        [Display(Name = "Twitter URL")]
        public string TwitterURL { get; set; }

        [Display(Name = "FaceBook URL")]
        public string FaceBookURL { get; set; }

        [Display(Name = "Display Order")]
        public int? DisplayOrder { get; set; }

        [Display(Name = "Show in Contacts")]
        public bool ShowInContacts { get; set; }

        [NotMapped]
        public IList<SelectListItem> TeamTypes { get; set; }

        [Required(ErrorMessage = "Designation is Required")]
        [Display(Name = "Designation")]
        public string Designation { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }

    }
}