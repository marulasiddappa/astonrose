﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using AstonRoseFrondEnd.Helpers;

namespace AstonRoseFrondEnd.Models.Binders
{
    public class SearchParametersBinder : IModelBinder
    {
        public static int DefaultPageSize = SearchParameters.DefaultPageSize;

        public IDictionary<string, string> NVToDict(NameValueCollection nv)
        {
            var d = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var k in nv.AllKeys)
                if (k != null)
                    d[k] = nv[k];
            return d;
        }

        private static readonly Regex FacetRegex = new Regex("^f_", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var qs = controllerContext.HttpContext.Request.QueryString;
            var qsDict = NVToDict(qs);

            string _DefaultSortBy = StringHelper.EmptyToNull(qs["sort"]);
            #region "Default Sort"
            if (_DefaultSortBy == null)
            {
                _DefaultSortBy = "buildingname";
            }
            #endregion


            var sp = new SearchParameters
            {
                FreeSearch = StringHelper.EmptyToNull(qs["q"]),
                PageIndex = StringHelper.TryParse(qs["page"], 1),
                PageSize = StringHelper.TryParse(qs["pageSize"], GetDefaultPageSize),
                Sort = _DefaultSortBy,
                Facets = qsDict.Where(k => FacetRegex.IsMatch(k.Key))
                    .Select(k => k.WithKey(FacetRegex.Replace(k.Key, "")))
                    .ToDictionary()
            };
            return sp;
        }

        public int GetDefaultPageSize
        {
            get
            {



                return SCHelpers.EmptyIfInt(System.Configuration.ConfigurationManager.AppSettings["DefaultPageSize"]);
            }
        }

      
    }
}