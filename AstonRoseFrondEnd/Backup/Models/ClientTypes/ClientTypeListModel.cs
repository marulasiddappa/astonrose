﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class ClientTypeListModel
    {
        public List<ClientTypeView> ViewList { get; set; }
        public ClientTypeViewModel SelectedClientType { get; set; }
    }

    public class ClientTypeView:ServiceView
    {
    }
    public class ClientTypeViewModel : ServiceViewModel
    {

        
    }
}