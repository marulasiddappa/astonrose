﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.IO;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Models;

namespace AstonRoseFrondEnd.Controllers
{
    public class BaseController : Controller
    {
        public string uploadPath = ConfigurationManager.AppSettings["FileUploadPath"];
        public string fileUploadURL = ConfigurationManager.AppSettings["FileUploadURL"];

        public BaseController()
            : base()
        {

            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                List<ServiceView> serviceTitles = (from s in db.Services
                                                   select new ServiceView { Id = s.Id, Title = s.Title }).ToList();

                ViewBag.AllServices = serviceTitles;
                List<ClientTypeView> clientTypeTitles = (from s in db.ClientTypes
                                                         select new ClientTypeView { Id = s.Id, Title = s.Title }).ToList();
                ViewBag.AllClientTypes = clientTypeTitles;
            }
            
            
        }
        protected override void Execute(System.Web.Routing.RequestContext requestContext)
        {
            Basket basket = Basket.GetCart(requestContext.HttpContext);
            ViewBag.PropertyCount = basket.GetCount();
            base.Execute(requestContext);
            
        }

        #region "files handling"
        protected string saveFile(HttpPostedFileBase uploadedFile)
        {
            if (uploadedFile == null)
                return null;
            string fileName = Path.GetFileName(uploadedFile.FileName);
            string filePath = Path.Combine(uploadPath, fileName);

            int i = 1;
            while (System.IO.File.Exists(filePath))
            {
                filePath = Path.Combine(uploadPath, System.IO.Path.GetFileNameWithoutExtension(fileName) + i.ToString() + System.IO.Path.GetExtension(fileName));
                i++;
            }
            uploadedFile.SaveAs(filePath);

            return Path.GetFileName(filePath);
        }

        protected string SaveUploadFile(HttpPostedFileBase uploadedFile)
        {
            if (uploadedFile == null)
                return null;
            string fileName = Path.GetFileName(uploadedFile.FileName);
            string filePath = Path.Combine(uploadPath, fileName);

            int i = 1;
            while (System.IO.File.Exists(filePath))
            {
                filePath = Path.Combine(uploadPath, System.IO.Path.GetFileNameWithoutExtension(fileName) + i.ToString() + System.IO.Path.GetExtension(fileName));
                i++;
            }
            uploadedFile.SaveAs(filePath);
            return filePath;
        }
        protected string updateFile(string oldFileName, HttpPostedFileBase postedFile)
        {
            string fileName = oldFileName;
            if (postedFile != null)
            {
                if (!string.IsNullOrEmpty(oldFileName))
                {
                    deleteFile(oldFileName);
                }
                fileName = saveFile(postedFile);
            }
            return fileName;
        }
        protected void deleteFile(string fileName)
        {
            string filePath = string.Empty;
            if (fileName != null)
                filePath = Path.Combine(uploadPath, fileName);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
        }
        #endregion
    }
}
