﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.DSL;
using SolrNet.Exceptions;
using System.Threading;
using Yotors.Services;
using System.Text;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Helpers;
using AstonRoseFrondEnd.Models;

namespace AstonRoseFrondEnd.Controllers
{
    public class PropController : Controller
    {

        private readonly ISolrReadOnlyOperations<SolrProperty> solr;

        public PropController(ISolrReadOnlyOperations<SolrProperty> solr)
        {
            this.solr = solr;

        }


        #region Solr Stuffs goes here
        [HttpPost]
        public ActionResult QuickSearch()
        {
            string url = "/prop/propertyresults?st=qs";
            var location = SCHelpers.EmptyIfStr(Request["location"]);
            url += location.Length > 0 ? "&postcode=" + location : "";

            var propertyType = SCHelpers.EmptyIfStr(Request["propertyType"]);
            url += propertyType.Length > 0 ? "&f_proptype=" + propertyType : "";

            var distance = SCHelpers.EmptyIfStr(Request["Distance"]);
            url += distance.Length > 0 ? "&d=" + distance : "";
            return Redirect(url);
        }


        public ActionResult PropertyResults(string q, string postcode, string f_proptype, string d, SearchParameters parameters, FormCollection myfrm)
        {
            
            try
            {
                
                if (q != null)
                {
                    q = q.Replace("/", " ");
                }
                if (parameters.FreeSearch != null)
                {
                    parameters.FreeSearch = parameters.FreeSearch.Replace("/", " ");
                }
                parameters.Facets.Add(new KeyValuePair<string,string>("tenure","To Let"));
                //parameters.Facets.Add(new KeyValuePair<string, string>("region", "'South','London - West','South East'"));

                ViewBag.PageTitle = "Property Search Results : " + ViewBag.SearchKeyword;
                var start = (parameters.PageIndex - 1) * parameters.PageSize;

                var sq = BuildQueryPriceRange(parameters);
                sq = MultipleFilterQry(sq, parameters);


                var mySort = new[] {
                                        new SortOrder("score", SolrNet.Order.DESC)
									};


                var options = new QueryOptions();
                options.Rows = parameters.PageSize;
                options.Start = start;
                options.SpellCheck = new SpellCheckingParameters();
                options.Facet = new FacetParameters
                {
                    Queries = AllFacetFields.Except(SelectedFacetFields(parameters))
                                                                          .Select(f => new SolrFacetFieldQuery(f) { MinCount = 1 })
                                                                          .Cast<ISolrFacetQuery>()
                                                                          .ToList(),
                    Limit = 9999,
                };
                options.FilterQueries = BuildFilterQueries(parameters);
                postcode = SCHelpers.EmptyIfStr(postcode);
                if (postcode != "")
                {
                    parameters.PostCode = postcode;
                    int distance = SCHelpers.EmptyIfInt(Request["d"]);
                    parameters.Distance = distance == 0 ? 999 : distance;
                    distance = Convert.ToInt32(distance / 0.62137);
                    Geoloc gl = Geocoder.LocateGooglePostcode(postcode.ToString()) ?? new Geoloc();
                    options.AddFilterQueries((ISolrQuery)new SolrQueryByDistance("proplocation", gl.Lat, gl.Lon, distance, CalculationAccuracy.Radius));

                }
                else
                {
                    postcode = SCHelpers.EmptyIfStr(parameters.PostCode);
                    if (postcode != "")
                    {
                        int distance = SCHelpers.EmptyIfInt(Request["d"]);
                        parameters.Distance = distance == 0 ? 999 : distance;
                        distance = Convert.ToInt32(distance / 0.62137);

                        Geoloc gl = Geocoder.LocateGooglePostcode(postcode.ToString()) ?? new Geoloc();
                        options.AddFilterQueries((ISolrQuery)new SolrQueryByDistance("proplocation", gl.Lat, gl.Lon, distance, CalculationAccuracy.Radius));
                    }

                }
                options.OrderBy = mySort;
                var matchingproperties = solr.Query(sq, options);

                var currenturl = UrlHelperExtensions.UrlOriginal(Request);
                var parts = currenturl.ToString().Split('?');
                string reseturl = currenturl.ToString();
                if (parts.Count() > 0)
                {
                    reseturl = parts[0] + "?q=" + parameters.FreeSearch + "&page=1";
                }
                else
                {
                    reseturl = currenturl.ToString();
                }

                var view = new PropertyView
                {

                    SolrProperties = matchingproperties,
                    Search = parameters,
                    TotalCount = matchingproperties.NumFound,
                    Facets = matchingproperties.FacetFields,
                    DidYouMean = GetSpellCheckingResult(matchingproperties),

                    SortSelectedItem = parameters.Sort,
                    PageSizeSelectedItem = parameters.PageSize,
                    PageResetURL = reseturl,

                };

                view.TravelDistance.Add(new SelectListItem() { Text = "All", Value = "" });
                foreach (var t in SCHelpers.TravelDistanceDropdown().ToList())
                {
                    view.TravelDistance.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == parameters.Distance.ToString()) });
                }


                return View(view);

            }
            catch (InvalidFieldException)
            {
                return View(new PropertyView
                {
                    QueryError = true,
                });
            }


        }
       

        #endregion


        AstonRoseCMSContext db = new AstonRoseCMSContext();
        //SCHelpers mySCHelper = new SCHelpers();

        #region jobs page actions & form


        public ActionResult Index()
        {
            return View();
        }

       


        #endregion


        #region Solr Query & Facets
        #region "Solor Stuffs"



        private static readonly string[] AllFacetFields = new[] { "proptype", "tenure", "category", "region", "proplocation", "propsize" };

        public ISolrQuery BuildQuery(SearchParameters parameters)
        {
            if (!string.IsNullOrEmpty(parameters.FreeSearch))
            {
                return new SolrQuery(parameters.FreeSearch);
            }
            else
            {
                return SolrQuery.All;
            }

        }
        public AbstractSolrQuery BuildQueryPriceRange(SearchParameters parameters)
        {
            if (!string.IsNullOrEmpty(parameters.FreeSearch))
            {
                return new SolrQuery(parameters.FreeSearch);
            }
            else
            {
                return (AbstractSolrQuery)(SolrQuery.All);
            }

        }


        public ICollection<ISolrQuery> BuildFilterQueries(SearchParameters parameters)
        {

            var queriesFromFacets = from p in parameters.Facets
                                    select (ISolrQuery)Query.Field(p.Key).Is(p.Value);



            return queriesFromFacets.ToList();
        }



        public IEnumerable<string> SelectedFacetFields(SearchParameters parameters)
        {

            return parameters.Facets.Select(f => f.Key);
        }

        public SortOrder[] GetSelectedSort(SearchParameters parameters)
        {
            return new[] { SortOrder.Parse(parameters.Sort) }.Where(o => o != null).ToArray();
        }

        private string GetSpellCheckingResult(SolrQueryResults<SolrProperty> properties)
        {
            return string.Join(" ", properties.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray());
        }
        #endregion

        public AbstractSolrQuery MultipleFilterQry(AbstractSolrQuery sq, SearchParameters parameters)
        {
            var ms_regions = "South,London - West,South East";
            if (ms_regions != null)
            {
                string[] split = ms_regions.Split(',');
                var _multivaluelist = split.Where(x => x != "");
                sq = sq && new SolrQueryInList("region", _multivaluelist);
            }

            return sq;
            //if (Request["smin"] != null && Request["smax"] != null)
            //{

            //    parameters.smin = Convert.ToDecimal(Request["smin"].ToString());
            //    parameters.smax = Convert.ToDecimal(Request["smax"].ToString());
            //    sq = sq && new SolrQueryByRange<decimal>("salaryfrom", parameters.smin, parameters.smax);
            //}

            //var mssalaryranges = Request["ms_salaryranges"];
            //if (mssalaryranges != null)
            //{
            //    string[] split = mssalaryranges.Split(',');
            //    var _multivaluelist = split.Where(x => x != "");
            //    sq = sq && new SolrQueryInList("salaryrangelevel", _multivaluelist);
            //}

            //return sq;

        }
        #endregion



     

    }
}
