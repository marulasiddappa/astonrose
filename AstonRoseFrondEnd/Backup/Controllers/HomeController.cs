﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseFrondEnd.Models;
using System.Configuration;
using System.Text.RegularExpressions;
using AstonRoseFrondEnd.Controllers;
using System.Data.Entity.Validation;
using AstonRoseFrondEnd.Helpers;
using System.ServiceModel.Syndication;
using System.Xml;
namespace AstonRoseFrondEnd.Controllers
{
    public class HomeController : BaseController
    {
        public string fileUploadURL = ConfigurationManager.AppSettings["FileUploadURL"];

        //private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public ActionResult Index()
        {
            HomeModel model = new HomeModel();
            ViewBag.MetaTitle = "Aston Rose";
            ViewBag.MetaDescription = "Aston Rose is a professional property consultancy, providing effective and efficient quality advice and services across a broad range of commercial property interests."; 

            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.LatestNews = (from n in db.News
                                    where n.GoLive && n.HomePageDisplay
                                    orderby n.NewsDate descending
                                    select n).FirstOrDefault();
                if (model.LatestNews == null)
                    model.LatestNews = new News() { FullDescription = "" };
                string newsDescription = Regex.Replace(model.LatestNews.FullDescription, "<[^>]+>", "");

                model.LatestNews.FullDescription = (newsDescription.Length > 120 ? newsDescription.Substring(0, 120) + "..." : newsDescription);

                ViewBag.FileUploadUrl = fileUploadURL;

                var reader = XmlReader.Create("http://www.costar.co.uk/en/Latest-News-RSS-Feed/");
                var newsItems = SyndicationFeed.Load(reader);
                reader.Close();

                model.CoActorNews = newsItems.Items;
            }

            return View(model);
        }
        public ActionResult _Login()
        {
            return View();
        }
        public ActionResult About()
        {
            HomeModel model = new HomeModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.SelectedMember = (from m in db.Team
                                        where m.TeamType == "Director"
                                        orderby m.DisplayOrder
                                        select m).FirstOrDefault();

                if (model.SelectedMember == null)
                {
                    model.SelectedMember = new Team();
                }

                ViewBag.MetaTitle = model.SelectedMember.MetaTitle;
                
                model.Members = (from m in db.Team
                                 where m.TeamType == "Director"
                                 orderby m.LastName
                                 select new TeamMember { Id = m.Id, Name = m.FirstName + " " + m.LastName }).ToList();
                ViewBag.MetaTitle = "About Us: Aston Rose";
                ViewBag.MetaDescription = "Aston Rose is a professional property consultancy, providing effective and efficient quality advice and services across a broad range of commercial property interests."; 
                ViewBag.PageTitle = "About Us  - " ;
            }
            return View(model);
        }

        public ActionResult Directors(string name = null, int id = 0)
        {
            HomeModel model = new HomeModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.TeamMembers = (from m in db.Team
                                     where m.TeamType == "Director"
                                     orderby m.LastName
                                     select m).ToList();
                ViewBag.MetaTitle = "Directors : Aston Rose";
                ViewBag.MetaDescription = "Aston Rose is a professional property consultancy, providing effective and efficient quality advice and services across a broad range of commercial property interests."; 
                ViewBag.PageTitle = "Directors  - ";
            }
            return View(model);
        }

        public ActionResult OurTeam(string name = null, int id = 0)
        {
            HomeModel model = new HomeModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.TeamMembers = (from m in db.Team
                                     where m.TeamType != "Director"
                                     orderby m.TeamType, m.LastName
                                     select m).ToList();
                ViewBag.MetaTitle = "Our Team : Aston Rose";
                ViewBag.MetaDescription = "Aston Rose is a professional property consultancy, providing effective and efficient quality advice and services across a broad range of commercial property interests."; 
                ViewBag.PageTitle = "Our Team  - ";
            }
            return View(model);
        }

        public ActionResult OurClients(string name = null, int id = 0)
        {
            HomeModel model = new HomeModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.Clients = db.Client.ToList();
               
                model.Members = (from m in db.Client
                                 select new TeamMember { Id = m.Id, Name = m.Name }).ToList();
                ViewBag.MetaTitle = "Our Clients : Aston Rose";
                ViewBag.MetaDescription = "Aston Rose is a professional property consultancy, providing effective and efficient quality advice and services across a broad range of commercial property interests."; 
                ViewBag.PageTitle = "Our Clients  - ";

            }
            return View(model);
        }

        public ActionResult CaseStudies(string name = null, int id = 0)
        {
            HomeModel model = new HomeModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.Testimonials = db.Testimonials.ToList();
                
                model.Members = (from m in db.Testimonials
                                
                                 select new TeamMember { Id = m.Id, Name = m.Name }).ToList();
                ViewBag.MetaTitle = "Case Studies : Aston Rose";
                ViewBag.MetaDescription = "Aston Rose is a professional property consultancy, providing effective and efficient quality advice and services across a broad range of commercial property interests."; 
                ViewBag.PageTitle = "Case Studies  - ";

            }
            return View(model);
        }

        public ActionResult OurOffices(string name = null, int id = 0)
        {
            HomeModel model = new HomeModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.SelectedOffice = (from m in db.Offices
                                        where (id == 0 || m.Id == id)
                                        orderby m.DisplayOrder
                                        select m).FirstOrDefault();
                if (model.SelectedOffice == null)
                {
                    model.SelectedOffice = new Offices();
                }
                ViewBag.MetaTitle = model.SelectedOffice.MetaTitle;
                ViewBag.MetaDescription = model.SelectedOffice.MetaDescription;
                if (string.IsNullOrEmpty(model.SelectedOffice.MetaTitle))
                {
                    ViewBag.MetaTitle = model.SelectedOffice.OfficeName;
                }

                if (string.IsNullOrEmpty(model.SelectedOffice.MetaDescription))
                {
                    ViewBag.MetaDescription = model.SelectedOffice.OfficeName;
                }
                ViewBag.PageTitle = model.SelectedOffice.OfficeName + " - ";
                model.Members = (from m in db.Offices
                                 orderby m.DisplayOrder
                                 select new TeamMember { Id = m.Id, Name = m.OfficeName }).ToList();
            }
            return View(model);
        }
    }
}
