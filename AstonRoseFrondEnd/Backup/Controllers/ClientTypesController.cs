﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Controllers;
using System.Text.RegularExpressions;

namespace AstonRoseFrondEnd.Controllers
{
    public class ClientTypesController : BaseController
    {
        //
        // GET: /ClientTypes/
        //private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public ActionResult Index(string title = null, int id = 0)
        {
            
            ClientTypeListModel model = GetModel(title, id);
            ViewBag.PageTitle = "Client Types - ";
            ViewBag.MetaTitle = "Client Types : Aston Rose";
            ViewBag.MetaDescription = "Client Types : Aston Rose"; 
            return View(model);

        }

        public ActionResult Select(string title = null, int id = 0)
        {
            ClientTypeListModel model = GetModel(title, id);
            return View(model);
        }

        ClientTypeListModel GetModel(string title, int id)
        {
            ClientTypeListModel model = new ClientTypeListModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                


                model.ViewList = (from s in db.ClientTypes
                                  select new ClientTypeView { Id = s.Id, Title = s.Title }).ToList(); ;
                model.SelectedClientType = (from s in db.ClientTypes
                                            join cs in db.CaseStudies on s.Id equals cs.ClientTypeID into caseStudies
                                            from cs1 in caseStudies.DefaultIfEmpty()
                                            where id == 0 || s.Id == id
                                            select new ClientTypeViewModel
                                            {
                                                Id = s.Id,
                                                Title = s.Title,
                                                Description = s.Description,

                                                Image = s.Image,
                                                CaseStudyTitle = cs1.Title,
                                                CaseStudyDescription = cs1.Description,
                                                MetaTitle = s.MetaDescription,
                                                MetaDescription = s.MetaDescription,
                                                ContactIds = s.ContactIds,
                                                ServicesOfferedTitle1 = s.ServicesOfferedTitle1,
                                                ServicesOffered1 = s.ServicesOffered1,
                                                ServicesOfferedTitle2 = s.ServicesOfferedTitle2,
                                                ServicesOffered2 = s.ServicesOffered2,
                                                PDFBrochure = s.PDFBrochure,

                                            }).FirstOrDefault();



                if (model.SelectedClientType != null)
                {
                    model.SelectedClientType.Contacts = db.Database.SqlQuery<ServiceContact>("select FirstName + ' ' + LastName as Name, TelephoneNumber as Phone, FirstName as EmailName,Email as EmailID  from Team where ID in (" + model.SelectedClientType.ContactIds + ") ").ToList();
                    ViewBag.MetaTitle = model.SelectedClientType.MetaTitle;
                    ViewBag.MetaDescription = model.SelectedClientType.MetaDescription;

                    if (string.IsNullOrEmpty(model.SelectedClientType.MetaTitle))
                    {
                        ViewBag.MetaTitle = model.SelectedClientType.Title;
                    }

                    if (string.IsNullOrEmpty(model.SelectedClientType.MetaDescription))
                    {
                        ViewBag.MetaDescription = Regex.Replace(model.SelectedClientType.Description, @"<(.|\n)*?>", ""); 
                    }
                    ViewBag.PageTitle = model.SelectedClientType.Title + " - ";

                    model.SelectedClientType.Images = (from i in db.UploadedImages
                                                       where i.ReferenceId == model.SelectedClientType.Id && i.Type == "ClientType"
                                                       select i).ToList();

                    model.SelectedClientType.CaseStudies = (from cs in db.CaseStudies
                                                         join scs in db.ClientTypeCaseStudies on cs.Id equals scs.CaseStudyId
                                                         where scs.ClientTypeId == model.SelectedClientType.Id && scs.Live != null && scs.Live == true
                                                         select cs).ToList();

                }
            }
            return model;
        }

    }
}
