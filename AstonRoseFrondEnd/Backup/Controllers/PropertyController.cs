﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseFrondEnd.Controllers;
using AstonRoseFrondEnd.Models;
using System.Data.SqlClient;
using AstonRoseFrondEnd.Models;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using AstonRoseFrondEnd.Helpers;
using SolrNet;
using SolrNet.DSL;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;
using System.Globalization;
using System.Text.RegularExpressions;

namespace AstonRoseFrondEnd.Controllers
{
    public class PropertyController : BaseController
    {
        //private AstonRoseCMSContext db = new AstonRoseCMSContext();
        private readonly ISolrReadOnlyOperations<SolrProperty> solr;

        public PropertyController(ISolrReadOnlyOperations<SolrProperty> solr)
        {
            this.solr = solr;

        }
        public ActionResult Search()
        {
            return View();
        }


        public ActionResult Index()
        {
            ViewBag.MetaTitle = "Property Search : Aston Rose";
            ViewBag.MetaDescription = "Property Search : Aston Rose";
            ViewBag.PageTitle = "Property Search  - ";
            PropertyFilter propertyFilter = new PropertyFilter();
            SplitValues(propertyFilter);
            return View(propertyFilter);
        }

        private void SplitValues(PropertyFilter propertyFilter)
        {
            if (string.IsNullOrEmpty(propertyFilter.PropsizeF))
            {
                propertyFilter.PropsizeF = "0 - 15000";
            }

            if (string.IsNullOrEmpty(propertyFilter.PropsizeM))
            {
                propertyFilter.PropsizeM = "0 - 1500";
            }

            string[] fvalues = propertyFilter.PropsizeF.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
            propertyFilter.FloorMinF = decimal.Parse(fvalues[0]);
            propertyFilter.FloorMaxF = decimal.Parse(fvalues[1]);

            string[] mvalues = propertyFilter.PropsizeM.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
            propertyFilter.FloorMinM = decimal.Parse(mvalues[0]);
            propertyFilter.FloorMaxM = decimal.Parse(mvalues[1]);
        }
        const int PageSize = AppConstants.PageSize;
        //PropertyFilter GetInitialValue
        public ActionResult QuickSearch(PropertyFilter propertyFilter)
        {
            return RedirectToAction("searchresult", new { Proplocation = propertyFilter.Proplocation, Proptype = propertyFilter.Proptype, Distance = propertyFilter.Distance });
        }

        public ActionResult SearchResult(PropertyFilter propertyFilter, SearchParameters parameters, FormCollection myfrm)
        {
            ViewBag.MetaTitle = "Property Search Results : Aston Rose";
            ViewBag.MetaDescription = "Property Search Results : Aston Rose";
            ViewBag.PageTitle = "Property Search Results  - ";
            ViewBag.GoogleMapKey = ConfigurationManager.AppSettings["GoogleMapsKey"];
            SplitValues(propertyFilter);


            try
            {
                if (parameters.FreeSearch != null)
                {
                    parameters.FreeSearch = parameters.FreeSearch.Replace("/", " ");
                }
                FillFacets(parameters, propertyFilter);
                
                var start = (parameters.PageIndex - 1) * parameters.PageSize;
                var sq = BuildQueryPriceRange(parameters);
                sq = MultipleFilterQry(sq, parameters, propertyFilter);
                var mySort = new[] {
                                        new SolrNet.SortOrder("score", SolrNet.Order.DESC)
									};
                var options = new QueryOptions();
                options.Rows = parameters.PageSize;
                options.Start = start;
                options.SpellCheck = new SpellCheckingParameters();
                options.Facet = new FacetParameters
                {
                    Queries = AllFacetFields.Except(SelectedFacetFields(parameters))
                                                                          .Select(f => new SolrFacetFieldQuery(f) { MinCount = 1 })
                                                                          .Cast<ISolrFacetQuery>()
                                                                          .ToList(),
                    Limit = 9999,
                };
                options.FilterQueries = BuildFilterQueries(parameters);
                string postcode = propertyFilter.Proplocation;
                postcode = SCHelpers.EmptyIfStr(postcode);
                if (postcode != "")
                {
                    if (postcode.ToLower() == "w1")
                    {
                        postcode = "London," + postcode;
                    }
                    parameters.PostCode = postcode;
                    int distance = SCHelpers.EmptyIfInt(propertyFilter.Distance);
                    parameters.Distance = distance == 0 ? 999 : distance;
                    distance = Convert.ToInt32(distance / 0.62137);
                    GeoLoc gl = GeoCoder.LocateGooglePostcode(postcode.ToString()) ?? new GeoLoc();
                    ViewBag.latitude = gl.Lat;
                    ViewBag.longitude = gl.Lon;
                    ViewBag.mapzoom = 11;
                    options.AddFilterQueries((ISolrQuery)new SolrQueryByDistance("proplocation", gl.Lat, gl.Lon, distance, CalculationAccuracy.Radius));
                    //Below query to find the nearest first
                    options.OrderBy = new[] { new SolrNet.SortOrder("geodist()", Order.ASC) };
                    options.ExtraParams = new Dictionary<string, string>
                                          {
                                              // uncomment for filtering by distance
                                              {"fq", "{!geofilt}"},
                                              {"d", distance.ToString(CultureInfo.InvariantCulture)}, // replace distance with your radius filter
                                              {"sfield", "proplocation"}, // replace lat_long with your field in solr that stores the lat long values
                                              {"pt", gl.Lat + "," + gl.Lon}, // this is the point of reference
                                          };
                                                     
                }
                else
                {
                    postcode = SCHelpers.EmptyIfStr(parameters.PostCode);
                    if (postcode != "")
                    {
                        int distance = SCHelpers.EmptyIfInt(Request["d"]);
                        parameters.Distance = distance == 0 ? 999 : distance;
                        distance = Convert.ToInt32(distance / 0.62137);

                        GeoLoc gl = GeoCoder.LocateGooglePostcode(postcode.ToString()) ?? new GeoLoc();
                        options.AddFilterQueries((ISolrQuery)new SolrQueryByDistance("proplocation", gl.Lat, gl.Lon, distance, CalculationAccuracy.Radius));
                    }
                    options.OrderBy = mySort;
                }
               
                                               
                var matchingproperties = solr.Query(sq, options);


                var currenturl = UrlHelperExtensions.UrlOriginal(Request);
                var parts = currenturl.ToString().Split('?');
                string reseturl = currenturl.ToString();
                if (parts.Count() > 0)
                {
                    reseturl = parts[0] + "?q=" + parameters.FreeSearch + "&page=1";
                }
                else
                {
                    reseturl = currenturl.ToString();
                }

                var view = new PropertyView
                {

                    SolrProperties = matchingproperties,
                    //Map = DisplayPoints(matchingproperties),
                    Search = parameters,
                    TotalCount = matchingproperties.NumFound,
                    Facets = matchingproperties.FacetFields,
                    DidYouMean = GetSpellCheckingResult(matchingproperties),
                    Filter = propertyFilter,
                    SortSelectedItem = parameters.Sort,
                    PageSizeSelectedItem = parameters.PageSize,
                    PageResetURL = reseturl,

                };

                view.TravelDistance.Add(new SelectListItem() { Text = "All", Value = "" });
                foreach (var t in SCHelpers.TravelDistanceDropdown().ToList())
                {
                    view.TravelDistance.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == parameters.Distance.ToString()) });
                }


                return View(view);

            }
            catch (InvalidFieldException)
            {
                return View(new PropertyView
                {
                    QueryError = true,
                });
            }
        }

        private void FillFacets(Models.SearchParameters parameters, Models.PropertyFilter propertyFilter)
        {

            if (!string.IsNullOrEmpty(propertyFilter.Proptype))
            {
                parameters.Facets.Add("proptype", propertyFilter.Proptype);
            }
            if (!string.IsNullOrEmpty(propertyFilter.Tenure))
            {
                parameters.Facets.Add("tenure", propertyFilter.Tenure);
            }

            if (!string.IsNullOrEmpty(propertyFilter.Category))
            {
                parameters.Facets.Add("category", propertyFilter.Category);
            }
        }


        #region Solr Query & Facets
        #region "Solor Stuffs"



        private static readonly string[] AllFacetFields = new[] { "proptype", "tenure", "category", "region", "proplocation", "propsize" };

        public ISolrQuery BuildQuery(SearchParameters parameters)
        {
            if (!string.IsNullOrEmpty(parameters.FreeSearch))
            {
                return new SolrQuery(parameters.FreeSearch);
            }
            else
            {
                return SolrQuery.All;
            }

        }
        public AbstractSolrQuery BuildQueryPriceRange(SearchParameters parameters)
        {
            if (!string.IsNullOrEmpty(parameters.FreeSearch))
            {
                return new SolrQuery(parameters.FreeSearch);
            }
            else
            {
                return (AbstractSolrQuery)(SolrQuery.All);
            }

        }


        public ICollection<ISolrQuery> BuildFilterQueries(SearchParameters parameters)
        {

            var queriesFromFacets = from p in parameters.Facets
                                    select (ISolrQuery)Query.Field(p.Key).Is(p.Value);



            return queriesFromFacets.ToList();
        }



        public IEnumerable<string> SelectedFacetFields(SearchParameters parameters)
        {

            return parameters.Facets.Select(f => f.Key);
        }

        public SolrNet.SortOrder[] GetSelectedSort(SearchParameters parameters)
        {
            return new[] { SolrNet.SortOrder.Parse(parameters.Sort) }.Where(o => o != null).ToArray();
        }

        private string GetSpellCheckingResult(SolrQueryResults<SolrProperty> properties)
        {
            return string.Join(" ", properties.SpellChecking
                                        .Select(c => c.Suggestions.FirstOrDefault())
                                        .Where(c => !string.IsNullOrEmpty(c))
                                        .ToArray());
        }
        #endregion

        public AbstractSolrQuery MultipleFilterQry(AbstractSolrQuery sq, SearchParameters parameters, PropertyFilter filter)
        {

            if (filter != null)
            {
                if (filter.Region != null)
                {
                    string[] split = filter.Region;
                    var _multivaluelist = split.Where(x => x != "");
                    sq = sq && new SolrQueryInList("region", _multivaluelist);
                }

                FloorAreas areas = new FloorAreas();
                List<string> fareas = new List<string>();
                if (filter.FloorMinF == 0 && filter.FloorMaxF == 15000)
                {
                    //fareas = areas.FloorAreaValues.Select(a => a.Area).ToArray();

                }
                else
                {
                    fareas = (from a in areas.FloorAreaFValues
                              where a.Min >= filter.FloorMinF && a.Max <= filter.FloorMaxF
                              select a.Area).ToList();
                }
                List<string> mareas = new List<string>();
                
                if (filter.FloorMinM == 0 && filter.FloorMaxM == 1500)
                {
                    //fareas = areas.FloorAreaValues.Select(a => a.Area).ToArray();

                }
                else
                {
                     mareas = (from a in areas.FloorAreaMValues
                              where a.Min >= filter.FloorMinM && a.Max <= filter.FloorMaxM
                              select a.Area).ToList();
                }
                if (fareas == null)
                {
                    fareas = new List<string>();
                }
                fareas.AddRange(mareas);

                if (fareas != null && fareas.Count > 0)
                {

                    sq = sq && new SolrQueryInList("propsize", fareas);
                }

                if (!string.IsNullOrEmpty(filter.Proplocation))
                {
                    //sq = sq & new SolrQuery("(county:" + filter.Proplocation + " OR town:" + filter.Proplocation + " OR address1:"+  filter.Proplocation +")");
                    sq = sq | new SolrQuery("(county:" + filter.Proplocation + " OR town:" + filter.Proplocation + " OR region: " + filter.Proplocation + " OR address1: " + filter.Proplocation + " OR address2: " + filter.Proplocation + " OR address3: " + filter.Proplocation + " )");
                    //sq = sq | new SolrQuery("town:" + filter.Proplocation );

                    //sq = sq | new SolrQuery("buildingname:" + filter.Proplocation);
                    //sq = sq && new 
                }
            }



            return sq;

        }
        #endregion


        public ActionResult MyProperties(int pageIndex = 0)
        {
            Basket basket = Basket.GetCart(this.HttpContext);
            MyProperty myProperties = basket.GetMyBasketItems(pageIndex);
            myProperties.Enquiry = new EnquiryUser();
            ViewBag.MetaTitle = "My Properties : Aston Rose";
            ViewBag.MetaDescription = "My Properties : Aston Rose";
            ViewBag.PageTitle = "My Properties  - ";
            return View(myProperties);
        }

        public JsonResult AddToMyProperty(int propertyId = 0)
        {
            Property p = null;
            
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                p = db.Property.Find(propertyId);
            }
            Basket basket = Basket.GetCart(this.HttpContext);
            basket.AddToBasket(p);

            return Json(basket.GetCount(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Remove(int propertyId = 0)
        {
            

            Basket basket = Basket.GetCart(this.HttpContext);
            basket.RemoveFromBasket(propertyId);
            return RedirectToAction("MyProperties");
        }

        public ActionResult Detail(string name = "", int id = 0)
        {
            ViewBag.GoogleMapKey = ConfigurationManager.AppSettings["GoogleMapsKey"];

            PropertyListModel model = new PropertyListModel();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.SelectedProperty = db.Property.Find(id);
                //Get all the images of this property
                model.UploadedImages = (from ui in db.UploadedImages
                                        where ui.ReferenceId == id && ui.Type == "Property"
                                        select ui).ToList();


                ViewBag.MetaTitle = model.SelectedProperty.MetaTitle;
                ViewBag.MetaDescription = model.SelectedProperty.MetaDescription;
                if (string.IsNullOrEmpty(model.SelectedProperty.MetaTitle))
                {
                    ViewBag.MetaTitle = model.SelectedProperty.ToString();
                }

                if (string.IsNullOrEmpty(model.SelectedProperty.MetaDescription))
                {
                    ViewBag.MetaDescription = Regex.Replace(model.SelectedProperty.Description, @"<(.|\n)*?>", "");
                }
                ViewBag.PageTitle = model.SelectedProperty.ToString() + " - ";
                ViewBag.BodyCSS = "detail";

                //Get the contacts of property
                var ids = model.SelectedProperty.ContactIDs.Split(',');
                model.Contacts = new List<Team>();
                if (ids != null && ids.Length > 0)
                {
                    foreach (string cid in ids)
                    {
                        model.Contacts.Add(db.Team.Find(int.Parse(cid)));
                    }
                }

                //This is used when user enquires about property
                model.Enquiry = new EnquiryUser();

                //Checking if this property already added to basket
                Basket basket = Basket.GetCart(this.HttpContext);
                model.SelectedProperty.Added = basket.CheckIfAdded(model.SelectedProperty.Id);

                //Function to show the property in google map
                List<Property> properties = new List<Property>();
                properties.Add(model.SelectedProperty);
                //model.Map = DisplayPoints(properties);
            }
            return View(model);
        }

        public JsonResult SendEnquiry(MyProperty myProperties)
        {
            EnquiryUser user = myProperties.Enquiry;

            Basket basket = Basket.GetCart(this.HttpContext);
            List<Property> properties = basket.GetMyBasketItems();
            if (properties.Count == 0)
                return Json("Please select the properties");

            string props = "";
            string propContent = "<tr><td width=\"270\" height=\"50\" valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif;line-height: 20px; margin: 1em 0; font-size: 12px;\">{0}</td>" +
                              "<td width=\"30\"></td>" +
                              "<td width=\"270\" height=\"50\" valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif;line-height: 20px; margin: 1em 0; font-size: 12px;\">{1}</td>" +
                            "</tr>";
            foreach (Property p in properties)
            {
                props += string.Format(propContent, p.BuildingName, p.Price);
            }
            

            
            string folder = Server.MapPath("~/email");
            string html = System.IO.File.ReadAllText(folder + @"\" + "PropertyList_Enquiry_Receipt.html");
            html = string.Format(html, user.Name, user.Email, user.Tel, user.Message, props);


            LinkedResource rHeader = new LinkedResource(folder + @"\" + "header.gif");
            rHeader.ContentId = "header";

            LinkedResource rOffice = new LinkedResource(folder + @"\" + "office.jpg");
            rOffice.ContentId = "office";

            LinkedResource rSpacer = new LinkedResource(folder + @"\" + "spacer.gif");
            rSpacer.ContentId = "spacer";
            AlternateView view = AlternateView.CreateAlternateViewFromString(html, Encoding.UTF8, "text/html");
            view.LinkedResources.Add(rHeader);
            view.LinkedResources.Add(rOffice);
            view.LinkedResources.Add(rSpacer);



            MailMessage msg = new MailMessage(user.Email, ConfigurationManager.AppSettings["AdminEmail"]);
            msg.Subject = "Property Enquiry";
            msg.AlternateViews.Add(view);
            msg.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            client.Send(msg);
            return Json("");
        }

        public Map DisplayPoints(List<Property> properties)
        {

            string displayPoints = "";
            int i = 1;

            foreach (Property p in properties)
            {

                StringBuilder sbHTML = new StringBuilder();
                int propId = p.Id;
                //Dim LocationName As String = EmptyIfStr(rdrResult.Item("LocationName"))

                string title = p.BuildingName;
                //Dim LocationDescription As String = EmptyIfStr(rdrResult.Item("LocationDescription"))
                string pc = p.Postcode;
                double lat = (p.Latitude == null ? 0 : p.Latitude.Value);
                double lon = (p.Longitude == null ? 0 : p.Longitude.Value);

                sbHTML.Append("<li><a id=\"{4}\" href=\"http://maps.google.co.uk/maps?ll={0},{1}\" title=\"{2}\">" + i.ToString() + " GREEN</a>");
                sbHTML.Append("	<div class=\"map-buble\">");
                sbHTML.Append("		<div class=\"image\">");
                sbHTML.Append("			<div class=\"company-thumb\">");
                sbHTML.Append("				<table cellspacing=\"0\"><tbody><tr><td>");
                sbHTML.Append("					" + title);
                sbHTML.Append("					</td></tr></tbody>");
                sbHTML.Append("				</table>");
                sbHTML.Append("			</div>");
                sbHTML.Append("		</div>");
                sbHTML.Append("		<div class=\"desc\">");
                sbHTML.Append("			<h2>" + title + "</h2>&nbsp;({3})");
                sbHTML.Append("		</div>");
                sbHTML.Append("	</div>");
                sbHTML.Append("</li>");
                string point = sbHTML.ToString();
                displayPoints += String.Format(point, lat, lon, title, "", propId);

                i++;
            }
            Map map = new Map();
            map.MapPoints = displayPoints;
            return map;

        }
        public JsonResult SendDetailEnquiry(PropertyListModel model)
        {
            EnquiryUser user = model.Enquiry;

            Basket basket = Basket.GetCart(this.HttpContext);
            Property property = null;
            string [] contacts = null;
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                property = db.Property.Find(user.Id);//this id is property id

                var arr = property.ContactIDs.Split(',');
                List<int> contactIds = new List<int>();
                foreach (string c in arr)
                {
                    contactIds.Add(int.Parse(c));
                }
                contacts = (from t in db.Team
                            where contactIds.Contains(t.Id)
                           select t.Email).ToArray();

            }
            if (contacts == null || contacts.Length == 0)
            {
                return Json("There are no contacts associated to this property. Please contact Astonrose");
            }

            string folder = Server.MapPath("~/email");
            string html = System.IO.File.ReadAllText(folder + @"\" + "Enquiry_Receipt.html");
            html = string.Format(html, user.Name, user.Email, user.Tel, user.Message, property.BuildingName, property.Price);
            

            LinkedResource rHeader = new LinkedResource(folder + @"\" + "header.gif");
            rHeader.ContentId = "header";

            LinkedResource rOffice = new LinkedResource(folder + @"\" + "office.jpg");
            rOffice.ContentId = "office";

            LinkedResource rSpacer = new LinkedResource(folder + @"\" + "spacer.gif");
            rSpacer.ContentId = "spacer";
            AlternateView view = AlternateView.CreateAlternateViewFromString(html, Encoding.UTF8, "text/html");
            view.LinkedResources.Add(rHeader);
            view.LinkedResources.Add(rOffice);
            view.LinkedResources.Add(rSpacer);


            
            MailMessage msg = new MailMessage(user.Email, string.Join(",", contacts));
            msg.Subject = "Property Enquiry";
            msg.AlternateViews.Add(view);
            msg.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            client.Send(msg);
            return Json("");
        }

        [HttpPost]
        public ActionResult _AutoCompleteAjaxLoading(string text)
        {
            using(AstonRoseCMSContext _db = new AstonRoseCMSContext)
            {

            var products = _db.AutoSuggestions;
                if (text.HasValue())
                {
                    products = products.Where((p) => p.ProductName.StartsWith(text));
                }
                return new JsonResult { Data = products.Select(p => p.ProductName).ToList() };
            }
            
        }

    }
}
