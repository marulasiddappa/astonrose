﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Controllers;
using System.Text.RegularExpressions;

namespace AstonRoseFrondEnd.Controllers
{
    public class CSRController : BaseController
    {
        //
        // GET: /CSR/
        //private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public ActionResult Index(int year = 0)
        {
            return View(GetModel(year));
        }

        public ActionResult Search(int year = 0)
        {
            return View(GetModel(year));
        }
        int pageSize = 3;
        public ActionResult Page(int pageIndex = 0, int pageCount = 0, int year = 0)
        {
            return View(GetModel(year, pageIndex, pageCount));
        }

        private CSRListModel GetModel(int year = 0, int pageIndex = 0, int pageCount = 0)
        {
            ViewBag.Year = year;

            CSRListModel model = new CSRListModel();
            model.FilteredList = GetArchivedCSR();
            ViewBag.MetaTitle = "CSR : Aston Rose";
            ViewBag.MetaDescription = "CSR : Aston Rose";
            ViewBag.PageTitle = "CSR  - ";
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                
                int cnt = (from n in db.CSR
                           where n.GoLive && (year == 0 || n.Date.Year == year) && n.Date.Year > 2008
                           orderby n.Date descending
                           select n.Id).Count();
                ViewBag.PageCount = (cnt % pageSize == 0 ? cnt / pageSize : (cnt / pageSize) + 1);
                ViewBag.PageIndex = pageIndex;

                model.CSRList = (from n in db.CSR
                                 where n.GoLive && (year == 0 || n.Date.Year == year)
                                 orderby n.Date descending
                                 select n).Skip(pageIndex * pageSize).Take(pageSize).ToList();
            }
            return model;
        }

        List<CSRFilter> GetArchivedCSR()
        {
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                return db.Database.SqlQuery<CSRFilter>("select Year, Count from (SELECT DATEPART(Year, Date) Year, Count(ID) [Count] " +
                    " FROM CSR where GoLive=1 " +
                    " GROUP BY DATEPART(Year, Date) " +

                    " ) as CSRTable  where Year > 2008 ORDER BY Year desc").ToList();
            }
        }

        public ActionResult CSRDetail(string title = "", int Id = 0)
        {


            CSRListModel model = new CSRListModel();
            model.FilteredList = GetArchivedCSR();
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                model.SelectedCSR = db.CSR.Find(Id);

                if (model.SelectedCSR != null)
                {
                    ViewBag.MetaTitle = model.SelectedCSR.MetaTitle;
                    ViewBag.MetaDescription = model.SelectedCSR.MetaDescription;

                    if (string.IsNullOrEmpty(model.SelectedCSR.MetaTitle))
                    {
                        ViewBag.MetaTitle = model.SelectedCSR.Title;
                    }

                    if (string.IsNullOrEmpty(model.SelectedCSR.MetaDescription))
                    {
                        ViewBag.MetaDescription = Regex.Replace(model.SelectedCSR.FullDescription, @"<(.|\n)*?>", "");
                    }
                    ViewBag.PageTitle = model.SelectedCSR.Title + " - ";
                }
            }
            return View(model);
        }

    }
}
