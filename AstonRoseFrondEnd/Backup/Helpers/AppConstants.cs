﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AstonRoseFrondEnd.Helpers
{
    public class AppConstants
    {
        static AppConstants()
        {
            Months.Add(1, "Jan");
            Months.Add(2, "Feb");
            Months.Add(3, "Mar");
            Months.Add(4, "Apr");
            Months.Add(5, "May");
            Months.Add(6, "Jun");
            Months.Add(7, "Jul");
            Months.Add(8, "Aug");
            Months.Add(9, "Sep");
            Months.Add(10, "Oct");
            Months.Add(11, "Nov");
            Months.Add(12, "Dec");
        }
        public const string Administrator = "Administrator";
        public const string SuperUser = "Super User";
        public const string AllRoles = "Administrator, Super User";
        public static Dictionary<int, string> Months = new Dictionary<int, string>();
        public const int PageSize = 10;
    }
}