﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class ServiceListModel
    {
        //public GridModel<Service> ServiceList { get; set; }
        //public Service SelectedService { get; set; }
        //public Team contact1 { get; set; }
        //public Team contact2 { get; set; }


        public List<ServiceView> ViewList { get; set; }
        public ServiceViewModel SelectedService { get; set; }

       
    }

    public class ServiceView
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class ServiceViewModel
    {
        public ServiceViewModel()
        {
            Images = new List<UploadedImage>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ContactIds { get; set; }
        public List<ServiceContact> Contacts { get; set; }
        public List<UploadedImage> Images { get; set; }
        public List<CaseStudy> CaseStudies { get; set; }

        public string Image { get; set; }
        public string CaseStudyTitle { get; set; }
        public string CaseStudyDescription { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        string servicesOfferedTitle1 = "";
        public string PDFBrochure { get; set; }
        public string ServicesOfferedTitle1
        {
            get
            {
                return (servicesOfferedTitle1 == null ? "" : servicesOfferedTitle1);

            }
            set { servicesOfferedTitle1 = value; }
        }

        string servicesOffered1 = "";
        public string ServicesOffered1
        {
            get
            {
                return (servicesOffered1 == null ? "" : servicesOffered1);

            }
            set { servicesOffered1 = value; }
        }


        string servicesOfferedTitle2 = "";
        public string ServicesOfferedTitle2
        {
            get
            {


                return (servicesOfferedTitle2 == null ? "" : servicesOfferedTitle2);
            }
            set { servicesOfferedTitle2 = value; }
        }

        public string ServicesOffered2
        {
            get
            {


                return (servicesOffered2 == null ? "" : servicesOffered2);
            }
            set { servicesOffered2 = value; }
        }
        string servicesOffered2 = "";




    }

    public class ServiceContact
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string EmailName { get; set; }
        public string EmailID { get; set; }

    }
}