﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AstonRoseFrondEnd.Models
{
    public class BasketProperty
    {
        [Key]
        public int Id { get; set; }
        public string BasketId { get; set; }
        public int PropertyId { get; set; }
        public DateTime DateCreated { get; set; }
    }
}