﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;
using System.ServiceModel.Syndication;

namespace AstonRoseFrondEnd.Models
{
    public class HomeModel
    {
        public News LatestNews { get; set; }
        public Team SelectedMember { get; set; }
        public List<Team> TeamMembers { get; set; }
        public Client SelectedClient { get; set; }
        public Offices SelectedOffice { get; set; }
        public List<TeamMember> Members { get; set; }
        public List<Client> Clients { get; set; }
        public List<Testimonials> Testimonials { get; set; }
        public IEnumerable<SyndicationItem> CoActorNews { get; set; }

        
    }
    public class TeamMember
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}