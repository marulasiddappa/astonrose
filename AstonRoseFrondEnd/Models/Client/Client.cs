﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class Client
    {
        public Client()
        {
            
        }

        [Key]
        //[ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Client Name")]
        [Required(ErrorMessage = "Client Name is Required")]
        public string Name { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }

        [Display(Name = "Logo")]
        public string Logo { get; set; }

        [Display(Name = "Website URL")]
        public string Website { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }
    }
}