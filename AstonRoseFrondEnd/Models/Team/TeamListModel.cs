﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;
using System.Web.WebPages;
using AstonRoseFrondEnd.Helpers;
using System.Web.Mvc;


namespace AstonRoseFrondEnd.Models
{
    public class TeamListModel
    {
        public TeamListModel()
        {
            
            DisplayOrder = AstonRoseCMSHelper.GetDisplayOrder();
        }
        public GridModel<Team> TeamList { get; set; }
        public List<SelectListItem> DisplayOrder { get; set; }

    }
}