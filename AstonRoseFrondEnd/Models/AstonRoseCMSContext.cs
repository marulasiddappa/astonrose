﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Models;

namespace AstonRoseFrondEnd.Models
{
    public class AstonRoseCMSContext : DbContext
    {
      
        public DbSet<Users> Users { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Testimonials> Testimonials { get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Property> Property { get; set; }
        public DbSet<Offices> Offices { get; set; }
        public DbSet<ClientType> ClientTypes { get; set; }
        public DbSet<CaseStudy> CaseStudies { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<UploadedImage> UploadedImages { get; set; }
        public DbSet<BasketProperty> BasketProperties { get; set; }
        public DbSet<CSR> CSR { get; set; }
        public DbSet<ClientTypeCaseStudy> ClientTypeCaseStudies { get; set; }
        public DbSet<ServiceCaseStudy> ServiceCaseStudies { get; set; }
        public DbSet<AutoSuggestion> AutoSuggestions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Users>().ToTable("Users");
            modelBuilder.Entity<News>().ToTable("News");
            modelBuilder.Entity<Testimonials>().ToTable("Testimonials");
            modelBuilder.Entity<Team>().ToTable("Team");
            modelBuilder.Entity<Client>().ToTable("Client");
            modelBuilder.Entity<Property>().ToTable("Property");
            modelBuilder.Entity<Offices>().ToTable("Offices");
            modelBuilder.Entity<Service>().ToTable("Services");
            modelBuilder.Entity<CaseStudy>().ToTable("CaseStudies");
            modelBuilder.Entity<ClientType>().ToTable("ClientTypes");
            modelBuilder.Entity<ActivityLog>().ToTable("ActivityLogs");
            modelBuilder.Entity<UploadedImage>().ToTable("UploadedImages");
            modelBuilder.Entity<BasketProperty>().ToTable("BasketProperties");
            modelBuilder.Entity<CSR>().ToTable("CSR");
            modelBuilder.Entity<ClientTypeCaseStudy>().ToTable("ClientTypeCaseStudies");
            modelBuilder.Entity<ServiceCaseStudy>().ToTable("ServiceCaseStudies");
            modelBuilder.Entity<AutoSuggestion>().ToTable("AutoSuggestions");
        }

        
    }
}