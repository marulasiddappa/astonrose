﻿using System.Collections.Generic;
using System.Web.Mvc;


namespace AstonRoseFrondEnd.Models
{
    public class PropertyView
    {
         public SearchParameters Search { get; set; }
         public ICollection<SolrProperty> SolrProperties { get; set; }
        
        public int TotalCount { get; set; }
        public IDictionary<string, ICollection<KeyValuePair<string, int>>> Facets { get; set; }
        public string DidYouMean { get; set; }
        public bool QueryError { get; set; }


        public string SortSelectedItem { get; set; }
        public IEnumerable<SelectListItem> SortItems { get; set; }

        public int PageSizeSelectedItem { get; set; }
        public IEnumerable<SelectListItem> PageSizeItems { get; set; }
        public IList<SelectListItem> TravelDistance { get; set; }

        public string PageResetURL { get; set; }
        public Map Map { get; set; }
        public PropertyFilter Filter { get; set; }

        public PropertyView()
        {
            Search = new SearchParameters();
            Facets = new Dictionary<string, ICollection<KeyValuePair<string, int>>>();
            SolrProperties = new List<SolrProperty>();
            TravelDistance = new List<SelectListItem>();
        }
       
    }
}