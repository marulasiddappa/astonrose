﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;
using AstonRoseFrondEnd.Models;

namespace AstonRoseFrondEnd.Models
{
    public class CSRListModel
    {
        public List<CSR> CSRList { get; set; }
        public List<CSRFilter> FilteredList { get; set; }
        public CSR SelectedCSR { get; set; }
    }

    public class CSRFilter : Filter
    {
    }

}