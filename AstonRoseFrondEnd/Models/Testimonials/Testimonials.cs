﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class Testimonials
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [Display(Name = "Name")]
        [AllowHtml]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Job Title is Required")]
        [Display(Name = "Job Title")]
        [AllowHtml]
        public string JobTitle { get; set; }

        //[Required(ErrorMessage = "Company Name is Required")]
        [Display(Name = "Company Name")]
        [AllowHtml]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Quote is Required")]
        [Display(Name = "Quote")]
        [AllowHtml]
        public string Quote { get; set; }

        [Display(Name = "PDF")]
        public string PDF
        {
            get;
            set;
        }

        public override string ToString()
        {
            string returnVal = "";
            returnVal = (string.IsNullOrEmpty(JobTitle) ? "" : JobTitle + "<br/>");
            returnVal = (string.IsNullOrEmpty(CompanyName) ? "" : CompanyName + "<br/>");
            returnVal += (string.IsNullOrEmpty(Quote) ? "" : Quote + "<br/>");
            return returnVal;
        }
        
    }
}