﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolrNet.Attributes;
using System.IO;
namespace AstonRoseFrondEnd.Models
{
    public class SolrProperty
    {
        [SolrUniqueKey("id")]
        public string Id { get; set; }

        [SolrField("buildingname")]
        public string BuildingName { get; set; }

        [SolrField("category")]
        public string Category { get; set; }

        [SolrField("tenure")]
        public string Tenure { get; set; }

        [SolrField("town")]
        public string Town { get; set; }

        [SolrField("postcode")]
        public string Postcode { get; set; }

        [SolrField("propdescription")]
        public string PropDescription { get; set; }

        [SolrField("proplocation")]
        public string PropLocation { get; set; }
        

        [SolrField("proptype")]
        public string PropType { get; set; }

        [SolrField("propsize")]
        public string PropSize { get; set; }


        [SolrField("propstatus")]
        public string PropStatus { get; set; }

        [SolrField("region")]
        public string Region { get; set; }


        [SolrField("propprice")]
        public string PropPrice { get; set; }

        [SolrField("priceflag")]
        public string PriceFlag { get; set; }


        [SolrField("propimage")]
        public string PropImage { get; set; }

        [SolrField("video")]
        public string Video { get; set; }



        [SolrField("address1")]
        public string Address1 { get; set; }

        [SolrField("address2")]
        public string Address2 { get; set; }

        [SolrField("address3")]
        public string Address3 { get; set; }



        [SolrField("county")]
        public string County { get; set; }



        [SolrField("pdf")]
        public string PDF { get; set; }

        [SolrField("epc")]
        public string EPC { get; set; }

        [SolrField("contactids")]
        public string ContactIDs { get; set; }

        //[SolrField("incentive")]
        //public string Incentive { get; set; }

        //[SolrField("datecreated")]
        //public DateTime DateCreated { get; set; }


        [SolrField("metatitle")]
        public string MetaTitle { get; set; }

        [SolrField("metadescription")]
        public string MetaDescription { get; set; }

        [SolrField("metakeywords")]
        public List<string> MetaKeywords { get; set; }

    }
}
