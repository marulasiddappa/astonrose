﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class NewsListModel
    {
        public List<News> NewsList { get; set; }
        
        public List<NewsFilter> FilteredList { get; set; }
        public News SelectedNews { get; set; }
    }

    public class NewsFilter:Filter
    {
    }

    public class Filter
    {
        public int Year { get; set; }
        public int Count { get; set; }
    }
}