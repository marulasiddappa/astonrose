﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;

namespace AstonRoseFrondEnd.Models
{
    public class CaseStudyListModel
    {
        public GridModel<CaseStudy> CaseStudyList { get; set; }
    }
}