﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AstonRoseFrondEnd.Models;
using AstonRoseFrondEnd.Helpers;

namespace AstonRoseFrondEnd.Models
{
    public class Basket
    {

        //AstonRoseCMSContext db = new AstonRoseCMSContext();
        string BasketPropertyId { get; set; }

        public const string BasketSessionKey = "BasketId";

        public static Basket GetCart(HttpContextBase context)
        {
            var cart = new Basket();
            cart.BasketPropertyId = cart.GetBasketId(context);
            return cart;
        }

        private string GetBasketId(HttpContextBase context)
        {
            if (context.Session[BasketSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[BasketSessionKey] = context.User.Identity.Name;
                }
                else
                {
                    // Generate a new random GUID using System.Guid class
                    Guid tempCartId = Guid.NewGuid();

                    // Send tempCartId back to client as a cookie
                    context.Session[BasketSessionKey] = tempCartId.ToString();
                }
            }

            return context.Session[BasketSessionKey].ToString();
        }

        public void AddToBasket(Property property)
        {
            // Get the matching cart and album instances
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                var basketItem = db.BasketProperties.SingleOrDefault(
    c => c.BasketId == BasketPropertyId
    && c.PropertyId == property.Id);

                if (basketItem == null)
                {
                    // Create a new cart item if no cart item exists
                    basketItem = new BasketProperty
                    {
                        PropertyId = property.Id,
                        BasketId = BasketPropertyId,
                        DateCreated = DateTime.Now
                    };

                    db.BasketProperties.Add(basketItem);
                }


                // Save changes
                db.SaveChanges();
            }
        }


        public void RemoveFromBasket(int propertyId)
        {
            // Get the matching cart and album instances
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                var basketItem = db.BasketProperties.SingleOrDefault(
    c => c.BasketId == BasketPropertyId
    && c.PropertyId == propertyId);

                if (basketItem != null)
                {
                    // Create a new cart item if no cart item exists
                    db.BasketProperties.Remove(basketItem);
                }


                // Save changes
                db.SaveChanges();
            }
        }


        public void EmptyBasket()
        {
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                var basketItems = db.BasketProperties.Where(property => property.BasketId == BasketPropertyId);

                foreach (var basketItem in basketItems)
                {
                    db.BasketProperties.Remove(basketItem);
                }

                // Save changes
                db.SaveChanges();
            }
        }
        public int GetCount()
        {
            // Get the count of each item in the cart and sum them up
            
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                int? count = (from b in db.BasketProperties
                              where b.BasketId == BasketPropertyId
                              select (int?)b.Id).Count();
                return count ?? 0;
            }   

            // Return 0 if all entries are null
            
        }
        public List<BasketProperty> GetBasketItems()
        {
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                return db.BasketProperties.Where(basketProperty => basketProperty.BasketId == BasketPropertyId).ToList();
            }
        }

        public void MigrateCart(string userName)
        {
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                var basket = db.BasketProperties.Where(c => c.BasketId == BasketPropertyId);

                foreach (BasketProperty item in basket)
                {
                    item.BasketId = userName;
                }
                db.SaveChanges();
            }
        }

        internal MyProperty GetMyBasketItems(int pageIndex)
        {
            MyProperty myProperties = new MyProperty();
            myProperties.Pagination = new Pagination() { CurrentPageIndex = pageIndex }; 

            if (myProperties.Pagination.CurrentPageIndex == 0)
                myProperties.Pagination.CurrentPageIndex = 1;
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                int cnt = (from p in db.Property
                           join bp in db.BasketProperties on p.Id equals bp.PropertyId
                           where bp.BasketId == BasketPropertyId
                           select p).Count();


                myProperties.Pagination.TotalPages = (cnt % PageSize == 0 ? cnt / PageSize : (cnt / PageSize) + 1);
                int toSkip = (myProperties.Pagination.CurrentPageIndex - 1) * PageSize;

                myProperties.MyProperties = (from p in db.Property
                                             join bp in db.BasketProperties on p.Id equals bp.PropertyId
                                             where bp.BasketId == BasketPropertyId
                                             orderby bp.Id
                                             select p).Skip(toSkip).Take(PageSize).ToList();
            }
            return myProperties;
            //return db.BasketProperties.Where(basketProperty => basketProperty.BasketId == BasketPropertyId).ToList();
        }
        internal List<Property> GetMyBasketItems()
        {
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                return (from p in db.Property
                        join bp in db.BasketProperties on p.Id equals bp.PropertyId
                        where bp.BasketId == BasketPropertyId
                        select p).ToList();
            }
            //return db.BasketProperties.Where(basketProperty => basketProperty.BasketId == BasketPropertyId).ToList();
        }

        public const int PageSize = AppConstants.PageSize;

        internal bool CheckIfAdded(int p)
        {
            using (AstonRoseCMSContext db = new AstonRoseCMSContext())
            {
                var basketProperty = (from bp in db.BasketProperties
                                      where bp.PropertyId == p && bp.BasketId == BasketPropertyId
                                      select bp).FirstOrDefault();
                if (basketProperty == null)
                    return false;
                else
                    return true;
            }
        }
    }


}