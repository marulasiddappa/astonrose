﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Reflection;
using System.Diagnostics;

namespace AstonRoseCMS.Helpers
{
    public class AppConstants
    {
        public const string Administrator = "Administrator";
        public const string SuperUser = "Super User";
        public const string AllRoles = "Administrator, Super User";
    }

    public interface IEntitySorter<TEntity>
    {
        IOrderedQueryable<TEntity> Sort(IQueryable<TEntity> collection);
    }
    internal enum SortDirection
    {
        Ascending,
        Descending
    }

    public static class EntitySorter<T>
    {
        public static IEntitySorter<T> AsQueryable()
        {
            return new EmptyEntitySorter();
        }

        public static IEntitySorter<T> OrderBy<TKey>(
            Expression<Func<T, TKey>> keySelector)
        {
            return new OrderBySorter<T, TKey>(keySelector,
                SortDirection.Ascending);
        }

        public static IEntitySorter<T> OrderByDescending<TKey>(
            Expression<Func<T, TKey>> keySelector)
        {
            return new OrderBySorter<T, TKey>(keySelector,
                SortDirection.Descending);
        }

        public static IEntitySorter<T> OrderBy(string propertyName)
        {
            var builder = new EntitySorterBuilder<T>(propertyName);

            builder.Direction = SortDirection.Ascending;

            return builder.BuildOrderByEntitySorter();
        }

        public static IEntitySorter<T> OrderByDescending(
            string propertyName)
        {
            var builder = new EntitySorterBuilder<T>(propertyName);

            builder.Direction = SortDirection.Descending;

            return builder.BuildOrderByEntitySorter();
        }

        private sealed class EmptyEntitySorter : IEntitySorter<T>
        {
            public IOrderedQueryable<T> Sort(
                IQueryable<T> collection)
            {
                string exceptionMessage = "OrderBy should be called.";

                throw new InvalidOperationException(exceptionMessage);
            }
        }
    }

    public static class EntitySorterExtensions
    {
        public static IEntitySorter<T> OrderBy<T, TKey>(
            this IEntitySorter<T> sorter,
            Expression<Func<T, TKey>> keySelector)
        {
            return EntitySorter<T>.OrderBy(keySelector);
        }

        public static IEntitySorter<T> OrderByDescending<T, TKey>(
            this IEntitySorter<T> sorter,
            Expression<Func<T, TKey>> keySelector)
        {
            return EntitySorter<T>.OrderByDescending(keySelector);
        }

        public static IEntitySorter<T> ThenBy<T, TKey>(
            this IEntitySorter<T> sorter,
            Expression<Func<T, TKey>> keySelector)
        {
            return new ThenBySorter<T, TKey>(sorter,
                keySelector, SortDirection.Ascending);
        }

        public static IEntitySorter<T> ThenByDescending<T, TKey>(
            this IEntitySorter<T> sorter,
            Expression<Func<T, TKey>> keySelector)
        {
            return new ThenBySorter<T, TKey>(sorter,
                keySelector, SortDirection.Descending);
        }

        public static IEntitySorter<T> ThenBy<T>(
            this IEntitySorter<T> sorter, string propertyName)
        {
            var builder = new EntitySorterBuilder<T>(propertyName);

            builder.Direction = SortDirection.Ascending;

            return builder.BuildThenByEntitySorter(sorter);
        }

        public static IEntitySorter<T> ThenByDescending<T>(
            this IEntitySorter<T> sorter, string propertyName)
        {
            var builder = new EntitySorterBuilder<T>(propertyName);

            builder.Direction = SortDirection.Descending;

            return builder.BuildThenByEntitySorter(sorter);
        }
    }

    internal class OrderBySorter<T, TKey> : IEntitySorter<T>
    {
        private readonly Expression<Func<T, TKey>> keySelector;
        private readonly SortDirection direction;

        public OrderBySorter(Expression<Func<T, TKey>> selector,
            SortDirection direction)
        {
            this.keySelector = selector;
            this.direction = direction;
        }

        public IOrderedQueryable<T> Sort(IQueryable<T> col)
        {
            if (this.direction == SortDirection.Ascending)
            {
                return Queryable.OrderBy(col, this.keySelector);
            }
            else
            {
                return Queryable.OrderByDescending(col,
                    this.keySelector);
            }
        }
    }

    internal sealed class ThenBySorter<T, TKey> : IEntitySorter<T>
    {
        private readonly IEntitySorter<T> baseSorter;
        private readonly Expression<Func<T, TKey>> keySelector;
        private readonly SortDirection direction;

        public ThenBySorter(IEntitySorter<T> baseSorter,
            Expression<Func<T, TKey>> selector, SortDirection direction)
        {
            this.baseSorter = baseSorter;
            this.keySelector = selector;
            this.direction = direction;
        }

        public IOrderedQueryable<T> Sort(IQueryable<T> col)
        {
            var sorted = this.baseSorter.Sort(col);

            if (this.direction == SortDirection.Ascending)
            {
                return Queryable.ThenBy(sorted, this.keySelector);
            }
            else
            {
                return Queryable.ThenByDescending(sorted,
                    this.keySelector);
            }
        }
    }
    internal class EntitySorterBuilder<T>
    {
        private readonly Type keyType;
        private readonly LambdaExpression keySelector;

        public EntitySorterBuilder(string propertyName)
        {
            List<MethodInfo> propertyAccessors =
                GetPropertyAccessors(propertyName);

            this.keyType = propertyAccessors.Last().ReturnType;

            ILambdaBuilder builder = CreateLambdaBuilder(keyType);

            this.keySelector =
                builder.BuildLambda(propertyAccessors);
        }

        private interface ILambdaBuilder
        {
            LambdaExpression BuildLambda(
                IEnumerable<MethodInfo> propertyAccessors);
        }

        public SortDirection Direction { get; set; }

        public IEntitySorter<T> BuildOrderByEntitySorter()
        {
            Type[] typeArgs = new[] { typeof(T), this.keyType };

            Type sortType =
                typeof(OrderBySorter<,>).MakeGenericType(typeArgs);

            return (IEntitySorter<T>)Activator.CreateInstance(sortType,
                this.keySelector, this.Direction);
        }

        public IEntitySorter<T> BuildThenByEntitySorter(
            IEntitySorter<T> baseSorter)
        {
            Type[] typeArgs = new[] { typeof(T), this.keyType };

            Type sortType =
                typeof(ThenBySorter<,>).MakeGenericType(typeArgs);

            return (IEntitySorter<T>)Activator.CreateInstance(sortType,
                baseSorter, this.keySelector, this.Direction);
        }

        private static ILambdaBuilder CreateLambdaBuilder(Type keyType)
        {
            Type[] typeArgs = new[] { typeof(T), keyType };

            Type builderType =
                typeof(LambdaBuilder<>).MakeGenericType(typeArgs);

            return (ILambdaBuilder)Activator.CreateInstance(builderType);
        }

        private static List<MethodInfo> GetPropertyAccessors(
            string propertyName)
        {
            try
            {
                return GetPropertyAccessorsFromChain(propertyName);
            }
            catch (InvalidOperationException ex)
            {
                string message = propertyName +
                    " could not be parsed. " + ex.Message;

                // We throw a more expressive exception at this level.
                throw new ArgumentException(message, "propertyName");
            }
        }

        private static List<MethodInfo> GetPropertyAccessorsFromChain(
            string propertyNameChain)
        {
            var propertyAccessors = new List<MethodInfo>();

            var declaringType = typeof(T);

            foreach (string name in propertyNameChain.Split('.'))
            {
                var accessor = GetPropertyAccessor(declaringType, name);

                propertyAccessors.Add(accessor);

                declaringType = accessor.ReturnType;
            }

            return propertyAccessors;
        }

        private static MethodInfo GetPropertyAccessor(Type declaringType,
            string propertyName)
        {
            var prop = GetPropertyByName(declaringType, propertyName);

            return GetPropertyGetter(prop);
        }

        private static PropertyInfo GetPropertyByName(Type declaringType,
            string propertyName)
        {
            BindingFlags flags = BindingFlags.IgnoreCase |
                BindingFlags.Instance | BindingFlags.Public;

            var prop = declaringType.GetProperty(propertyName, flags);

            if (prop == null)
            {
                string exceptionMessage = string.Format(
                    "{0} does not contain a property named '{1}'.",
                    declaringType, propertyName);

                throw new InvalidOperationException(exceptionMessage);
            }

            return prop;
        }

        private static MethodInfo GetPropertyGetter(PropertyInfo property)
        {
            var propertyAccessor = property.GetGetMethod();

            if (propertyAccessor == null)
            {
                string exceptionMessage = string.Format(
                    "The property '{1}' does not contain a getter.",
                    property.Name);

                throw new InvalidOperationException(exceptionMessage);
            }

            return propertyAccessor;
        }

        private sealed class LambdaBuilder<TKey> : ILambdaBuilder
        {
            public LambdaExpression BuildLambda(
                IEnumerable<MethodInfo> propertyAccessors)
            {
                ParameterExpression parameterExpression =
                    Expression.Parameter(typeof(T), "entity");

                Expression propertyExpression = BuildPropertyExpression(
                    propertyAccessors, parameterExpression);

                return Expression.Lambda<Func<T, TKey>>(
                    propertyExpression, new[] { parameterExpression });
            }

            private static Expression BuildPropertyExpression(
                IEnumerable<MethodInfo> propertyAccessors,
                ParameterExpression parameterExpression)
            {
                Expression propertyExpression = null;

                foreach (var propertyAccessor in propertyAccessors)
                {
                    var innerExpression =
                        propertyExpression ?? parameterExpression;

                    propertyExpression = Expression.Property(
                        innerExpression, propertyAccessor);
                }

                return propertyExpression;
            }
        }
    }

    public interface IEntityFilter<TEntity>
    {
        /// <summary>Filters the specified collection.</summary>
        /// <param name="collection">The collection.</param>
        /// <returns>A filtered collection.</returns>
        IQueryable<TEntity> Filter(IQueryable<TEntity> collection);
    }

    /// <summary>Enables filtering of entities.</summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public static class EntityFilter<TEntity>
    {
        /// <summary>
        /// Returns a <see cref="IEntityFilter{TEntity}"/> instance that allows construction of
        /// <see cref="IEntityFilter{TEntity}"/> objects though the use of LINQ syntax.
        /// </summary>
        /// <returns>A <see cref="IEntityFilter{TEntity}"/> instance.</returns>
        public static IEntityFilter<TEntity> AsQueryable()
        {
            return new EmptyEntityFilter();
        }

        /// <summary>
        /// Returns a <see cref="IEntityFilter{TEntity}"/> that filters a sequence based on a predicate.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>A new <see cref="IEntityFilter{TEntity}"/>.</returns>
        public static IEntityFilter<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            return new WhereEntityFilter<TEntity>(predicate);
        }

        /// <summary>An empty entity filter.</summary>
        [DebuggerDisplay("EntityFilter ( Unfiltered )")]
        private sealed class EmptyEntityFilter : IEntityFilter<TEntity>
        {
            /// <summary>Filters the specified collection.</summary>
            /// <param name="collection">The collection.</param>
            /// <returns>A filtered collection.</returns>
            public IQueryable<TEntity> Filter(IQueryable<TEntity> collection)
            {
                // We don't filter, but simply return the collection.
                return collection;
            }

            /// <summary>Returns an empty string.</summary>
            /// <returns>An empty string.</returns>
            public override string ToString()
            {
                return string.Empty;
            }
        }
    }

    /// <summary>
    /// Extension methods for the <see cref="IEntityFilter{TEntity}"/> interface.
    /// </summary>
    public static class EntityFilterExtensions
    {
        /// <summary>
        /// Returns a <see cref="IEntityFilter{TEntity}"/> that filters a sequence based on a predicate.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="baseFilter">The base filter.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns>A new <see cref="IEntityFilter{TEntity}"/>.</returns>
        public static IEntityFilter<TEntity> Where<TEntity>(this IEntityFilter<TEntity> baseFilter,
            Expression<Func<TEntity, bool>> predicate)
        {
            if (baseFilter == null)
            {
                throw new ArgumentNullException("baseFilter");
            }

            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            return new WhereEntityFilter<TEntity>(baseFilter, predicate);
        }
    }

    /// <summary>
    /// Filters the collection using a predicate.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    [DebuggerDisplay("EntityFilter ( where {ToString()} )")]
    internal sealed class WhereEntityFilter<TEntity> : IEntityFilter<TEntity>
    {
        private readonly IEntityFilter<TEntity> baseFilter;
        private readonly Expression<Func<TEntity, bool>> predicate;

        /// <summary>Initializes a new instance of the <see cref="WhereEntityFilter{TEntity}"/> class.</summary>
        /// <param name="predicate">The predicate.</param>
        public WhereEntityFilter(Expression<Func<TEntity, bool>> predicate)
        {
            this.predicate = predicate;
        }

        /// <summary>Initializes a new instance of the <see cref="WhereEntityFilter{TEntity}"/> class.</summary>
        /// <param name="baseFilter">The base filter.</param>
        /// <param name="predicate">The predicate.</param>
        public WhereEntityFilter(IEntityFilter<TEntity> baseFilter, Expression<Func<TEntity, bool>> predicate)
        {
            this.baseFilter = baseFilter;
            this.predicate = predicate;
        }

        /// <summary>Filters the specified collection.</summary>
        /// <param name="collection">The collection.</param>
        /// <returns>A filtered collection.</returns>
        public IQueryable<TEntity> Filter(IQueryable<TEntity> collection)
        {
            if (this.baseFilter == null)
            {
                return collection.Where(this.predicate);
            }
            else
            {
                return this.baseFilter.Filter(collection).Where(this.predicate);
            }
        }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            string baseFilterPresentation =
                this.baseFilter != null ? this.baseFilter.ToString() : string.Empty;

            // The returned string is used in de DebuggerDisplay.
            if (!string.IsNullOrEmpty(baseFilterPresentation))
            {
                return baseFilterPresentation + ", " + this.predicate.ToString();
            }
            else
            {
                return this.predicate.ToString();
            }
        }
    }

}