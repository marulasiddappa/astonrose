﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;
using System.Globalization;
using AstonRoseCMS.Models;

namespace AstonRoseCMS.Helpers
{
    public class AstonRoseCMSHelper
    {
        public static List<SelectListItem> GetTitleDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Mr", Value = "Mr" });
            _ReturnList.Add(new SelectListItem() { Text = "Mrs", Value = "Mrs" });
            _ReturnList.Add(new SelectListItem() { Text = "Miss", Value = "Miss" });
            _ReturnList.Add(new SelectListItem() { Text = "Ms", Value = "Ms" });
            _ReturnList.Add(new SelectListItem() { Text = "Dr", Value = "Dr" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyRegionDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "All", Value = "All" });
            _ReturnList.Add(new SelectListItem() { Text = "London (Central)", Value = "London (Central)" });
            _ReturnList.Add(new SelectListItem() { Text = "London (Greater)", Value = "London (Greater)" });
            _ReturnList.Add(new SelectListItem() { Text = "South", Value = "South" });
            _ReturnList.Add(new SelectListItem() { Text = "South East", Value = "South East" });
            _ReturnList.Add(new SelectListItem() { Text = "South West", Value = "South West" });
            _ReturnList.Add(new SelectListItem() { Text = "East Anglia", Value = "East Anglia" });
            _ReturnList.Add(new SelectListItem() { Text = "East Midlands", Value = "East Midlands" });
            _ReturnList.Add(new SelectListItem() { Text = "West Midlands", Value = "West Midlands" });
            _ReturnList.Add(new SelectListItem() { Text = "Wales", Value = "Wales" });
            _ReturnList.Add(new SelectListItem() { Text = "North West", Value = "North West" });
            _ReturnList.Add(new SelectListItem() { Text = "North East", Value = "North East" });
            _ReturnList.Add(new SelectListItem() { Text = "Outside of UK", Value = "Outside of UK" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyTypeDropdowns(bool includeAll = false)
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            if (includeAll)
            {
                _ReturnList.Add(new SelectListItem() { Text = "All", Value = "" });
            }
            _ReturnList.Add(new SelectListItem() { Text = "Offices", Value = "Offices" });
            _ReturnList.Add(new SelectListItem() { Text = "Retail", Value = "Retail" });
            _ReturnList.Add(new SelectListItem() { Text = "Industrial", Value = "Industrial" });
            _ReturnList.Add(new SelectListItem() { Text = "Leisure", Value = "Leisure" });
            _ReturnList.Add(new SelectListItem() { Text = "Mixed Use", Value = "Mixed Use" });
            _ReturnList.Add(new SelectListItem() { Text = "Land", Value = "Land" });
            _ReturnList.Add(new SelectListItem() { Text = "Residential", Value = "Residential" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyCategoryDropdowns(bool includeAll = false)
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            if (includeAll)
            {
                _ReturnList.Add(new SelectListItem() { Text = "All", Value = "" });
            }
            _ReturnList.Add(new SelectListItem() { Text = "Sales/Let", Value = "Sales/Let" });
            _ReturnList.Add(new SelectListItem() { Text = "Investment", Value = "Investment" });

            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyTenureDropdowns(bool includeAll = false)
        {

            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            if (includeAll)
            {
                _ReturnList.Add(new SelectListItem() { Text = "All", Value = "" });
            }
            _ReturnList.Add(new SelectListItem() { Text = "Sale", Value = "Sale" });
            _ReturnList.Add(new SelectListItem() { Text = "To let", Value = "To let" });
            _ReturnList.Add(new SelectListItem() { Text = "Sale/To let", Value = "Sale/To let" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertyStatusDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Available", Value = "Available" });
            _ReturnList.Add(new SelectListItem() { Text = "Under Offer", Value = "Under Offer" });
            _ReturnList.Add(new SelectListItem() { Text = "Reserved", Value = "Reserved" });
            _ReturnList.Add(new SelectListItem() { Text = "Sold", Value = "Sold" });
            _ReturnList.Add(new SelectListItem() { Text = "Let", Value = "Let" });
            _ReturnList.Add(new SelectListItem() { Text = "Not Released", Value = "Not Released" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetPropertySizeDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "250-500 sq ft (23.23-46.45 sq m)", Value = "250-500 sq ft (23.23-46.45 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "500-2,000 sq ft (46.45-185.80 sq m)", Value = "500-2,000 sq ft (46.45-185.80 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "2,000-5,000 sq ft (185.80-464.50 sq m)", Value = "2,000-5,000 sq ft (185.80-464.50 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "5,000-10,000 sq ft (464.5-929.03 sq m)", Value = "5,000-10,000 sq ft (464.5-929.03 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "10,000-15,000 sq ft (929.03-1393.50 sq m)", Value = "10,000-15,000 sq ft (929.03-1393.50 sq m)" });
            _ReturnList.Add(new SelectListItem() { Text = "15,000 sq ft + (1393.50 sq m +)", Value = "15,000 sq ft + (1393.50 sq m +)" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetTeamTypeDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Director", Value = "Director" });
            _ReturnList.Add(new SelectListItem() { Text = "Associate Director", Value = "Associate Director" });
            _ReturnList.Add(new SelectListItem() { Text = "Team Member", Value = "Team Member" });
            return _ReturnList;
        }

        public static List<SelectListItem> GetDropdowns()
        {
            List<SelectListItem> _ReturnList = new List<SelectListItem>();
            _ReturnList.Add(new SelectListItem() { Text = "Director", Value = "Director" });
            _ReturnList.Add(new SelectListItem() { Text = "Team Member", Value = "Team Member" });
            return _ReturnList;
        }

        public static List<string> GetCountryList()
        {
            List<string> list = new List<string>();
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo info in cultures)
            {
                //if (info.IsNeutralCulture) continue;
                RegionInfo info2 = new RegionInfo(info.LCID);
                if (!list.Contains(info2.EnglishName))
                {
                    list.Add(info2.EnglishName);
                }
            }
            list = (from l in list orderby l ascending select l).ToList();
            return list;
        }

        public static List<System.Web.Mvc.SelectListItem> GetDisplayOrder()
        {
            List<System.Web.Mvc.SelectListItem> _ReturnList = new List<System.Web.Mvc.SelectListItem>();
            for (int i = 1; i <= 20; i++)
            {
                _ReturnList.Add(new System.Web.Mvc.SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }

            return _ReturnList;
        }


        public static System.Collections.IEnumerable GetTowns(bool p)
        {
            using (AstonRoseCMSContext context = new AstonRoseCMSContext())
            {

                var _property = (from j in context.Property

                                 select new System.Web.Mvc.SelectListItem() { Text = j.Town, Value = j.Town }).Distinct().ToList();
                var empty = (from t in _property
                             where t.Value == ""
                             select t).FirstOrDefault();
                if (empty != null)
                    _property.Remove(empty);

                _property.Insert(0, new System.Web.Mvc.SelectListItem() { Text = "All", Value = "" });


                return _property;
            }
        }
    }
}