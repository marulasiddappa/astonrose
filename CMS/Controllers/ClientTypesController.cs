﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Configuration;
using Telerik.Web.Mvc;
using AstonRoseCMS.Helpers;

namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class ClientTypesController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);
        //
        // GET: /ClientTypes/

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            

            var model = new CLientTypeListModel();
            model.ClientTypeList = new GridModel<ClientType>
            {
                Data = db.ClientTypes.OrderBy(n => n.Id).Skip(0).Take(GridPageSize).ToList(),
                Total = db.ClientTypes.Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, CLientTypeListModel model)
        {
            //var caseStudyList = GetCaseStudyList();
            var gridmodel = new GridModel<ClientType>();
            gridmodel.Data = db.ClientTypes.OrderBy(n => n.Id).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.ClientTypes.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }


        //
        // GET: /ClientTypes/Create

        public ActionResult Create()
        {
            ClientType clientType = new ClientType();
            GetClientTypeModelRequiredInfo(clientType);
            return View(clientType);
        }

        //
        // POST: /ClientTypes/Create

        [HttpPost]
        public ActionResult Create(ClientType clientType, int[] selectedContact, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    clientType.PDFBrochure = saveFile(Request.Files["PDFUpload"]);
                }
                if (selectedContact != null)
                {
                    clientType.ContactIds = string.Join(",", selectedContact);
                }
                clientType.DateCreated = DateTime.Now;
                clientType.DateModified = DateTime.Now;
                clientType.CreatedBy = User.Identity.Name;
                clientType.ModifiedBy = User.Identity.Name;
                db.ClientTypes.Add(clientType);
                db.SaveChanges();

                if (attachments != null)
                {
                    foreach (HttpPostedFileBase file in attachments)
                    {
                        UploadedImage image = new UploadedImage();
                        image.ImageName = saveFile(file);
                        image.ReferenceId = clientType.Id;
                        image.Type = "ClientType";
                        db.UploadedImages.Add(image);
                    }
                }
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new Client Type", Type = "ClientType", ReferenceID = clientType.Id, ReferenceKey = clientType.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            GetClientTypeModelRequiredInfo(clientType);
            return View(clientType);
        }

        //
        // GET: /ClientTypes/Edit/5

        public ActionResult Edit(int id)
        {
            ClientType clientType = db.ClientTypes.Find(id);
            clientType.UploadedImages = (from i in db.UploadedImages
                                         where i.ReferenceId == id && i.Type == "ClientType"
                                         select i).ToList();
            GetClientTypeModelRequiredInfo(clientType);
            return View(clientType);
        }

        public List<Team> OfficesContactList()
        {
            List<Team> officesContactList = new List<Team>();
            officesContactList = db.Team.Where(t => t.OfficeContactFlag == true).ToList();
            return officesContactList;
        }
        private ClientType GetClientTypeModelRequiredInfo(ClientType model)
        {
         
            

            var contactList = from c in db.Team
                              where c.OfficeContactFlag == true
                              select new { Text = c.FirstName + " " + c.LastName, Value = c.Id };

            foreach (var t in contactList)
            {
                model.AvailableContacts1.Add(new SelectListItem() { Text = t.Text, Value = t.Value.ToString() });
            }

            if (model.ContactIds != null && model.ContactIds != string.Empty)
            {
                model.ContactSelectedIdList = model.ContactIds.Split(',').ToList();
            }
            else
            {
                model.ContactSelectedIdList = new List<string>();
            }

            return model;
        }
        //
        // POST: /ClientTypes/Edit/5

        [HttpPost]
        public ActionResult Edit(ClientType clientType, int[] selectedContact, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                var dbClientType = db.ClientTypes.Find(clientType.Id);
                UpdateModel(dbClientType);
                if (attachments != null)
                {
                    foreach (HttpPostedFileBase file in attachments)
                    {
                        UploadedImage image = new UploadedImage();
                        image.ImageName = saveFile(file);
                        image.ReferenceId = dbClientType.Id;
                        image.Type = "ClientType";
                        db.UploadedImages.Add(image);
                    }
                }

                string oldPDFUpload = Request["oldPDFUpload"];
                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    dbClientType.PDFBrochure = updateFile(oldPDFUpload, Request.Files["PDFUpload"]);
                }
                else
                {
                    dbClientType.PDFBrochure = oldPDFUpload;
                }


                if (selectedContact != null)
                {
                    dbClientType.ContactIds = string.Join(",", selectedContact);
                }
                dbClientType.DateModified = DateTime.Now;
                dbClientType.ModifiedBy = User.Identity.Name;
                db.SaveChanges();
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a Client Type", Type = "ClientType", ReferenceID = clientType.Id, ReferenceKey = clientType.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            GetClientTypeModelRequiredInfo(clientType);
            return View(clientType);
        }

        //
        // GET: /ClientTypes/Delete/5
        public ActionResult DeletePDF(int id, string filename)
        {
            deleteFile(filename);
            ClientType clientType = db.ClientTypes.Find(id);
            clientType.PDFBrochure = null;
            db.Entry(clientType).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a PDF", Type = "Client Type", ReferenceID = clientType.Id, ReferenceKey = clientType.Title, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }
        public ActionResult Delete(int id)
        {
            ClientType clientType = db.ClientTypes.Find(id);

            
            List<UploadedImage> uploadedImages = (from i in db.UploadedImages
                                         where i.ReferenceId == id && i.Type == "ClientType"
                                         select i).ToList();

            foreach (UploadedImage image in uploadedImages)
            {
                deleteFile(image.ImageName);
                db.UploadedImages.Remove(image);
            }
            
            db.ClientTypes.Remove(clientType);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Client Type", Type = "ClientType", ReferenceID = clientType.Id, ReferenceKey = clientType.Title };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // POST: /ClientTypes/Delete/5

        
        public ActionResult DeleteImage(int imageId,int clientTypeId, string filename)
        {
            deleteFile(filename);
            //ClientType clientType = db.ClientTypes.Find(id);
            //clientType.DateModified = DateTime.Now;
            //clientType.ModifiedBy = User.Identity.Name;
            //clientType.Image = null;
            //db.Entry(clientType).State = EntityState.Modified;

            UploadedImage image = db.UploadedImages.Find(imageId);
            db.UploadedImages.Remove(image);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Client Type's Image", Type = "ClientType Image", ReferenceID = image.Id, ReferenceKey = image.ImageName, ReferenceImageName=filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = clientTypeId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}