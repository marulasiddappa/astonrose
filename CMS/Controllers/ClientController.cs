﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Configuration;
using Telerik.Web.Mvc;
using System.Data;
using AstonRoseCMS.Helpers;

namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class ClientController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var clientlist = GetClientList();
            var model = new ClientListModel();
            model.ClientList = new GridModel<Client>
            {
                Data = db.Client.OrderBy(c => c.Id).Skip(0).Take(GridPageSize).ToList(),
                Total = db.Client.Count()
            };
            return View(model);
        }

        public List<Client> GetClientList()
        {
            var clientlist = db.Client.ToList();
            return clientlist;
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, ClientListModel model)
        {
            //var caseStudyList = GetCaseStudyList();
            var gridmodel = new GridModel<Client>();
            gridmodel.Data = db.Client.OrderBy(n => n.Id).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.Client.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public ActionResult Create()
        {
            Client model = new Client();
            model = GetClientModelRequiredInfo(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Client model)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["LogoUpload"].ContentLength > 0 && Request.Files["LogoUpload"] != null)
                {
                    model.Logo = saveFile(Request.Files["LogoUpload"]);
                }
                db.Client.Add(model);
                db.SaveChanges();
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new Client", Type = "Client", ReferenceID = model.Id, ReferenceKey = model.Name };
                db.ActivityLogs.Add(log);
                db.SaveChanges();

                return RedirectToAction("list", "client");
            }
            model = GetClientModelRequiredInfo(model);
            return View(model);
        }

        public ActionResult Edit(int Id)
        {
            Client model = db.Client.Find(Id);

            model = GetClientModelRequiredInfo(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Client model)
        {
            if (ModelState.IsValid)
            {
                string oldLogoUpload = Request["oldLogoUpload"];
                if (Request.Files["LogoUpload"].ContentLength > 0 && Request.Files["LogoUpload"] != null)
                {
                    model.Logo = updateFile(oldLogoUpload, Request.Files["LogoUpload"]);
                }
                else
                {
                    model.Logo = oldLogoUpload;
                }

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a Client", Type = "Client", ReferenceID = model.Id, ReferenceKey = model.Name };
                db.ActivityLogs.Add(log);
                db.SaveChanges();

                return RedirectToAction("list", "client");
            }
            model = GetClientModelRequiredInfo(model);
            return View(model);
        }

        public Client GetClientModelRequiredInfo(Client model)
        {

            return model;
        }

        public ActionResult DeleteLogo(int id, string filename)
        {
            deleteFile(filename);
            Client client = db.Client.Find(id);
            client.Logo = null;
            db.Entry(client).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Logo", Type = "Client", ReferenceID = client.Id, ReferenceKey = client.Name, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }
        public ActionResult Delete(int id)
        {
            Client client = db.Client.Find(id);
            db.Client.Remove(client);
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Client", Type = "Client", ReferenceID = client.Id, ReferenceKey = client.Name };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("list");
        }
    }
}
