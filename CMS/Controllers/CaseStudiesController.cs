﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Data;
using Telerik.Web.Mvc;
using System.Configuration;
using AstonRoseCMS.Helpers;

namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class CaseStudiesController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var testimonialsList = GetTestimonialsList();
            var model = new TestimonialsListModel();
            model.TestimonialsList = new GridModel<Testimonials>
            {
                Data = db.Testimonials.OrderBy(n => n.Id).Skip(0).Take(GridPageSize).ToList(),
                Total = db.Testimonials.Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, TestimonialsListModel model)
        {
            //var testimonialsList = GetTestimonialsList();
            var gridmodel = new GridModel<Testimonials>();
            gridmodel.Data = db.Testimonials.OrderBy(n => n.Id).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.Testimonials.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public List<Testimonials> GetTestimonialsList()
        {
            var testimonialsList = db.Testimonials.OrderBy(n => n.Id).ToList();
            return testimonialsList;
        }

        public ActionResult Create()
        {
            Testimonials testimonials = new Testimonials();
            return View(testimonials);
        }

        [HttpPost]
        public ActionResult Create(Testimonials testimonials)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["PDF"].ContentLength > 0 && Request.Files["PDF"] != null)
                {
                    testimonials.PDF = saveFile(Request.Files["PDF"]);
                }
                db.Testimonials.Add(testimonials);


                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new Testimonial", Type = "Testimonial", ReferenceID = testimonials.Id, ReferenceKey = testimonials.Name };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(testimonials);
        }

        public ActionResult Edit(int id)
        {
            Testimonials testimonials = db.Testimonials.Find(id);
            return View(testimonials);
        }

        [HttpPost]
        public ActionResult Edit(Testimonials testimonials)
        {
            if (ModelState.IsValid)
            {
                string oldLogoUpload = Request["oldPDF"];
                if (Request.Files["PDF"].ContentLength > 0 && Request.Files["PDF"] != null)
                {
                    testimonials.PDF = updateFile(oldLogoUpload, Request.Files["PDF"]);
                }
                else
                {
                    testimonials.PDF = oldLogoUpload;
                }
                db.Entry(testimonials).State = EntityState.Modified;
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a Testimonial", Type = "Testimonial", ReferenceID = testimonials.Id, ReferenceKey = testimonials.Name };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(testimonials);
        }

        public ActionResult DeletePDF(int id, string filename)
        {
            deleteFile(filename);
            Testimonials model = db.Testimonials.Find(id);
            model.PDF = null;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a PDF", Type = "Testimonials", ReferenceID = model.Id, ReferenceKey = model.Name, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }
        public ActionResult Delete(int id)
        {
            Testimonials testimonials = db.Testimonials.Find(id);
            db.Testimonials.Remove(testimonials);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Testimonial", Type = "Testimonial", ReferenceID = testimonials.Id, ReferenceKey = testimonials.Name };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
