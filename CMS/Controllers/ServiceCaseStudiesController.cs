﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Configuration;
using Telerik.Web.Mvc;
using AstonRoseCMS.Helpers;


namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class ServiceCaseStudiesController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var caseStudyList = GetCaseStudyList();
            var model = new CaseStudyListModel();
            model.CaseStudyList = new GridModel<CaseStudy>
            {
                Data = db.CaseStudies.OrderBy(n => n.Id).Skip(0).Take(GridPageSize).ToList(),
                Total = db.CaseStudies.Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, CaseStudyListModel model)
        {
            //var caseStudyList = GetCaseStudyList();
            var gridmodel = new GridModel<CaseStudy>();
            gridmodel.Data = db.CaseStudies.OrderBy(n => n.Id).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.CaseStudies.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public List<CaseStudy> GetCaseStudyList()
        {
            var caseStudies = db.CaseStudies.OrderBy(n => n.Id).ToList();
            return caseStudies;
        }


        public ActionResult Create()
        {
            CaseStudy caseStudy = new CaseStudy();
            caseStudy = GetCaseStudyModelRequiredInfo(caseStudy);
            return View(caseStudy);
        }

        private CaseStudy GetCaseStudyModelRequiredInfo(CaseStudy model)
        {


            var services = (from s in db.Services
                            select new { Text = s.Title, Value = s.Id }).ToList();

            var clientTypes = (from s in db.ClientTypes
                               select new { Text = s.Title, Value = s.Id }).ToList();

            foreach (var t in services)
            {

                ServiceSelectListItem item = new ServiceSelectListItem() { Text = t.Text, Value = t.Value.ToString() };
                model.AvailableServices.Add(item);


                var serCaseStudy = (from ts in db.ServiceCaseStudies
                                    where ts.CaseStudyId == model.Id && ts.ServiceId == t.Value
                                    select ts).FirstOrDefault();
                if (serCaseStudy != null)
                {
                    item.Live = serCaseStudy.Live;
                    item.Selected = true;
                }

            }

            foreach (var t in clientTypes)
            {
                var item = new ServiceSelectListItem() { Text = t.Text, Value = t.Value.ToString() };
                model.AvailableClientTypes.Add(item);
                var cliCaseStudy = (from ts in db.ClientTypeCaseStudies
                                    where ts.CaseStudyId == model.Id && ts.ClientTypeId == t.Value
                                    select ts).FirstOrDefault();
                if (cliCaseStudy != null)
                {
                    item.Live = cliCaseStudy.Live;
                    item.Selected = true;
                }
            }
            return model;
        }

        [HttpPost]
        public ActionResult Create(CaseStudy caseStudy, int[] ServiceIDs, int[] ClientTypeIDs)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    caseStudy.PDF = saveFile(Request.Files["PDFUpload"]);
                }

                db.CaseStudies.Add(caseStudy);
                caseStudy.DateCreated = DateTime.Now;
                caseStudy.DateModified = DateTime.Now;
                caseStudy.CreatedBy = User.Identity.Name;
                caseStudy.ModifiedBy = User.Identity.Name;
                db.SaveChanges();


                if (ServiceIDs != null && ServiceIDs.Length > 0)
                {
                    foreach (int cId in ServiceIDs)
                    {
                        ServiceCaseStudy ts = new ServiceCaseStudy() { ServiceId = cId, CaseStudyId = caseStudy.Id };
                        if (Request.Form["slive" + cId.ToString()] != null)
                            ts.Live = true;
                        db.ServiceCaseStudies.Add(ts);
                    }
                }
                if (ClientTypeIDs != null && ClientTypeIDs.Length > 0)
                {
                    foreach (int cId in ClientTypeIDs)
                    {
                        ClientTypeCaseStudy ts = new ClientTypeCaseStudy() { ClientTypeId = cId, CaseStudyId = caseStudy.Id };
                        if (Request.Form["clive" + cId.ToString()] != null)
                            ts.Live = true;
                        db.ClientTypeCaseStudies.Add(ts);
                    }
                }
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new case study", Type = "CaseStudy", ReferenceID = caseStudy.Id, ReferenceKey = caseStudy.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            caseStudy = GetCaseStudyModelRequiredInfo(caseStudy);
            return View(caseStudy);
        }

        public ActionResult Edit(int id)
        {
            CaseStudy caseStudy = db.CaseStudies.Find(id);
            caseStudy = GetCaseStudyModelRequiredInfo(caseStudy);
            return View(caseStudy);
        }

        [HttpPost]
        public ActionResult Edit(CaseStudy caseStudy, int[] ServiceIDs,  int[] ClientTypeIDs)
        {
            if (ModelState.IsValid)
            {

                var dbCaseStudy = db.CaseStudies.Find(caseStudy.Id);
                UpdateModel(dbCaseStudy);
                string oldPDFUpload = Request["oldPDFUpload"];
                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    dbCaseStudy.PDF = updateFile(oldPDFUpload, Request.Files["PDFUpload"]);
                }
                else
                {
                    dbCaseStudy.PDF = oldPDFUpload;
                }
                dbCaseStudy.DateModified = DateTime.Now;
                dbCaseStudy.ModifiedBy = User.Identity.Name;
                db.Entry(dbCaseStudy).State = EntityState.Modified;


                var existingServices = (from c in db.ServiceCaseStudies
                                        where c.CaseStudyId == caseStudy.Id
                                        select c);
                foreach (var c in existingServices)
                    db.ServiceCaseStudies.Remove(c);
                if (ServiceIDs != null && ServiceIDs.Length > 0)
                {
                    foreach (int cId in ServiceIDs)
                    {
                        ServiceCaseStudy ts = new ServiceCaseStudy() { ServiceId = cId, CaseStudyId = caseStudy.Id };
                        if (Request.Form["slive" + cId.ToString()] != null)
                            ts.Live = true;
                        db.ServiceCaseStudies.Add(ts);
                    }
                }


                var existingClientTypes = (from c in db.ClientTypeCaseStudies
                                        where c.CaseStudyId == caseStudy.Id
                                        select c);
                foreach (var c in existingClientTypes)
                    db.ClientTypeCaseStudies.Remove(c);
                if (ClientTypeIDs != null && ClientTypeIDs.Length > 0)
                {
                    foreach (int cId in ClientTypeIDs)
                    {
                        ClientTypeCaseStudy ts = new ClientTypeCaseStudy() { ClientTypeId = cId, CaseStudyId = caseStudy.Id };
                        if (Request.Form["clive" + cId.ToString()] != null)
                            ts.Live = true;
                        db.ClientTypeCaseStudies.Add(ts);
                    }
                }
                db.SaveChanges();
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a case study", Type = "CaseStudy", ReferenceID = dbCaseStudy.Id, ReferenceKey = dbCaseStudy.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(caseStudy);
        }

        public ActionResult Delete(int id)
        {
            CaseStudy caseStudy = db.CaseStudies.Find(id);
            var existingServices = (from c in db.ServiceCaseStudies
                                    where c.CaseStudyId == caseStudy.Id
                                    select c);
            foreach (var c in existingServices)
                db.ServiceCaseStudies.Remove(c);

            var existingClientTypes = (from c in db.ClientTypeCaseStudies
                                       where c.CaseStudyId == caseStudy.Id
                                       select c);
            foreach (var c in existingClientTypes)
                db.ClientTypeCaseStudies.Remove(c);


            db.CaseStudies.Remove(caseStudy);
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a case study", Type = "CaseStudy", ReferenceID = caseStudy.Id, ReferenceKey = caseStudy.Title };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult DeletePDF(int id, string filename)
        {
            deleteFile(filename);
            CaseStudy CaseStudy = db.CaseStudies.Find(id);
            CaseStudy.PDF = null;
            db.Entry(CaseStudy).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a PDF", Type = "Service", ReferenceID = CaseStudy.Id, ReferenceKey = CaseStudy.Title, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
