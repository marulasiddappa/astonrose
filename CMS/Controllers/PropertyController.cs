﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Configuration;
using Telerik.Web.Mvc;
using AstonRoseCMS.Helpers;



namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.AllRoles)]
    public class PropertyController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var propertyList = GetPropertyList();
            var model = new PropertyListModel();
            model.PropertyList = new GridModel<Property>
            {
                Data = db.Property.OrderByDescending(p => p.Id).Skip(0).Take(GridPageSize).ToList(),
                Total = db.Property.Count()
            };
            return View(model);
        }
        [HttpGet()]
        public ActionResult List(string Proptype, string Category, string Town)
        {
            var model = new PropertyListModel();
            model.PropertyList = new GridModel<Property>
            {
                Data = new List<Property>(),
                Total = 0

            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, PropertyListModel model, string Proptype, string Category, string Town)
        {
            // 1. Using strings:
            IEntitySorter<Property> sorter = null;
            if (command.SortDescriptors != null && command.SortDescriptors.Count > 0)
            {
                if (command.SortDescriptors[0].SortDirection == System.ComponentModel.ListSortDirection.Ascending)

                    sorter = EntitySorter<Property>
                        .OrderBy(command.SortDescriptors[0].Member);
                else
                    sorter = EntitySorter<Property>
                    .OrderByDescending(command.SortDescriptors[0].Member);
            }


            //var propertyList = GetPropertyList();
            var gridmodel = new GridModel<Property>();
            if (sorter == null)
            {
                gridmodel.Data = (from p in db.Property
                                  where (string.IsNullOrEmpty(Proptype) || p.Type == Proptype)
                                  && (string.IsNullOrEmpty(Category) || p.Category == Category)
                                  && (string.IsNullOrEmpty(Town) || p.Town == Town)
                                  orderby p.Id descending
                                  select p).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            }
            else
            {
                gridmodel.Data = sorter.Sort(from p in db.Property
                                             where (string.IsNullOrEmpty(Proptype) || p.Type == Proptype)
                                             && (string.IsNullOrEmpty(Category) || p.Category == Category)
                                             && (string.IsNullOrEmpty(Town) || p.Town == Town)
                                             orderby p.Id
                                             select p).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            }
            gridmodel.Total = (from p in db.Property
                               where (string.IsNullOrEmpty(Proptype) || p.Type == Proptype)
                               && (string.IsNullOrEmpty(Category) || p.Category == Category)
                               && (string.IsNullOrEmpty(Town) || p.Town == Town)
                               select p).Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public List<Property> GetPropertyList()
        {
            var propertyList = db.Property.OrderByDescending(p => p.Id).ToList();
            return propertyList;
        }

        public ActionResult Create()
        {
            Property model = new Property();
            model = GetPropertyModelRequiredInfo(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Property model, int[] selectedContact, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                //if (Request.Files["ImageUpload"].ContentLength > 0 && Request.Files["ImageUpload"] != null)
                //{
                //    model.Image = saveFile(Request.Files["ImageUpload"]);
                //}

                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    model.PDF = saveFile(Request.Files["PDFUpload"]);
                }

                if (Request.Files["EPCUpload"].ContentLength > 0 && Request.Files["EPCUpload"] != null)
                {
                    model.EPC = saveFile(Request.Files["EPCUpload"]);
                }


                GeoLoc gl = GeoCoder.LocateGooglePostcode(model.ToString());
                if (gl == null && !string.IsNullOrEmpty(model.Postcode))
                {
                    gl = GeoCoder.LocateGooglePostcode(model.Postcode) ?? new GeoLoc();
                }
                if (gl != null)
                {
                    model.Longitude = gl.Lon;
                    model.Latitude = gl.Lat;
                    model.GoogleMapURL = gl.MapURL;
                }
                

                if (selectedContact != null)
                {
                    model.ContactIDs = string.Join(",", selectedContact);
                }

                model.DateCreated = DateTime.Now;
                db.Property.Add(model);
                db.SaveChanges();


                if (attachments != null)
                {
                    foreach (HttpPostedFileBase file in attachments)
                    {
                        UploadedImage image = new UploadedImage();
                        image.ImageName = saveFile(file);
                        image.ReferenceId = model.Id;
                        image.Type = "Property";
                        db.UploadedImages.Add(image);
                    }
                }
                db.SaveChanges();
                UploadedImage firstImage = db.UploadedImages.FirstOrDefault(i => i.ReferenceId == model.Id && i.Type == "Property");
                if (firstImage != null)
                    model.Image = firstImage.ImageName;

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new Property", Type = "Property", ReferenceID = model.Id, ReferenceKey = model.BuildingName };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("list", "Property");
            }
            model = GetPropertyModelRequiredInfo(model);
            return View(model);
        }

        public ActionResult Edit(int Id)
        {
            Property model = db.Property.Find(Id);
            model.UploadedImages = (from i in db.UploadedImages
                                    where i.ReferenceId == Id && i.Type == "Property"
                                    select i).ToList();
            model = GetPropertyModelRequiredInfo(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Property model, int[] selectedContact, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                if (attachments != null)
                {
                    foreach (HttpPostedFileBase file in attachments)
                    {
                        UploadedImage image = new UploadedImage();
                        image.ImageName = saveFile(file);
                        image.ReferenceId = model.Id;
                        image.Type = "Property";
                        db.UploadedImages.Add(image);
                    }
                }

                string oldPDFUpload = Request["oldPDFUpload"];
                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    model.PDF = updateFile(oldPDFUpload, Request.Files["PDFUpload"]);
                }
                else
                {
                    model.PDF = oldPDFUpload;
                }

                string oldEPCUpload = Request["oldEPCUpload"];
                if (Request.Files["EPCUpload"].ContentLength > 0 && Request.Files["EPCUpload"] != null)
                {
                    model.EPC = updateFile(oldEPCUpload, Request.Files["EPCUpload"]);
                }
                else
                {
                    model.EPC = oldEPCUpload;
                }
                GeoLoc gl = GeoCoder.LocateGooglePostcode(model.ToString());
                if (gl == null && !string.IsNullOrEmpty(model.Postcode))
                {
                    gl = GeoCoder.LocateGooglePostcode(model.Postcode) ?? new GeoLoc();
                }
                if (gl != null)
                {
                    model.Longitude = gl.Lon;
                    model.Latitude = gl.Lat;
                    model.GoogleMapURL = gl.MapURL;
                }
                if (selectedContact != null)
                {
                    model.ContactIDs = string.Join(",", selectedContact);
                }

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                UploadedImage firstImage = db.UploadedImages.FirstOrDefault(i => i.ReferenceId == model.Id && i.Type == "Property");
                if (firstImage != null)
                    model.Image = firstImage.ImageName;
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edit a Property", Type = "Property", ReferenceID = model.Id, ReferenceKey = model.BuildingName };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("list", "property");
            }
            model = GetPropertyModelRequiredInfo(model);
            return View(model);
        }


        public ActionResult Delete(int id)
        {
            Property property = db.Property.Find(id);
            List<UploadedImage> uploadedImages = (from i in db.UploadedImages
                                                  where i.ReferenceId == id && i.Type == "Property"
                                                  select i).ToList();
            foreach (UploadedImage image in uploadedImages)
            {
                deleteFile(image.ImageName);
                db.UploadedImages.Remove(image);
            }
            db.Property.Remove(property);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Property", Type = "Property", ReferenceID = property.Id, ReferenceKey = property.BuildingName };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public Property GetPropertyModelRequiredInfo(Property model)
        {
            //model.AvailableCategories.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in AstonRoseCMSHelper.GetPropertyCategoryDropdowns().ToList())
            {
                model.AvailableCategories.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == model.Type) });
            }

            model.AvailableType.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in AstonRoseCMSHelper.GetPropertyTypeDropdowns().ToList())
            {
                model.AvailableType.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == model.Type) });
            }

            model.AvailableSize.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in AstonRoseCMSHelper.GetPropertySizeDropdowns().ToList())
            {
                model.AvailableSize.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == model.Size) });
            }

            //model.AvailableTenure.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in AstonRoseCMSHelper.GetPropertyTenureDropdowns().ToList())
            {
                model.AvailableTenure.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == model.Tenure) });
            }

            model.AvailableStatus.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in AstonRoseCMSHelper.GetPropertyStatusDropdowns().ToList())
            {
                model.AvailableStatus.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == model.Status) });
            }

            //model.AvailableRegion.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in AstonRoseCMSHelper.GetPropertyRegionDropdowns().ToList())
            {
                model.AvailableRegion.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == model.Region) });
            }

            model.PropertyContactList = GetPropertyContactList();

            if (model.ContactIDs != null && model.ContactIDs != string.Empty)
            {
                model.PropertyContactSelectedIdList = model.ContactIDs.Split(',').ToList();
            }
            else
            {
                model.PropertyContactSelectedIdList = new List<string>();
            }

            return model;
        }

        public ActionResult DeleteImage(int imageId, int propertyId, string filename)
        {
            deleteFile(filename);
            UploadedImage image = db.UploadedImages.Find(imageId);
            db.UploadedImages.Remove(image);
            db.SaveChanges();
            UploadedImage firstImage = db.UploadedImages.FirstOrDefault(i => i.ReferenceId == propertyId && i.Type == "Property");
            Property property = db.Property.Find(propertyId);
            if (firstImage != null)
                property.Image = firstImage.ImageName;

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Property Image", Type = "Property", ReferenceID = image.Id, ReferenceKey = image.ImageName, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = propertyId });
        }

        public ActionResult DeletePDF(int id, string filename)
        {
            deleteFile(filename);
            Property property = db.Property.Find(id);
            property.PDF = null;
            db.Entry(property).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a PDF", Type = "Property", ReferenceID = property.Id, ReferenceKey = property.BuildingName, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult DeleteEPC(int id, string filename)
        {
            deleteFile(filename);
            Property property = db.Property.Find(id);
            property.EPC = null;
            db.Entry(property).State = EntityState.Modified;
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a ECP", Type = "Property", ReferenceID = property.Id, ReferenceKey = property.BuildingName, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        public List<Team> GetPropertyContactList()
        {
            List<Team> propertyContactList = new List<Team>();
            propertyContactList = db.Team.Where(t => t.PropertyContactFlag == true).ToList();
            return propertyContactList;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}