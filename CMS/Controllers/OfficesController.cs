﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Configuration;
using Telerik.Web.Mvc;
using AstonRoseCMS.Helpers;


namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class OfficesController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var officesList = GetOfficesList();
            var model = new OfficesListModel();
            model.OfficesList = new GridModel<Offices>
            {
                Data = db.Offices.OrderBy(o => o.DisplayOrder).Skip(0).Take(GridPageSize).ToList(),
                Total = db.Offices.Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, OfficesListModel model)
        {
            //var officesList = GetOfficesList();
            var gridmodel = new GridModel<Offices>();
            gridmodel.Data = db.Offices.OrderBy(o => o.DisplayOrder).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.Offices.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public List<Offices> GetOfficesList()
        {
            var officesList = db.Offices.ToList();
            return officesList;
        }

        public ActionResult Create()
        {
            Offices model = new Offices();
            model = GetOfficesModelRequiredInfo(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Offices offices)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["BannerImageUpload"].ContentLength > 0 && Request.Files["BannerImageUpload"] != null)
                {
                    offices.BannerImage = saveFile(Request.Files["BannerImageUpload"]);
                }

                if (Request.Files["ThumbImageUpload"].ContentLength > 0 && Request.Files["ThumbImageUpload"] != null)
                {
                    offices.ThumbImage = saveFile(Request.Files["ThumbImageUpload"]);
                }
                //if (offices.PostCode != "")
                //{
                //    GeoLoc gl = GeoCoder.LocateGooglePostcode(offices.PostCode) ?? new GeoLoc();
                //    offices.Longitude = gl.Lon;
                //    offices.Latitude = gl.Lat;
                //    offices.GoogleMapURL = gl.MapURL;
                //}
                db.Offices.Add(offices);
                db.SaveChanges();
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new Office", Type = "Office", ReferenceID = offices.Id, ReferenceKey = offices.OfficeName };
                db.ActivityLogs.Add(log);
                db.SaveChanges();

                return RedirectToAction("list", "offices");
            }
            offices = GetOfficesModelRequiredInfo(offices);
            return View(offices);
        }

        public ActionResult Edit(int id)
        {
            Offices offices = db.Offices.Find(id);
            offices = GetOfficesModelRequiredInfo(offices);
            return View(offices);
        }

        [HttpPost]
        public ActionResult Edit(Offices offices)
        {
            if (ModelState.IsValid)
            {
                string oldBannerImageUpload = Request["oldBannerImageUpload"];
                if (Request.Files["BannerImageUpload"].ContentLength > 0 && Request.Files["BannerImageUpload"] != null)
                {
                    offices.BannerImage = updateFile(oldBannerImageUpload, Request.Files["BannerImageUpload"]);
                }
                else
                {
                    offices.BannerImage = oldBannerImageUpload;
                }

                string oldThumbImageUpload = Request["oldThumbImageUpload"];
                if (Request.Files["ThumbImageUpload"].ContentLength > 0 && Request.Files["ThumbImageUpload"] != null)
                {
                    offices.ThumbImage = updateFile(oldThumbImageUpload, Request.Files["ThumbImageUpload"]);
                }
                else
                {
                    offices.ThumbImage = oldThumbImageUpload;
                }

                //if (offices.PostCode != "")
                //{
                //    GeoLoc gl = GeoCoder.LocateGooglePostcode(offices.PostCode) ?? new GeoLoc();
                //    offices.Longitude = gl.Lon;
                //    offices.Latitude = gl.Lat;
                //    offices.GoogleMapURL = gl.MapURL;
                //}

                db.Entry(offices).State = EntityState.Modified;
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a Office", Type = "Office", ReferenceID = offices.Id, ReferenceKey = offices.OfficeName };
                db.ActivityLogs.Add(log);
                db.SaveChanges();

                return RedirectToAction("list", "offices");
            }
            offices = GetOfficesModelRequiredInfo(offices);
            return View(offices);
        }

        public Offices GetOfficesModelRequiredInfo(Offices model)
        {
            model.AvailableOfficeContact1.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in OfficesContactList())
            {
                model.AvailableOfficeContact1.Add(new SelectListItem() { Text = t.FirstName + " " + t.LastName, Value = t.Id.ToString(), Selected = (t.Id == model.ContactId1) });
            }

            model.AvailableOfficeContact2.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            foreach (var t in OfficesContactList())
            {
                model.AvailableOfficeContact2.Add(new SelectListItem() { Text = t.FirstName + " " + t.LastName, Value = t.Id.ToString(), Selected = (t.Id == model.ContactId2) });
            }

            return model;
        }

        public List<Team> OfficesContactList()
        {
            List<Team> officesContactList = new List<Team>();
            officesContactList = db.Team.Where(t => t.OfficeContactFlag == true).ToList();
            return officesContactList;
        }

        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Offices offices = db.Offices.Find(id);
            db.Offices.Remove(offices);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Office", Type = "Office", ReferenceID = offices.Id };
            db.ActivityLogs.Add(log);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult DeleteBannerImage(int id, string filename)
        {
            deleteFile(filename);
            Offices office = db.Offices.Find(id);
            office.BannerImage = null;
            db.Entry(office).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Office Banner Image", Type = "Office", ReferenceID = office.Id, ReferenceKey = office.OfficeName, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult DeleteThumbImage(int id, string filename)
        {
            deleteFile(filename);
            Offices office = db.Offices.Find(id);
            office.ThumbImage = null;
            db.Entry(office).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Office Thumb Nail", Type = "Office", ReferenceID = office.Id, ReferenceKey = office.OfficeName, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}