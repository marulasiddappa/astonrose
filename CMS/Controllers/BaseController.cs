﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace AstonRoseCMS.Controllers
{
    public class BaseController : Controller
    {
        public string uploadPath = ConfigurationManager.AppSettings["FileUploadPath"];
        public string fileUploadURL = ConfigurationManager.AppSettings["FileUploadURL"];

        #region "files handling"
        protected string saveFile(HttpPostedFileBase uploadedFile)
        {
            if (uploadedFile == null)
                return null;
            string fn = Path.GetFileNameWithoutExtension(uploadedFile.FileName);
            string ext = Path.GetExtension(uploadedFile.FileName);

            Regex rgx = new Regex("[^a-zA-Z0-9]");
            string fileName = rgx.Replace(fn, "-");
            while (fileName.IndexOf("--") > -1)
            {
                fileName = fileName.Replace("--", "-");
            }
            string filePath = Path.Combine(uploadPath, fileName + ext);

            int i = 1;
            while (System.IO.File.Exists(filePath))
            {
                filePath = Path.Combine(uploadPath, fileName + i.ToString() + ext);
                i++;
            }
            uploadedFile.SaveAs(filePath);

            return Path.GetFileName(filePath);
        }

        protected string SaveUploadFile(HttpPostedFileBase uploadedFile)
        {
            if (uploadedFile == null)
                return null;
            string fn = Path.GetFileName(uploadedFile.FileName);
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            string fileName = rgx.Replace(fn, "-");
            while (fileName.IndexOf("--") > -1)
            {
                fileName = fileName.Replace("--", "-");
            }
            string filePath = Path.Combine(uploadPath, fileName);

            int i = 1;
            while (System.IO.File.Exists(filePath))
            {
                filePath = Path.Combine(uploadPath, System.IO.Path.GetFileNameWithoutExtension(fileName) + i.ToString() + System.IO.Path.GetExtension(fileName));
                i++;
            }
            uploadedFile.SaveAs(filePath);
            return filePath;
        }

        protected string updateFile(string oldFileName, HttpPostedFileBase postedFile)
        {
            string fileName = oldFileName;
            if (postedFile != null)
            {
                if (!string.IsNullOrEmpty(oldFileName))
                {
                    deleteFile(oldFileName);
                }
                fileName = saveFile(postedFile);
            }
            return fileName;
        }
        protected void deleteFile(string fileName)
        {
            string filePath = string.Empty;
            if (fileName != null)
                filePath = Path.Combine(uploadPath, fileName);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
        }
        #endregion
    }
}
