﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using Telerik.Web.Mvc;
using System.Configuration;
using System.Data;
using AstonRoseCMS.Helpers;
using System.Data.Entity.Validation;

namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class TeamController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult ToggleShow(bool isChecked = false, int itemId = 0)
        {
            
                Team team = db.Team.Find(itemId);
                //team.ShowInContacts = Convert.ToBoolean(isChecked);
                team.ShowInContacts = isChecked;
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Selected contact to " + (isChecked ? "Show" : "Hide") + " to show in Contacts", Type = "Team", ReferenceID = team.Id, ReferenceKey = team.FirstName + " " + team.LastName };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
            
            return RedirectToAction("Index");
        }

        public ActionResult ChangeDisplayOrder(string displayOrder = "1", int itemId = 0)
        {
            Team team = db.Team.Find(itemId);
            int selectedDisplayOrder = int.Parse(displayOrder);

            //team.ShowInContacts = Convert.ToBoolean(isChecked);
            team.DisplayOrder = selectedDisplayOrder;
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Changed the display order to " + displayOrder, Type = "Team", ReferenceID = team.Id, ReferenceKey = team.FirstName + " " + team.LastName };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult List()
        {
            //var teamList = GetTeamList();
            var model = new TeamListModel();
            model.TeamList = new GridModel<Team>
            {
                Data = db.Team.OrderBy(n => n.LastName).Skip(0).Take(GridPageSize).ToList(),
                Total = db.Team.Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, TeamListModel model)
        {
            IEntitySorter<Team> sorter = null;
            if (command.SortDescriptors != null && command.SortDescriptors.Count > 0)
            {
                if (command.SortDescriptors[0].SortDirection == System.ComponentModel.ListSortDirection.Ascending)
                {
                    sorter = EntitySorter<Team>
                        .OrderBy(command.SortDescriptors[0].Member)
                        .ThenBy("LastName");

                }
                else
                {
                    sorter = EntitySorter<Team>
                    .OrderByDescending(command.SortDescriptors[0].Member)
                    .ThenBy("LastName");
                }
            }

            //var teamList = GetTeamList();
            var gridmodel = new GridModel<Team>();
            if (sorter == null)
            {
                gridmodel.Data = db.Team.OrderBy(n => n.LastName).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            }
            else
            {
                gridmodel.Data = sorter.Sort(db.Team).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            }
            gridmodel.Total = db.Team.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public List<Team> GetTeamList()
        {
            var partnerslist = db.Team.OrderBy(n => n.Id).ToList();
            return partnerslist;
        }


        public ActionResult Create()
        {
            Team team = new Team();
            team = GetTeamModelRequiredInfo(team);
            return View(team);
        }

        private Team GetTeamModelRequiredInfo(Team model)
        {
            foreach (var t in AstonRoseCMSHelper.GetTeamTypeDropdowns().ToList())
            {
                model.TeamTypes.Add(new SelectListItem() { Text = t.Text, Value = t.Value, Selected = (t.Value == model.TeamType) });
            }
            return model;
        }

        [HttpPost]
        public ActionResult Create(Team team)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["FileUpload"].ContentLength > 0 && Request.Files["FileUpload"] != null)
                {
                    team.Image = saveFile(Request.Files["FileUpload"]);
                }

                db.Team.Add(team);
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new Team Member", Type = "Team", ReferenceID = team.Id, ReferenceKey = team.FirstName + " " + team.LastName };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            team = GetTeamModelRequiredInfo(team);
            return View(team);
        }

        public ActionResult Edit(int id)
        {
            Team team = db.Team.Find(id);
            team = GetTeamModelRequiredInfo(team);
            return View(team);
        }

        [HttpPost]
        public ActionResult Edit(Team team)
        {
            if (ModelState.IsValid)
            {
                //var dbteam = db.Team.Find(team.Id);
                ////UpdateModel(dbCaseStudy);
                //team.ShowInContacts = dbteam.ShowInContacts;
                //team.DisplayOrder = dbteam.DisplayOrder;

                string oldFileUpload = Request["oldFileUpload"];
                if (Request.Files["FileUpload"].ContentLength > 0 && Request.Files["FileUpload"] != null)
                {
                    team.Image = updateFile(oldFileUpload, Request.Files["FileUpload"]);
                }
                else
                {
                    team.Image = oldFileUpload;
                }

                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a Team Member", Type = "Team", ReferenceID = team.Id, ReferenceKey = team.FirstName + " " + team.LastName };
                db.ActivityLogs.Add(log);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(team);
        }

        public ActionResult Delete(int id)
        {
            Team team = db.Team.Find(id);
            deleteFile(team.Image);
            db.Team.Remove(team);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Team Member", Type = "Team", ReferenceID = team.Id, ReferenceKey = team.FirstName + " " + team.LastName };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, string filename)
        {
            deleteFile(filename);
            Team team = db.Team.Find(id);
            team.Image = null;
            db.Entry(team).State = EntityState.Modified;
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Team Member Image", Type = "Team", ReferenceID = team.Id, ReferenceKey = team.FirstName + " " + team.LastName, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
