﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Data;
using Telerik.Web.Mvc;
using System.Configuration;
using AstonRoseCMS.Helpers;

namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class CSRController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var csrList = GetCSRList();
            var model = new CSRListModel();
            model.CSRList = new GridModel<CSR>
            {
                Data = db.CSR.OrderByDescending(n => n.Date).Skip(0).Take(GridPageSize).ToList(),
                Total = db.CSR.Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, CSRListModel model)
        {
            //var csrList = GetCSRList();
            var gridmodel = new GridModel<CSR>();
            gridmodel.Data = db.CSR.OrderByDescending(n => n.Id).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.CSR.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public List<CSR> GetCSRList()
        {
            var csrList = db.CSR.OrderBy(n => n.Date).ToList();
            return csrList;
        }

        public ActionResult Create()
        {
            CSR csr = new CSR();
            return View(csr);
        }

        [HttpPost]
        public ActionResult Create(CSR csr)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["FileUpload"].ContentLength > 0 && Request.Files["FileUpload"] != null)
                {
                    csr.Image = saveFile(Request.Files["FileUpload"]);
                }

                db.CSR.Add(csr);
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new CSR", Type = "CSR", ReferenceID = csr.Id,  ReferenceKey=csr.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(csr);
        }

        public ActionResult Edit(int id)
        {
            CSR csr = db.CSR.Find(id);
            return View(csr);
        }

        [HttpPost]
        public ActionResult Edit(CSR csr)
        {
            if (ModelState.IsValid)
            {
                string oldFileUpload = Request["oldFileUpload"];
                if (Request.Files["FileUpload"].ContentLength > 0 && Request.Files["FileUpload"] != null)
                {
                    csr.Image = updateFile(oldFileUpload, Request.Files["FileUpload"]);
                }
                else
                {
                    csr.Image = oldFileUpload;
                }

                db.Entry(csr).State = EntityState.Modified;
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a CSR", Type = "CSR", ReferenceID = csr.Id, ReferenceKey = csr.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(csr);
        }


        public ActionResult Delete(int id)
        {
            CSR csr = db.CSR.Find(id);
            deleteFile(csr.Image);
            db.CSR.Remove(csr);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a CSR", Type = "CSR", ReferenceID = csr.Id, ReferenceKey = csr.Title };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, string filename)
        {
            deleteFile(filename);
            CSR csr = db.CSR.Find(id);
            csr.Image = null;
            db.Entry(csr).State = EntityState.Modified;
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a CSR Image", Type = "CSR", ReferenceID = csr.Id, ReferenceKey = csr.Title, ReferenceImageName=filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


    }
}
