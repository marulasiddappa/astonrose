﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Configuration;
using Telerik.Web.Mvc;
using AstonRoseCMS.Helpers;

namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class ServicesController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);
        //
        // GET: /Services/

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var serviceList = db.Services.OrderBy(n => n.Id).ToList();

            var model = new ServiceListModel();
            model.ServiceList = new GridModel<Service>
            {
                Data = db.Services.OrderBy(n => n.Id).Skip(0).Take(GridPageSize).ToList(),
                Total = db.Services.OrderBy(n => n.Id).Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, ServiceListModel model)
        {
            
            var gridmodel = new GridModel<Service>();
            gridmodel.Data = db.Services.OrderByDescending(p => p.Id).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.Services.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        //
        // GET: /Services/Create

        public ActionResult Create()
        {
            Service service = new Service();
            GetServiceModelRequiredInfo(service);
            return View(service);
        }

        //
        // POST: /Services/Create

        [HttpPost]
        public ActionResult Create(Service service, int[] selectedContact, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    service.PDFBrochure = saveFile(Request.Files["PDFUpload"]);
                }
                if (selectedContact != null)
                {
                    service.ContactIds = string.Join(",", selectedContact);
                }
                service.DateCreated = DateTime.Now;
                service.DateModified = DateTime.Now;
                service.CreatedBy = User.Identity.Name;
                service.ModifiedBy = User.Identity.Name;
                db.Services.Add(service);
                db.SaveChanges();

                if (attachments != null)
                {
                    foreach (HttpPostedFileBase file in attachments)
                    {
                        UploadedImage image = new UploadedImage();
                        image.ImageName = saveFile(file);
                        image.ReferenceId = service.Id;
                        image.Type = "Service";
                        db.UploadedImages.Add(image);
                    }
                }
                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new Service", Type = "Service", ReferenceID = service.Id, ReferenceKey = service.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            GetServiceModelRequiredInfo(service);
            return View(service);
        }

        //
        // GET: /Services/Edit/5

        public ActionResult Edit(int id)
        {
            Service service = db.Services.Find(id);
            service.UploadedImages = (from i in db.UploadedImages
                                         where i.ReferenceId == id && i.Type == "Service"
                                         select i).ToList();
            GetServiceModelRequiredInfo(service);
            return View(service);
        }

        public List<Team> OfficesContactList()
        {
            List<Team> officesContactList = new List<Team>();
            officesContactList = db.Team.Where(t => t.OfficeContactFlag == true).ToList();
            return officesContactList;
        }
        private Service GetServiceModelRequiredInfo(Service model)
        {
            
            var contactList = from c in db.Team
                              select new { Text = c.FirstName + " " + c.LastName, Value = c.Id };

            foreach (var t in contactList)
            {
                model.AvailableContacts1.Add(new SelectListItem() { Text = t.Text, Value = t.Value.ToString()});
            }
            if (model.ContactIds != null && model.ContactIds != string.Empty)
            {
                model.ContactSelectedIdList = model.ContactIds.Split(',').ToList();
            }
            else
            {
                model.ContactSelectedIdList = new List<string>();
            }


            //model.AvailableContacts2.Add(new SelectListItem() { Text = "[Select]", Value = "" });
            //foreach (var t in contactList)
            //{
            //    model.AvailableContacts2.Add(new SelectListItem() { Text = t.Text, Value = t.Value.ToString(), Selected = (t.Value == model.Contact1) });
            //}


            return model;
        }
        //
        // POST: /Services/Edit/5

        [HttpPost]
        public ActionResult Edit(Service service, int[] selectedContact, IEnumerable<HttpPostedFileBase> attachments)
        {
            if (ModelState.IsValid)
            {
                var dbService = db.Services.Find(service.Id);
                UpdateModel(dbService);
                if (attachments != null)
                {
                    foreach (HttpPostedFileBase file in attachments)
                    {
                        UploadedImage image = new UploadedImage();
                        image.ImageName = saveFile(file);
                        image.ReferenceId = dbService.Id;
                        image.Type = "Service";
                        db.UploadedImages.Add(image);
                    }
                }
                string oldPDFUpload = Request["oldPDFUpload"];
                if (Request.Files["PDFUpload"].ContentLength > 0 && Request.Files["PDFUpload"] != null)
                {
                    dbService.PDFBrochure = updateFile(oldPDFUpload, Request.Files["PDFUpload"]);
                }
                else
                {
                    dbService.PDFBrochure = oldPDFUpload;
                }
                if (selectedContact != null)
                {
                    dbService.ContactIds = string.Join(",", selectedContact);
                }
                dbService.DateModified = DateTime.Now;
                dbService.ModifiedBy = User.Identity.Name;
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a Service", Type = "Service", ReferenceID = service.Id, ReferenceKey = service.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            GetServiceModelRequiredInfo(service);
            return View(service);
        }

        //
        
        public ActionResult DeletePDF(int id, string filename)
        {
            deleteFile(filename);
            Service service = db.Services.Find(id);
            service.PDFBrochure = null;
            db.Entry(service).State = EntityState.Modified;
            db.SaveChanges();
            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a PDF", Type = "Service", ReferenceID = service.Id, ReferenceKey = service.Title, ReferenceImageName = filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        //
        // GET: /Services/Delete/5

        public ActionResult Delete(int id)
        {
            Service service = db.Services.Find(id);
            List<UploadedImage> uploadedImages = (from i in db.UploadedImages
                                         where i.ReferenceId == id && i.Type == "Service"
                                         select i).ToList();

            foreach (UploadedImage image in uploadedImages)
            {
                deleteFile(image.ImageName);
                db.UploadedImages.Remove(image);
            }
            db.Services.Remove(service);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Service", Type = "Service", ReferenceID = service.Id, ReferenceKey = service.Title };
            db.ActivityLogs.Add(log);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //
        // POST: /Services/Delete/5

        public ActionResult DeleteImage(int imageId,int serviceId, string filename)
        {
            deleteFile(filename);
               UploadedImage image = db.UploadedImages.Find(imageId);
            db.UploadedImages.Remove(image);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a Service Image", Type = "Service", ReferenceID = image.Id, ReferenceKey = image.ImageName, ReferenceImageName=filename};
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = serviceId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}