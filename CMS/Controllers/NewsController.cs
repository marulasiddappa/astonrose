﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AstonRoseCMS.Models;
using System.Data;
using Telerik.Web.Mvc;
using System.Configuration;
using AstonRoseCMS.Helpers;

namespace AstonRoseCMS.Controllers
{
    [Authorize(Roles = AppConstants.SuperUser)]
    public class NewsController : BaseController
    {
        private AstonRoseCMSContext db = new AstonRoseCMSContext();
        public int GridPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultGridPageSize"]);

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //var newsList = GetNewsList();
            var model = new NewsListModel();
            model.NewsList = new GridModel<News>
            {
                Data = db.News.OrderByDescending(n => n.NewsDate).Skip(0).Take(GridPageSize).ToList(),
                Total = db.News.Count()
            };
            return View(model);
        }

        [HttpPost, GridAction(EnableCustomBinding = true)]
        public ActionResult List(GridCommand command, NewsListModel model)
        {
            //var newsList = GetNewsList();
            var gridmodel = new GridModel<News>();
            gridmodel.Data = db.News.OrderByDescending(n => n.NewsDate).Skip((command.Page - 1) * command.PageSize).Take(command.PageSize).ToList();
            gridmodel.Total = db.News.Count();
            return new JsonResult
            {
                Data = gridmodel
            };
        }

        public List<News> GetNewsList()
        {
            var newsList = db.News.OrderBy(n => n.NewsDate).ToList();
            return newsList;
        }

        public ActionResult Create()
        {
            News news = new News();
            return View(news);
        }

        [HttpPost]
        public ActionResult Create(News news)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files["FileUpload"].ContentLength > 0 && Request.Files["FileUpload"] != null)
                {
                    news.Image = saveFile(Request.Files["FileUpload"]);
                }

                db.News.Add(news);
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Created a new News", Type = "News", ReferenceID = news.Id,  ReferenceKey=news.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(news);
        }

        public ActionResult Edit(int id)
        {
            News news = db.News.Find(id);
            return View(news);
        }

        [HttpPost]
        public ActionResult Edit(News news)
        {
            if (ModelState.IsValid)
            {
                string oldFileUpload = Request["oldFileUpload"];
                if (Request.Files["FileUpload"].ContentLength > 0 && Request.Files["FileUpload"] != null)
                {
                    news.Image = updateFile(oldFileUpload, Request.Files["FileUpload"]);
                }
                else
                {
                    news.Image = oldFileUpload;
                }

                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();

                ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Edited a News", Type = "News", ReferenceID = news.Id, ReferenceKey = news.Title };
                db.ActivityLogs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(news);
        }


        public ActionResult Delete(int id)
        {
            News news = db.News.Find(id);
            deleteFile(news.Image);
            db.News.Remove(news);
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a News", Type = "News", ReferenceID = news.Id, ReferenceKey = news.Title };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, string filename)
        {
            deleteFile(filename);
            News news = db.News.Find(id);
            news.Image = null;
            db.Entry(news).State = EntityState.Modified;
            db.SaveChanges();

            ActivityLog log = new ActivityLog { ActivityBy = User.Identity.Name, ActivityOn = DateTime.Now, Description = "Deleted a News Image", Type = "News", ReferenceID = news.Id, ReferenceKey = news.Title, ReferenceImageName=filename };
            db.ActivityLogs.Add(log);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id = id });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


    }
}
