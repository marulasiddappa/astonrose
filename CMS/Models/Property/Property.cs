﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseCMS.Models
{
    public class Property
    {
        public Property()
        {
            AvailableType = new List<SelectListItem>();
            AvailableSize = new List<SelectListItem>();
            AvailableTenure = new List<SelectListItem>();
            AvailableStatus = new List<SelectListItem>();
            AvailableRegion = new List<SelectListItem>();
            AvailableCategories = new List<SelectListItem>();
            UploadedImages = new List<UploadedImage>();
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Category is Required")]
        [Display(Name = "Category")]
        public string Category { get; set; }

        [Required(ErrorMessage = "Type is Required")]
        [Display(Name = "Type")]
        public string Type { get; set; }
        [NotMapped]
        public IList<SelectListItem> AvailableType { get; set; }

        [Required(ErrorMessage = "Size is Required")]
        [Display(Name = "Size")]
        public string Size { get; set; }

        [Display(Name = "Actual Floor Area (sq ft)")]
        public string SquareFeet { get; set; }


        [Display(Name = "Actual Floor Area (sq m)")]
        public string SquareMeters { get; set; }

        [NotMapped]
        public IList<SelectListItem> AvailableSize { get; set; }

        [NotMapped]
        public IList<SelectListItem> AvailableCategories { get; set; }

        [Required(ErrorMessage = "Tenure is Required")]
        [Display(Name = "Tenure")]
        public string Tenure { get; set; }
        [NotMapped]
        public IList<SelectListItem> AvailableTenure { get; set; }

        [Required(ErrorMessage = "Status is Required")]
        [Display(Name = "Status")]
        public string Status { get; set; }
        [NotMapped]
        public IList<SelectListItem> AvailableStatus { get; set; }

        [Required(ErrorMessage = "Region is Required")]
        [Display(Name = "Region")]
        public string Region { get; set; }
        [NotMapped]
        public IList<SelectListItem> AvailableRegion { get; set; }

        [Required(ErrorMessage = "Price is Required")]
        [Display(Name = "Price")]
        public string Price { get; set; }

        [Required(ErrorMessage = "PriceFlag is Required")]
        [Display(Name = "PriceFlag")]
        public string PriceFlag { get; set; }


        //[Required(ErrorMessage = "Image is Required")]
        [Display(Name = "Image")]
        public string Image { get; set; }

        //[Required(ErrorMessage = "Video is Required")]
        [Display(Name = "Video")]
        public string Video { get; set; }

        [Required(ErrorMessage = "BuildingName is Required")]
        [Display(Name = "Building Name")]
        public string BuildingName { get; set; }

        [Required(ErrorMessage = "Address1 is Required")]
        [Display(Name = "Address1")]
        public string Address1 { get; set; }

        //[Required(ErrorMessage = "Address2 is Required")]
        [Display(Name = "Address2")]
        public string Address2 { get; set; }

        //[Required(ErrorMessage = "Address3 is Required")]
        [Display(Name = "Address3")]
        public string Address3 { get; set; }

        [Required(ErrorMessage = "Town is Required")]
        [Display(Name = "Town")]
        public string Town { get; set; }

        //[Required(ErrorMessage = "County is Required")]
        [Display(Name = "County")]
        public string County { get; set; }

        [Required(ErrorMessage = "Postcode is Required")]
        [Display(Name = "Postcode")]
        public string Postcode { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        [Display(Name = "Description")]
        [AllowHtml]
        public string Description { get; set; }

        //[Required(ErrorMessage = "PDF is Required")]
        [Display(Name = "PDF")]
        public string PDF { get; set; }

        //[Required(ErrorMessage = "EPC is Required")]
        [Display(Name = "EPC")]
        public string EPC { get; set; }

        //[Required(ErrorMessage = "ContactIDs is Required")]
        [Display(Name = "Contacts")]
        public string ContactIDs { get; set; }




        //[Required(ErrorMessage = "Date Created is Required")]
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }

        [NotMapped]
        public List<Team> PropertyContactList { get; set; }

        [NotMapped]
        public List<string> PropertyContactSelectedIdList { get; set; }

        [NotMapped]
        public List<UploadedImage> UploadedImages { get; set; }

        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public string GoogleMapURL { get; set; }

        public override string ToString()
        {
            string _return = "";
            if (!string.IsNullOrEmpty(Address1))
                _return += Address1 + ",";
            if (!string.IsNullOrEmpty(Address2))
                _return += Address2 + ",";
            if (!string.IsNullOrEmpty(Address3))
                _return += Address3 + ",";
            if (!string.IsNullOrEmpty(Town))
                _return += Town + ",";
            if (!string.IsNullOrEmpty(County))
                _return += County + ",";
            if (!string.IsNullOrEmpty(County))
                _return += County + ",";
            if (!string.IsNullOrEmpty(Postcode))
                _return += Postcode + ",";
            if (_return.EndsWith(","))
                _return = _return.Remove(_return.LastIndexOf(','));
            return _return;
        }
    }
}