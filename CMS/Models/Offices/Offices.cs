﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseCMS.Models
{
    public class Offices
    {
        public Offices()
        {
            AvailableOfficeContact1 = new List<SelectListItem>();
            AvailableOfficeContact2 = new List<SelectListItem>();
        }
        public override string ToString()
        {
            string returnVal = "";
            returnVal = (string.IsNullOrEmpty(AddressLine1) ? "" : AddressLine1 + "<br/>");
            returnVal += (string.IsNullOrEmpty(AddressLine2) ? "" : AddressLine2 + "<br/>");
            returnVal += (string.IsNullOrEmpty(AddressLine3) ? "" : AddressLine3 + "<br/>");
            returnVal += (string.IsNullOrEmpty(City) ? "" : City + "<br/>");
            returnVal += (string.IsNullOrEmpty(County) ? "" : County + "<br/>");
            returnVal += (string.IsNullOrEmpty(PostCode) ? "" : PostCode + "<br/>");
            return returnVal;
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Office Name is Required")]
        [Display(Name = "Office Name")]
        public string OfficeName { get; set; }

        [Required(ErrorMessage = "Address Line1 is Required")]
        [Display(Name = "Address Line1")]
        public string AddressLine1 { get; set; }

        
        [Display(Name = "Address Line2")]
        public string AddressLine2 { get; set; }

        
        [Display(Name = "Address Line3")]
        public string AddressLine3 { get; set; }

        
        [Display(Name = "City")]
        public string City { get; set; }

        
        [Display(Name = "County")]
        public string County { get; set; }

        [Required(ErrorMessage = "PostCode is Required")]
        [Display(Name = "PostCode")]
        public string PostCode { get; set; }

        
        [Display(Name = "Telephone")]
        public string Telephone { get; set; }

        
        [Display(Name = "Fax")]
        public string Fax { get; set; }

        
        [Display(Name = "Email")]
        public string Email { get; set; }

        
        [Display(Name = "Welcome Text")]
        public string WelcomeTitle { get; set; }

        
        [Display(Name = "Description")]
        [AllowHtml]
        public string Description { get; set; }

        
        [Display(Name = "Office Contact 1")]
        public int? ContactId1 { get; set; }
        [NotMapped]
        public IList<SelectListItem> AvailableOfficeContact1 { get; set; }

        
        [Display(Name = "Office Contact 2")]
        public int? ContactId2 { get; set; }
        [NotMapped]
        public IList<SelectListItem> AvailableOfficeContact2 { get; set; }

        [Display(Name = "Banner Image")]
        public string BannerImage { get; set; }

        [Display(Name = "Thumb Image")]
        public string ThumbImage { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }

        [Display(Name = "Display Order")]
        public int? DisplayOrder { get; set; }

        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public string GoogleMapURL { get; set; }
        
    }
}