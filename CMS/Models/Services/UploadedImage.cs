﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AstonRoseCMS.Models
{
    public class UploadedImage
    {
        [Key]
        public int Id { get; set; }
        public string ImageName { get; set; }
        public int? ReferenceId { get; set; }
        public string Type { get; set; }

    }
}