﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseCMS.Models
{
    public class Testimonials
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [Display(Name = "Name")]
        [AllowHtml]
        public string Name { get; set; }

        
        [Display(Name = "Job Title")]
        [AllowHtml]
        public string JobTitle { get; set; }

        
        [Display(Name = "Company Name")]
        [AllowHtml]
        public string CompanyName { get; set; }

        
        [Display(Name = "Quote")]
        [AllowHtml]
        public string Quote { get; set; }

        [Display(Name = "PDF")]
        public string PDF
        {
            get;
            set;
        }
    }
}