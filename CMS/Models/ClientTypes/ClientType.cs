﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseCMS.Models
{
    public class ClientType
    {
        public ClientType()
        {
            //AvailableType = new List<SelectListItem>();
            AvailableContacts1 = new List<SelectListItem>();
            AvailableContacts2 = new List<SelectListItem>();
            UploadedImages = new List<UploadedImage>();
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is Required")]
        [Display(Name = "Title")]
        public string Title { get; set; }
       
        [Required(ErrorMessage = "Description is Required")]
        [Display(Name = "Description")]
        [AllowHtml]
        public string Description { get; set; }

        [Display(Name = "Select Contacts")]
        public string ContactIds { get; set; }

        [Display(Name = "Services Offered Title 1")]
        public string ServicesOfferedTitle1 { get; set; }

        [Display(Name = "Services Offered 1")]
        [AllowHtml]
        public string ServicesOffered1 { get; set; }

        [Display(Name = "Services Offered Title 2")]
        public string ServicesOfferedTitle2 { get; set; }

        [Display(Name = "Services Offered 2")]
        [AllowHtml]
        public string ServicesOffered2 { get; set; }

        
        [Display(Name = "Image")]
        public string Image { get; set; }

        [Display(Name = "PDF Brochure")]
        public string PDFBrochure { get; set; }

        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Date Modified")]
        public DateTime DateModified { get; set; }


        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Meta Title")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Description")]
        public string MetaDescription { get; set; }

        [NotMapped]
        public IList<SelectListItem> AvailableContacts1 { get; set; }

        [NotMapped]
        public IList<SelectListItem> AvailableContacts2 { get; set; }

        [NotMapped]
        public List<UploadedImage> UploadedImages { get; set; }



        public List<string> ContactSelectedIdList { get; set; }
    }
}