﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc;

namespace AstonRoseCMS.Models
{
    public class NewsListModel
    {
        public GridModel<News> NewsList { get; set; }
    }
}