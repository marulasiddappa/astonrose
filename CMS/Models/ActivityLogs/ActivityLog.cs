﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseCMS.Models
{
    public class ActivityLog
    {
        public ActivityLog()
        {
            
        }

        [Key]
        public int Id { get; set; }


        public string Type { get; set; }
        public string Description { get; set; }
        public string ActivityBy { get; set; }
        public DateTime ActivityOn { get; set; }
        public int? ReferenceID { get; set; }
        public string ReferenceKey { get; set; }
        public string ReferenceImageName { get; set; }
        
    }
}