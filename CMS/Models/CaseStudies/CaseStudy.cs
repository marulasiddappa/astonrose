﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AstonRoseCMS.Models
{
    public class CaseStudy
    {
        public CaseStudy()
        {
            //AvailableType = new List<SelectListItem>();
            AvailableServices = new List<ServiceSelectListItem>();
            AvailableClientTypes = new List<ServiceSelectListItem>();
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is Required")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        [Display(Name = "Description")]
        public string Description { get; set; }


        
        [Display(Name = "Service")]
        public int? ServiceID { get; set; }

        
        [Display(Name = "Client Type")]
        public int? ClientTypeID { get; set; }

        [Display(Name = "PDF")]
        public string PDF { get; set; }


        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Date Modified")]
        public DateTime DateModified { get; set; }


        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }

        [NotMapped]
        public IList<ServiceSelectListItem> AvailableServices { get; set; }

        [NotMapped]
        public IList<ServiceSelectListItem> AvailableClientTypes { get; set; }


    }

    public class ServiceSelectListItem : SelectListItem
    {
        public bool? Live { get; set; }
    }

    public class ServiceCaseStudy
    {
        [Key]
        [Column(Order = 0)]
        public int CaseStudyId { get; set; }
        [Key]
        [Column(Order = 1)]
        public int ServiceId { get; set; }
        public bool? Live { get; set; }
    }

    public class ClientTypeCaseStudy
    {
        [Key]
        [Column(Order = 0)]
        public int CaseStudyId { get; set; }
        [Key]
        [Column(Order = 1)]
        public int ClientTypeId { get; set; }
        public bool? Live { get; set; }
    }
}